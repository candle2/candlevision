import torch
from torch import nn
import torch.nn.functional as F


class ContrastiveLoss(nn.Module):
    """ Compute the contrastive loss: used in self-supervised learning

    More details about the code: https://zablo.net/blog/post/understanding-implementing-simclr-guide-eli5-pytorch/
    """

    def __init__(self, batch_size: int, temperature: int = 0.5):
        super().__init__()
        self.batch_size = batch_size
        self.register_buffer("temperature", torch.tensor(temperature))
        # Create negative tensor that represents all the negatives pairs. For that, we use a tensor where the value of
        # the diagonal is 0 and the rest is 1
        self.register_buffer("negative_mask", (~torch.eye(batch_size * 2, batch_size * 2, dtype=bool)).float())

    def forward(self, emb_i, emb_j):
        """ emb_i and emb_j are batches of embeddings, where corresponding indices are pairs z_i, z_j """

        z_i, z_j = F.normalize(emb_i, dim=1), F.normalize(emb_j, dim=1)
        # All representations are concatenated together in order to efficiently calculate cosine similarity between
        # each image pair.
        representations = torch.cat([z_i, z_j], dim=0)
        similarity_matrix = F.cosine_similarity(representations.unsqueeze(1), representations.unsqueeze(0), dim=2)

        # get the similarity between positive pairs
        sim_ij = torch.diag(similarity_matrix, self.batch_size)
        sim_ji = torch.diag(similarity_matrix, -self.batch_size)
        positives = torch.cat([sim_ij, sim_ji], dim=0)

        nominator = torch.exp(positives / self.temperature)
        denominator = self.negative_mask * torch.exp(similarity_matrix / self.temperature)

        loss_partial = -torch.log(nominator / torch.sum(denominator, dim=1))
        # The final loss is an arithmetic mean of the losses for all positive pairs in the batch.
        loss = torch.sum(loss_partial) / (2 * self.batch_size)

        return loss

from torchvision import transforms as transforms


class SimCLRDataTransform(object):
    """ Data transformation for self-supervised learning """

    def __init__(self,
                 resize_input: tuple = (224, 244),
                 gaussian_blur: bool = True,
                 jitter_strength: float = 0.3,
                 normalize: bool = True):

        self.jitter_strength = jitter_strength
        self.resize_input = resize_input
        self.gaussian_blur = gaussian_blur
        self.normalize = normalize

        self.color_jitter = transforms.ColorJitter(brightness=0.8 * self.jitter_strength,
                                                   contrast=0.8 * self.jitter_strength,
                                                   saturation=0.8 * self.jitter_strength,
                                                   hue=0.2 * self.jitter_strength)

        data_transforms = [
            transforms.ToPILImage(),
            transforms.RandomResizedCrop(size=self.resize_input, scale=(0.5, 1.0)),
            transforms.RandomHorizontalFlip(p=0.5),
            transforms.RandomApply([self.color_jitter], p=0.7),
        ]

        if self.gaussian_blur:
            kernel_size = int(0.1 * self.resize_input[0])
            if kernel_size % 2 == 0:
                kernel_size += 1
            self.gaussian_blur = transforms.GaussianBlur(kernel_size=kernel_size, sigma=(0.1, 1.0))
            data_transforms.append(transforms.RandomApply([self.gaussian_blur], p=0.5))

        if normalize:
            self.final_transform = transforms.Compose([transforms.ToTensor(),
                                                       transforms.Normalize(mean=[0.559, 0.574, 0.566],
                                                                            std=[0.393, 0.395, 0.399])
                                                       ])
        else:
            self.final_transform = transforms.ToTensor()

        data_transforms.append(self.final_transform)
        self.train_transforms = transforms.Compose(data_transforms)

    def __call__(self, sample):
        return self.train_transforms(sample)

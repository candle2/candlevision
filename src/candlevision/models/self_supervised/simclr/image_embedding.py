import torch
import torch.nn as nn
from collections import OrderedDict
from torchvision.models.detection.backbone_utils import resnet_fpn_backbone
from candlevision.models.backbones.torchvision_backbones import create_torchvision_backbone, remove_frozen_batch_norm


class ImageEmbedding(nn.Module):

    def __init__(self,
                 name_model: str = "resnet50",
                 embedding_size: int = 512,
                 pretrained: bool = False,
                 fpn: bool = False):
        """ Model to do image embedding,  take an image as input and return a linear tensor

        :param name_model: name of the model
        :param embedding_size: size of the linear tensor return
        :param pretrained: if true load the ImageNet weights
        :param fpn: if true use the resnet fpn backbone from torchvision

        .. warning:: With FPN, it only works for resnet50 or higher
        """
        super().__init__()
        if fpn:
            # Creates a torchvision resnet model with fpn added.
            base_model = resnet_fpn_backbone(name_model, pretrained=pretrained, trainable_layers=5).body
            out_channels = base_model.layer4[-1].conv3.out_channels

        else:
            # This does not create fpn backbone, it is supported for all models
            base_model, out_channels = create_torchvision_backbone(name_model, pretrained)

        self.feature_extractor = remove_frozen_batch_norm(base_model)

        self.embedding = nn.Sequential(
            nn.AdaptiveAvgPool2d(output_size=(1, 1)),
            nn.Flatten(),
            nn.Identity()
        )
        self.projection = nn.Sequential(
            nn.Linear(in_features=out_channels, out_features=embedding_size),
            nn.ReLU(inplace=True),
            nn.Linear(in_features=embedding_size, out_features=embedding_size)
        )

    def forward(self, x):
        features = self.feature_extractor(x)
        if isinstance(features, OrderedDict):
            features = features['3']
        embedding = self.embedding(features)
        projection = self.projection(embedding)
        return projection

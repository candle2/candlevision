import os
import json
import numpy as np
import fiftyone as fo
import pandas as pd
from fiftyone import ViewField as F
from candlevision.utils.coco_evaluator import CocoEvaluator, name_metrics
from candlevision.utils.utils import synchronize_pycocotools_metrics, convert_to_xywh


##################################################################################
#                       Classic COCO evaluation
##################################################################################


def coco_validation_step(self, batch, batch_idx):
    """ Function to use in method validation_step of a lightning module

        Tested with:
         - faster_rcnn
         - retinanet
    """
    images, bboxes, labels = batch

    predictions = self.model(images)

    # If it's the first batch, we create the COCOEvaluator first
    if batch_idx == 0:
        # Overall metric
        self.coco_evaluator = CocoEvaluator(self.datamodule)

    # Update overall metric
    self.coco_evaluator.update_gt(bboxes=bboxes, labels=labels)
    self.coco_evaluator.update_pred(predictions)


def coco_validation_epoch_end(self):
    # Compute overall metrics
    # print("\n[INFO]... Global metric")
    metrics = self.coco_evaluator.compute_metrics()

    # gather all the metrics from the different processes
    metrics_stats = synchronize_pycocotools_metrics(self.all_gather(metrics.stats),
                                                    self.is_distributed)

    metrics_category_stats = synchronize_pycocotools_metrics(self.all_gather(metrics.category_stats),
                                                             self.is_distributed)
    # List of the labels (name)
    labels = list(self.datamodule.labels)

    # Log the metrics in Tensorboard
    for k, v in name_metrics.items():
        # self.log("GLOBAL METRICS/" + v, metrics_stats[k])
        # For the mAP with IoU, we want the statistics per classes
        if k == 1:
            for i in range(self.num_classes - 1):
                self.log("GLOBAL METRICS/CATEGORY/" + labels[i] + " " + v, metrics_category_stats[k, i])


def apeus_validation_step(self, batch, batch_idx):
    """ Function to use in method validation_step of a lightning module.
    This function is designed to gather update the Coco Evaluator for the APEUS data

    Tested with:
     - faster_rcnn
     - retinanet
     """
    images, bboxes, labels, patient_ids = batch

    predictions = self.model(images)
    # If it's the first batch, we create the COCOEvaluator first
    if batch_idx == 0:
        # Overall metric
        self.coco_evaluator = CocoEvaluator(self.datamodule)
        # Metric per patient. Initialize a CocoEvaluator for each patient in a dictionary
        self.patient_coco_evaluator = {}
        for id_patient in self.patient_ids:
            self.patient_coco_evaluator[id_patient] = CocoEvaluator(self.datamodule)

    # Update overall metric
    self.coco_evaluator.update_gt(bboxes=bboxes, labels=labels)
    self.coco_evaluator.update_pred(predictions)

    # Update metric per patient
    for i, (bbox, label, patient_id) in enumerate(zip(bboxes, labels, patient_ids)):
        self.patient_coco_evaluator[patient_id[0]].update_gt(bboxes=[bbox], labels=[label])
        self.patient_coco_evaluator[patient_id[0]].update_pred([predictions[i]])


def apeus_validation_epoch_end(self):
    """ Function to use in method validation_epoch_end of a lightning module.
    This function is designed to compute and log the metrics for the APEUS data

    """
    # Compute overall metrics
    # print("\n[INFO]... Global metric")
    metrics = self.coco_evaluator.compute_metrics()

    # gather all the metrics from the different processes
    metrics_stats = synchronize_pycocotools_metrics(self.all_gather(metrics.stats),
                                                    self.is_distributed)

    metrics_category_stats = synchronize_pycocotools_metrics(self.all_gather(metrics.category_stats),
                                                             self.is_distributed)

    # List of the labels (name)
    labels = list(self.datamodule.labels)

    # Log the metrics in Tensorboard
    for k, v in name_metrics.items():
        self.log("GLOBAL METRICS/" + v, metrics_stats[k])
        # For the mAP with IoU, we want the statistics per classes
        if k == 1:
            for i in range(self.num_classes - 1):
                self.log("GLOBAL METRICS/CATEGORY/" + labels[i] + " " + v, metrics_category_stats[k, i])

    # Compute metrics per patient
    patient_metrics = {}
    patient_metrics_categories = {}
    global_metrics_patient_wise = []
    global_metrics_categories_patient_wise = []
    for id_patient in self.patient_ids:
        # print(f"[INFO]... Evaluation patient : {id_patient}")
        patient_coco_result = self.patient_coco_evaluator[id_patient].compute_metrics()
        patient_metrics[id_patient] = synchronize_pycocotools_metrics(self.all_gather(patient_coco_result.stats),
                                                                      self.is_distributed)
        patient_metrics_categories[id_patient] = synchronize_pycocotools_metrics(
            self.all_gather(patient_coco_result.category_stats),
            self.is_distributed)
        # Keep the mAP IoU 50% for each patient
        global_metrics_patient_wise.append(patient_metrics[id_patient][1].cpu().numpy())
        # Keep the mAP IoU 50% for each categories of each patient
        global_metrics_categories_patient_wise.append(patient_metrics_categories[id_patient][1].cpu().numpy())

        # Log the metrics in Tensorboard
        for k, v in name_metrics.items():
            # For the mAP with IoU, we want the statistics per classes
            if k == 1:
                self.log(f"METRICS PER PATIENTS/Patient n°{id_patient}/" + v, patient_metrics[id_patient][k])
                for i in range(self.num_classes - 1):
                    self.log(f"METRICS PER PATIENTS/Patient n°{id_patient}/CATEGORY/" + labels[i] + " " + v,
                             patient_metrics_categories[id_patient][k, i])

    self.log("GLOBAL METRICS/Mean Average Precision Patient-wise [ IoU=0.50 | area= all | maxDets=100 ]",
             np.mean(global_metrics_patient_wise))

    global_metrics_categories_patient_wise = np.array(global_metrics_categories_patient_wise)
    # Index to remove the -1 values. Otherwise the average will not return a correct value
    index_to_keep_gmcpw = global_metrics_categories_patient_wise > -1
    for i in range(self.num_classes - 1):
        self.log("GLOBAL METRICS/CATEGORY/ " + labels[i] + " mAP Patient-wise[ IoU=0.50 | area= all | maxDets=100 ]",
                 np.mean(global_metrics_categories_patient_wise[index_to_keep_gmcpw[:, i], i], axis=0))


##################################################################################
#                                   Voxel51
##################################################################################

def fiftyone_validation_step(self, batch, batch_idx):
    """ Function to use in method validation_step of a lightning module.

    First, It creates a dataset object from voxel51. Next, for each image a sample object is created and it's fill with
    the ground truth and the predictions done by the model. Finally each sample are added to the dataset.

    .. warning:: only works with ApeusDataset

    """

    images, bboxes, labels, patient_ids = batch
    predictions = self.model(images)

    # image size
    height, width = self.datamodule.resize
    # names labels
    classes = list(self.datamodule.labels)

    # If it's the first batch, we create a fiftyone dataset for the evaluation
    if batch_idx == 0:
        self.evaluator = fo.Dataset()

    # Loop over the bounding boxes and labels in the data batch
    for k, (image_boxes, image_labels, id_patient, prediction) in enumerate(
            zip(bboxes, labels, patient_ids, predictions)):
        # For each image, we create a sample
        sample_image = fo.Sample(filepath=f"image_{batch_idx}_{k}",
                                 patient_id=id_patient[0])

        # GROUND TRUTH
        ground_truth = []
        # Loop over each bounding box and label
        for box, label in zip(image_boxes, image_labels):
            x, y, w, h = box
            rel_box = [x / width, y / height, w / width, h / height]

            ground_truth.append(
                fo.Detection(
                    label=classes[int(label - 1)],
                    bounding_box=rel_box,
                )
            )

        # Save sample ground truth in the dataset
        sample_image["ground_truth"] = fo.Detections(detections=ground_truth)

        if len(prediction) == 0:
            self.evaluate.add_sample(sample_image)
            continue

        scores = prediction["scores"].tolist()
        pred_boxes = prediction["boxes"]
        pred_boxes = convert_to_xywh(pred_boxes).tolist()
        labels = prediction["labels"].tolist()

        detections = []
        for box, label, score in zip(pred_boxes, labels, scores):
            x, y, w, h = box
            rel_box = [x / width, y / height, w / width, h / height]

            detections.append(
                fo.Detection(
                    label=classes[int(label - 1)],
                    bounding_box=rel_box,
                    confidence=score
                )
            )

        sample_image["predictions"] = fo.Detections(detections=detections)
        self.evaluator.add_sample(sample_image)


def fiftyone_validation_epoch_end(self, path_save: str, confidence_threshold: float = 0.2):
    """ Compute the evaluation results and save them in a json file. It also save the precision recall curves

    :param self: to interact with the object inside lightning module
    :param path_save: path to a directory where the evaluation results and the precision recall curves will be saved
    :param confidence_threshold: score below wich the predictions are ignored

    .. warning:: only works with ApeusDataset

    """
    os.makedirs(path_save, exist_ok=True)
    result_evaluation = {}

    for k in self.datamodule.patient_ids:
        print(f"EVALUATION PATIENT n°{k}")
        patient_results = {}
        patient_wise_evaluator = self.evaluator.match(F("patient_id") == k)

        results = patient_wise_evaluator.evaluate_detections("predictions",
                                                             gt_field="ground_truth",
                                                             eval_key="eval",
                                                             compute_mAP=True,
                                                             classwise=True,
                                                             iou_threshs=[0.5])
        # mAP  and AP per class
        patient_results['mAP'] = results.mAP()

        classes = list(self.datamodule.labels)
        for c in classes:
            patient_results['AP_' + c] = results._classwise_AP[results._get_class_index(c)]

        # Precision Recall curves
        plot = results.plot_pr_curves()
        name_figure = path_save + f"/PR_curve_patient_{k}.jpg"
        patient_results['fig'] = name_figure
        plot.write_image(name_figure)

        # Precision, recall, f1-score
        report_evaluator = patient_wise_evaluator.filter_labels("predictions",
                                                                F("confidence") > confidence_threshold)

        report_results = report_evaluator.evaluate_detections("predictions",
                                                              gt_field="ground_truth",
                                                              eval_key="eval",
                                                              compute_mAP=True,
                                                              classwise=True,
                                                              iou_threshs=[0.5])

        print("DATAFRAME: \n")
        report = report_results.report()
        df_report = pd.DataFrame(report).transpose()
        df_report = df_report.round(2)
        patient_results['report'] = df_report.to_string()

        result_evaluation[k] = patient_results

    with open(path_save + "/evaluation_results.json", 'w') as json_file:
        json.dump(result_evaluation, json_file)
# import necessary packages
import torch
import pytorch_lightning as pl
import numpy as np
from typing import *
from candlevision.models.detection import metrics
from candlevision.utils.utils import convert_coco_bboxes_to_tensor
from candlevision.data.loaders import DetectEusDataModule


class ObjectDetector(pl.LightningModule):

    def __init__(self,
                 datamodule: pl.LightningDataModule,
                 model: torch.nn.Module,
                 learning_rate: float = 0.001,
                 weight_decay: float = 0.001,
                 nb_epochs: int = 50000,
                 num_gpus: int = 1,
                 metric: str = 'coco',
                 path_save_evaluation_result: Optional[str] = None,
                 score_threshold: Optional[float] = 0.2):
        """

        :param datamodule: lightning data module
        :param model:
        :param learning_rate:
        :param weight_decay: weight decay for the optimizer
        :param nb_epochs:
        :param num_gpus:
        :param metric: if 'coco' compute the coco metrics, if 'voxel51' compute AP, precision, recall and f1-score and
        save Precision-Recall curves as well as the metrics results.
        :param path_save_evaluation_result: Only use when metric == 'voxel51', path to the directory where the figures
        and the json file containing the results will be saved.
        :param score_threshold: Only use when metric == 'voxel51', threshold to filter the predictions used to compute
        the precision, recall, f1-score.

        """
        super().__init__()

        self.num_gpus = num_gpus
        self.is_distributed = True if self.num_gpus > 1 else False
        self.nb_epochs = nb_epochs
        self.datamodule = datamodule
        self.learning_rate = learning_rate
        self.weight_dacay = weight_decay
        self.num_classes = int(self.datamodule.num_classes) + 1  # num of classes + background
        self.model = model
        self.metric = metric
        self.path_save_evaluation_result = path_save_evaluation_result
        self.score_threshold = score_threshold

        assert metric in ['coco', 'voxel51'], "Possible value for metric argument: ['coco', 'voxel51']"

        if metric == "voxel51":
            assert isinstance(datamodule, DetectEusDataModule), "voxel51 metric only works with DetectEusDataModule"
            assert path_save_evaluation_result, "to use voxel51, you need to provide a path to save the eval results"

        if isinstance(datamodule, DetectEusDataModule):
            self.num_patient_ids = self.datamodule.num_patients
            self.patient_ids = self.datamodule.patient_ids

    def forward(self, x):

        return self.model(x)

    def training_step(self, batch, batch_idx):

        if isinstance(self.datamodule, DetectEusDataModule):
            images, bboxes, labels, _ = batch
        else:
            images, bboxes, labels = batch

        # Transform what is returned by the dataloader into a correct input for the model
        images = list(images)
        targets = []
        # Loop over the images inside the batch
        for box, label in zip(bboxes, labels):
            if len(box) == 0:
                target_dict = {"boxes": torch.zeros((0, 4), dtype=torch.float32).to(torch.device('cuda')),
                               "labels": torch.zeros(1, dtype=torch.int64).to(torch.device('cuda'))}

            else:
                target_dict = {"boxes": convert_coco_bboxes_to_tensor(np.array(box)).to(torch.device('cuda')),
                               "labels": torch.from_numpy(np.array(label)).type(dtype=torch.int64).to(
                                   torch.device('cuda'))}
            targets.append(target_dict)

        # faster-rcnn takes both images and targets for training, returns
        loss_dict = self.model(images, targets)
        loss = sum(loss for loss in loss_dict.values())

        # log the different loss values in tensorboard
        for k, v in loss_dict.items():
            self.log("LOSSES/" + k, v, sync_dist=True)
        self.log("GLOBAL LOSS", loss, sync_dist=True)

        return loss

    def validation_step(self, batch, batch_idx):

        if self.metric == 'coco':
            if isinstance(self.datamodule, DetectEusDataModule):
                metrics.apeus_validation_step(self, batch, batch_idx)
            else:
                metrics.coco_validation_step(self, batch, batch_idx)

        else:
            metrics.fiftyone_validation_step(self, batch, batch_idx)

    def validation_epoch_end(self, outputs: List[Any]) -> None:

        if self.metric == 'coco':
            if isinstance(self.datamodule, DetectEusDataModule):
                metrics.apeus_validation_epoch_end(self)
            else:
                metrics.coco_validation_epoch_end(self)

        else:
            metrics.fiftyone_validation_epoch_end(
                self,
                path_save=self.path_save_evaluation_result,
                confidence_threshold=self.score_threshold
            )

    def predict_step(self, batch, batch_idx, dataloader_idx):
        images, crop_coordinates = batch[0], batch[1]

        predictions = self.model(images)

        return {"predictions": predictions, "crop_coordinates": crop_coordinates}

    def configure_optimizers(self):
        parameters = list(self.parameters())
        trainable_parameters = list(filter(lambda p: p.requires_grad, parameters))

        optimizer = torch.optim.SGD(params=trainable_parameters,
                                    lr=self.learning_rate,
                                    momentum=0.9,
                                    weight_decay=self.weight_dacay)

        # optimizer = torch.optim.AdamW(params=trainable_parameters,
        #                               lr=self.learning_rate,
        #                               weight_decay=self.weight_dacay)

        # lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer=optimizer, T_max=self.nb_epochs)

        # lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer=optimizer,
        #                                                step_size=1,
        #                                                gamma=0.2)

        return [optimizer]# , [lr_scheduler]

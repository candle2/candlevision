import torch
import torch.nn as nn
from torchvision.ops import box_iou
from typing import List, Dict
import sys


def calc_iou(a, b):
    # a(anchor) [boxes, (y1, x1, y2, x2)]
    # b(gt, coco-style) [boxes, (x1, y1, x2, y2)]

    area = (b[:, 2] - b[:, 0]) * (b[:, 3] - b[:, 1])
    iw = torch.min(torch.unsqueeze(a[:, 3], dim=1), b[:, 2]) - torch.max(torch.unsqueeze(a[:, 1], 1), b[:, 0])
    ih = torch.min(torch.unsqueeze(a[:, 2], dim=1), b[:, 3]) - torch.max(torch.unsqueeze(a[:, 0], 1), b[:, 1])
    iw = torch.clamp(iw, min=0)
    ih = torch.clamp(ih, min=0)
    ua = torch.unsqueeze((a[:, 2] - a[:, 0]) * (a[:, 3] - a[:, 1]), dim=1) + area - iw * ih
    ua = torch.clamp(ua, min=1e-8)
    intersection = iw * ih
    IoU = intersection / ua

    return IoU


class FocalLoss(nn.Module):

    def __init__(self,
                 alpha: float = 0.25,
                 gamma: float = 2,
                 upper_threshold_objectness: float = 0.4,
                 lower_threshold_objectness: float = 0.3):

        super().__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.upper_threshold_objectness = upper_threshold_objectness
        self.lower_threshold_objectness = lower_threshold_objectness

    @staticmethod
    def preprocess(targets: List[Dict[str, torch.Tensor]]):
        # gather the bbox coordinates with the label index
        annotations = [torch.cat([target["boxes"], (target["labels"] - 1).unsqueeze(dim=1)], dim=1) for target in
                       targets]

        # pad the tensor in order to stack them
        max_annotations_size = max([anno.size(0) for anno in annotations])
        new_annotations = []
        for anno in annotations:
            padding = torch.nn.ConstantPad2d((0, 0, 0, max_annotations_size - anno.size(0)), -1)
            new_annotations.append(padding(anno))

        return torch.stack(new_annotations)

    def forward(self, classifications, regressions, anchors, annotations):
        """
        Compute the classification loss and the regression loss with the focal loss formula.
        The goal of the focal loss is to put the emphasize on the wrong classifications
        :param classifications: output of the classification layers (batch size, num anchors, num classes)
        :param regressions: output of the regression layers (batch_size, num anchors, 4)
        :param anchors (1, num anchors, 4)
        :param annotations: (batch size, num annotations, 5)
        :return: classification losses and regression losses
        """
        alpha = 0.25
        gamma = 2.0
        batch_size = classifications.shape[0]
        classification_losses = []
        regression_losses = []

        # preprocess because annotations inputs are originally a list of dictionaries
        annotations = self.preprocess(targets=annotations)

        anchor = anchors[0, :, :]  # anchors dim : (1, num_anchors, 4) -> anchor dim (num_anchors, 4)
        dtype = anchors.dtype

        anchor_widths = anchor[:, 3] - anchor[:, 1]
        anchor_heights = anchor[:, 2] - anchor[:, 0]
        anchor_ctr_x = anchor[:, 1] + 0.5 * anchor_widths
        anchor_ctr_y = anchor[:, 0] + 0.5 * anchor_heights

        for j in range(batch_size):

            classification = classifications[j, :, :]
            regression = regressions[j, :, :]

            bbox_annotation = annotations[j]
            # remove the annotations of the empty images and the padding we add in the preprocessing
            bbox_annotation = bbox_annotation[bbox_annotation[:, 4] != -1]

            classification = torch.clamp(classification, 1e-4, 1.0 - 1e-4)

            if bbox_annotation.shape[0] == 0:
                alpha_factor = torch.ones_like(classification) * self.alpha
                alpha_factor = alpha_factor
                alpha_factor = 1. - alpha_factor
                focal_weight = classification
                focal_weight = alpha_factor * torch.pow(focal_weight, self.gamma)

                bce = -(torch.log(1.0 - classification))

                cls_loss = focal_weight * bce

                regression_losses.append(torch.tensor(0).float().to(torch.device('cuda')))
                classification_losses.append(cls_loss.sum())

                continue

            IoU = calc_iou(anchor[:, :], bbox_annotation[:, :4])  # size :(num_anchors, num_annotations)

            IoU_max, IoU_argmax = torch.max(IoU, dim=1)  # size :(num_anchors,1)

            ###############################################################################
            #                    compute the loss for classification
            ###############################################################################
            # initialisation targets
            targets = torch.ones_like(classification) * -1

            # set targets = 0 for every IoU_max < lower_threshold_objectness. The value proposed in the article is 0.4
            # Everything with a IoU score under lower_threshold_objectness will be considered as background
            targets[torch.lt(IoU_max, self.lower_threshold_objectness), :] = 0

            # get every index where IoU_max >= 0.2
            # Everything with a IoU score higher than upper_threshold_objectness will be considered as an object
            positive_indices = torch.ge(IoU_max, self.upper_threshold_objectness)  # The proposed in the article is 0.5

            num_positive_anchors = positive_indices.sum()

            # associate every anchors with the corresponding bbox (ground truth)
            assigned_annotations = bbox_annotation[IoU_argmax, :]

            targets[positive_indices, :] = 0
            # set the class id for every anchors having a IoU >= 0.5
            targets[positive_indices, assigned_annotations[positive_indices, 4].long()] = 1

            # Recall formula for the classification loss:
            # L_cls_i = alpha*yi*log(pi)(1-pi)**gamma + (1-alpha)(1-yi)*log(1-pi)pi**gamma
            # - yi : equals 1 if the ground-truth belongs to the i-th class and 0 otherwise
            # - pi : is the predicted probability for the i-th class
            # L_cls = -sum(L_cls_i) for i=1 to num_classes

            # Compute the alpha and (1-alpha) from the L_cls_i formula
            alpha_factor = torch.ones_like(targets) * self.alpha

            alpha_factor = torch.where(torch.eq(targets, 1.), alpha_factor, 1. - alpha_factor)
            # Compute the pi and (1-pi) from the L_cls_i formula
            focal_weight = torch.where(torch.eq(targets, 1.), 1. - classification, classification)
            # alpha*yi*(1-pi)**gamma + (1-alpha)(1-yi)*pi**gamma
            focal_weight = alpha_factor * torch.pow(focal_weight, self.gamma)

            bce = -(targets * torch.log(classification) + (1.0 - targets) * torch.log(1.0 - classification))

            cls_loss = focal_weight * bce

            zeros = torch.zeros_like(cls_loss)

            # Recall (default value):
            # IoU score < 0.4 -> background
            # IoU score >= 0.5 -> object
            # Set cls_loss_i = 0 for IoU E [0.4,0.5], indeed we ignore them
            cls_loss = torch.where(torch.ne(targets, -1.0), cls_loss, zeros)

            classification_losses.append(cls_loss.sum() / torch.clamp(num_positive_anchors.to(dtype), min=1.0))

            ###############################################################################
            #                    compute the loss for regression
            ###############################################################################

            if positive_indices.sum() > 0:
                # keep only the retained object
                assigned_annotations = assigned_annotations[positive_indices, :]

                anchor_widths_pi = anchor_widths[positive_indices]
                anchor_heights_pi = anchor_heights[positive_indices]
                anchor_ctr_x_pi = anchor_ctr_x[positive_indices]
                anchor_ctr_y_pi = anchor_ctr_y[positive_indices]

                # Ground truth are in (x1,y1,x2,y2) and we need (x_center, y_center, width, height)
                gt_widths = assigned_annotations[:, 2] - assigned_annotations[:, 0]
                gt_heights = assigned_annotations[:, 3] - assigned_annotations[:, 1]
                gt_ctr_x = assigned_annotations[:, 0] + 0.5 * gt_widths
                gt_ctr_y = assigned_annotations[:, 1] + 0.5 * gt_heights

                # clip widths and heights to 1
                gt_widths = torch.clamp(gt_widths, min=1)
                gt_heights = torch.clamp(gt_heights, min=1)

                # define regression targets (the standard parametrization used in object detection)
                targets_dx = (gt_ctr_x - anchor_ctr_x_pi) / anchor_widths_pi
                targets_dy = (gt_ctr_y - anchor_ctr_y_pi) / anchor_heights_pi
                targets_dw = torch.log(gt_widths / anchor_widths_pi)
                targets_dh = torch.log(gt_heights / anchor_heights_pi)

                targets = torch.stack((targets_dy, targets_dx, targets_dh, targets_dw))
                targets = targets.t()

                regression_diff = torch.abs(targets - regression[positive_indices, :])

                # smooth L1
                regression_loss = torch.where(
                    torch.le(regression_diff, 1.0 / 9.0),
                    0.5 * 9.0 * torch.pow(regression_diff, 2),
                    regression_diff - 0.5 / 9.0
                )

                regression_losses.append(regression_loss.mean())
            else:
                regression_losses.append(torch.tensor(0).float().to(torch.device('cuda')))

        return torch.stack(classification_losses).mean(dim=0, keepdim=True), \
               torch.stack(regression_losses).mean(dim=0, keepdim=True) * 50

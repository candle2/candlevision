from typing import Tuple

import timm
import torch.nn
from pathlib import Path
from candlevision.models.backbones.torchvision_backbones import create_torchvision_backbone


class ResnetClassifier(torch.nn.Module):
    def __init__(self, backbone, pretrained, num_classes, replace_state_dict_layer: Tuple[str, str] = None):
        super().__init__()
        self.num_classes = num_classes
        self.pretrained = pretrained
        self.backbone = backbone
        self.replace_state_dict_layer = replace_state_dict_layer
        self.feature_extractor = self._build_feature_extactor()
        self.head = self._build_head()

    def _get_feature_extractor(self):
        """ Return a backbone for feature extraction """
        return create_torchvision_backbone(
            self.backbone,
            self.pretrained,
            self.replace_state_dict_layer
        )

    def _build_feature_extactor(self):
        feature_extractor, self.in_features = self._get_feature_extractor()
        return torch.nn.Sequential(
            feature_extractor,
            torch.nn.AdaptiveAvgPool2d(output_size=(1, 1)),
            torch.nn.Flatten(),
        )

    def _build_head(self):
        return torch.nn.Sequential(
            torch.nn.Linear(self.in_features, self.num_classes),
            torch.nn.Softmax(dim=1)
        )

    def forward(self, x):
        x = self.feature_extractor(x)
        x = self.head(x)
        return x


class EfficientNet(torch.nn.Module):
    """ EfficientNet from timm library """

    def __init__(self, num_classes: int, name_model: str = 'efficientnetv2_rw_s', pretrained: bool = False):
        super().__init__()
        self.num_classes = num_classes
        self.name_model = name_model

        if self.name_model not in timm.list_models("efficientnet*", pretrained=True):
            raise ValueError(f"name_module : {name_model} is not available, it must be one of these models : /"
                             f"{timm.list_models('efficientnet*', pretrained=True)}")

        self.pretrained = pretrained
        self.model = timm.create_model(self.name_model, num_classes=self.num_classes, pretrained=self.pretrained)

    def forward(self, x):
        return self.model(x)


class BasicClassifier(torch.nn.Module):
    def __init__(self, num_classes: int, name_model: str, pretrained: bool = False, checkpoint_path: str = None):
        super().__init__()
        self.num_classes = num_classes
        self.name_model = name_model
        self.pretrained = pretrained
        self.model = timm.create_model(self.name_model, num_classes=self.num_classes, pretrained=self.pretrained)

        # load pretrained model
        checkpoint_path = str([p for p in Path("/home/apeus/PycharmProjects/models/pancreas_parts/miccai/").glob(f"basic_{name_model}*")][0])
        state_dict = torch.load(checkpoint_path)['state_dict']
        for key in list(state_dict.keys()):
            state_dict[key.replace('model.', '')] = state_dict.pop(key)

        self.model.load_state_dict(state_dict, strict=True)
        self.pool = torch.nn.Sequential(
            torch.nn.AdaptiveAvgPool2d(output_size=(1, self.num_classes)),
            torch.nn.Flatten()
        )

    def forward(self, x):
        predictions = []
        for s in range(x.size(1)):
            predictions.append(self.model(x[:, s, :, :, :]))
        x = torch.stack(predictions, dim=1)
        x = self.pool(x)
        return x

import albumentations as A
from albumentations.pytorch import ToTensorV2

from candlevision.models.detection.my_transforms import Rescale_0_1


class VideoTransform(object):
    """ Data transformation with albumentation for self-supervised learning """

    def __init__(self,
                 num_frame_per_clip: int,
                 resize: tuple = (224, 224),
                 spatial_transform: bool = True,
                 probability_spatial_transform: float = 1.0,
                 pixel_level_transform: bool = True,
                 probability_pixel_level_transform: float = 1.0,
                 ):
        self.num_frame_per_clip = num_frame_per_clip
        self.resize = resize
        self.spatial_transform = None
        self.probability_spatial_transform = probability_spatial_transform
        self.pixel_level_transform = None
        self.probability_pixel_level_transform = probability_pixel_level_transform

        self.general_transform = A.Compose(
            [
                A.Resize(self.resize[0], self.resize[1]),
            ],
            additional_targets={f"image{k}": "image" for k in range(self.num_frame_per_clip - 1)}
        )

        if spatial_transform:
            self.spatial_transform = A.Compose(
                [
                    A.HorizontalFlip(p=self.probability_spatial_transform),
                    A.CoarseDropout(max_holes=32, p=self.probability_spatial_transform),
                    A.ElasticTransform(alpha_affine=15, p=self.probability_spatial_transform),
                    A.GridDistortion(p=self.probability_spatial_transform),
                    A.RandomResizedCrop(self.resize[0], self.resize[1],
                                        scale=(0.6, 1.0), p=self.probability_spatial_transform),
                ],
                additional_targets={f"image{k}": "image" for k in range(self.num_frame_per_clip - 1)}
            )

        if pixel_level_transform:
            self.pixel_level_transform = A.Compose(
                [
                    A.Sharpen(p=self.probability_pixel_level_transform),
                    A.Emboss(strength=(0.2, 0.5), p=self.probability_pixel_level_transform),
                    A.Blur(blur_limit=4, p=self.probability_pixel_level_transform),
                    A.MultiplicativeNoise(p=self.probability_pixel_level_transform),
                ],
                additional_targets={f"image{k}": "image" for k in range(self.num_frame_per_clip - 1)}
            )

        self.final_transform = A.Compose(
            [
                self.general_transform,
                self.pixel_level_transform,
                self.spatial_transform,
                A.Normalize(mean=(0.559, 0.574, 0.566), std=(0.393, 0.395, 0.399)),
                ToTensorV2(),
                Rescale_0_1(),
            ],
            additional_targets={f"image{k}": "image" for k in range(self.num_frame_per_clip - 1)}
        )

    def __call__(self, image, **kwargs):
        return self.final_transform(image=image, **kwargs)
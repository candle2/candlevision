from typing import Union

import torch
import timm

from candlevision.utils.utils import my_logger

logger = my_logger()


class MViT(torch.nn.Module):
    """ Multi-scale vision transformers https://arxiv.org/abs/2104.11227
    available pre-trained models are 'mvit_base_16x4', 'mvit_base_32x3'
    """

    def __init__(self, model_num_class: int, name_model: str = "mvit_base_16x4", pretrained: Union[bool, str] = True):
        super().__init__()
        self.pretrained = pretrained
        self.name_model = name_model
        self.model_num_class = model_num_class

        if isinstance(self.pretrained, str):
            self.model = self.load_checkpoint()
        else:
            self.model = torch.hub.load(
                "facebookresearch/pytorchvideo",
                model=self.name_model,
                pretrained=self.pretrained
            )
            in_features = self.model.head.proj.in_features
            self.model.head.proj = torch.nn.Linear(in_features, self.model_num_class)

    def load_checkpoint(self):
        state_dict = torch.load(self.pretrained)['state_dict']
        for key in list(state_dict.keys()):
            state_dict[key.replace('model.model.', '')] = state_dict.pop(key)

        model = torch.hub.load("/home/afleurentin/.cache/torch/hub/facebookresearch_pytorchvideo_main",
                               model=self.name_model, pretrained=False, source="local")
        in_features = model.head.proj.in_features
        model.head.proj = torch.nn.Linear(in_features, self.model_num_class)
        try:
            model.load_state_dict(state_dict, strict=True)
        except RuntimeError as e:
            logger.warning(f"Ignoring: {e}")
            model.head.proj = torch.nn.Linear(in_features, 6)
            model.load_state_dict(state_dict, strict=False)
            logger.warning("the head of the model is changed")
            model.head.proj = torch.nn.Linear(in_features, self.model_num_class)

        return model

    def forward(self, x):
        x = x.permute((0, 2, 1, 3, 4))
        return self.model(x)


class ViT(torch.nn.Module):
    """ Vision Transformer from timm library

    Pretrained model available:
    'vit_base_patch8_224',
    'vit_base_patch8_224_in21k',
    'vit_base_patch16_224',
    'vit_base_patch16_224_in21k',
    'vit_base_patch16_224_miil',
    'vit_base_patch16_224_miil_in21k',
    'vit_base_patch16_384',
    'vit_base_patch16_sam_224',
    'vit_base_patch32_224',
    'vit_base_patch32_224_in21k',
    'vit_base_patch32_384',
    'vit_base_patch32_sam_224',
    'vit_base_r50_s16_224_in21k',
    'vit_base_r50_s16_384',
    'vit_huge_patch14_224_in21k',
    'vit_large_patch16_224',
    'vit_large_patch16_224_in21k',
    'vit_large_patch16_384',
    'vit_large_patch32_224_in21k',
    'vit_large_patch32_384',
    'vit_large_r50_s32_224',
    'vit_large_r50_s32_224_in21k',
    'vit_large_r50_s32_384',
    'vit_small_patch16_224',
    'vit_small_patch16_224_in21k',
    'vit_small_patch16_384',
    'vit_small_patch32_224',
    'vit_small_patch32_224_in21k',
    'vit_small_patch32_384',
    'vit_small_r26_s32_224',
    'vit_small_r26_s32_224_in21k',
    'vit_small_r26_s32_384',
    'vit_tiny_patch16_224',
    'vit_tiny_patch16_224_in21k',
    'vit_tiny_patch16_384',
    'vit_tiny_r_s16_p8_224',
    'vit_tiny_r_s16_p8_224_in21k',
    'vit_tiny_r_s16_p8_384',

    """

    def __init__(self, num_classes: int, name_model: str = 'vit_small_patch16_224', pretrained: bool = False):
        super().__init__()
        self.num_classes = num_classes
        self.name_model = name_model
        self.pretrained = pretrained
        self.model = timm.create_model(self.name_model, num_classes=self.num_classes, pretrained=self.pretrained)

    def forward(self, x):
        return self.model(x)


class DeiT(torch.nn.Module):
    """ DeiT from timm library

    Pretrained models available:
    'deit_base_distilled_patch16_224',
    'deit_base_distilled_patch16_384',
    'deit_base_patch16_224',
    'deit_base_patch16_384',
    'deit_small_distilled_patch16_224',
    'deit_small_patch16_224',
    'deit_tiny_distilled_patch16_224',
    'deit_tiny_patch16_224'
    """

    def __init__(self, num_classes: int, name_model: str = 'deit_small_patch16_224', pretrained: bool = False):
        super().__init__()
        self.num_classes = num_classes
        self.name_model = name_model
        self.pretrained = pretrained
        self.model = timm.create_model(self.name_model, num_classes=self.num_classes, pretrained=self.pretrained)

    def forward(self, x):
        return self.model(x)


if __name__ == '__main__':
    CHECKPOINT_PATH = "/home/afleurentin/Bureau/PPP/miccai_models/mvit_16x4_2_epoch=50.ckpt"
    model_mvit = MViT(model_num_class=6, pretrained=CHECKPOINT_PATH)
    input = torch.randn(1, 16, 3, 224, 224)
    print(model_mvit(input))

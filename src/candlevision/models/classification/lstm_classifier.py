from typing import Tuple

import timm
import torch.nn
import torch.nn.functional as F
from torchvision.models.detection.backbone_utils import resnet_fpn_backbone

from candlevision.models.backbones.torchvision_backbones import create_torchvision_backbone


class LSTMClassifier(torch.nn.Module):
    def __init__(self,
                 backbone: str,
                 pretrained: bool,
                 hidden_size: int,
                 num_frame_per_clip: int,
                 num_lstm_layer: int,
                 num_classes: int,
                 fpn: bool = False,
                 dropout: float = 0.05,
                 replace_state_dict_layer: Tuple[str, str] = None):
        super().__init__()
        self.fpn = fpn
        self.num_classes = num_classes
        self.num_lstm_layer = num_lstm_layer
        self.num_frame_per_clip = num_frame_per_clip
        self.hidden_size = hidden_size
        self.pretrained = pretrained
        self.dropout = dropout
        self.backbone = backbone
        self.replace_state_dict_layer = replace_state_dict_layer
        # self.feature_extractor = self._build_feature_extractor()
        self.feature_extractor = self._get_feature_extractor()

        self.flatten = torch.nn.Flatten()

        if self.fpn:
            self.lstm_list = torch.nn.ModuleList([self._build_lstm() for _ in range(5)])
            self.head_list = torch.nn.ModuleList([self._build_head() for _ in range(5)])

            self.classification_head_fpn = torch.nn.Sequential(
                torch.nn.Flatten(start_dim=1),
                torch.nn.Linear(5 * self.num_classes, self.num_classes),
                torch.nn.Softmax(dim=1)
            )

        else:
            self.lstm = self._build_lstm()
            self.head = self._build_head()

        """
        Create a classifier for a sequence of frames. the network expects an input with the following dimensions
        [B, T, C, W, H]
        B = Batch size
        T = number of frames in the clip
        C = Channel
        W = width
        H = Height
        
        Structure of the network without backbone:
        
        ::
                            input
                              ↓
                            Resnet
                              ↓
                            LSTM
                              ↓
                            HEAD
                            
        
        Structure of the network with backbone:
        
        ::
                            input
                              ↓
                            Resnet
                              ↓
                             FPN
                    ↓    ↓    ↓    ↓    ↓
                  LSTM LSTM LSTM LSTM LSTM
                    ↓    ↓    ↓    ↓    ↓
                  HEAD HEAD HEAD HEAD HEAD
                              ↓
                        AVG_POOLING1D
        
        
        """

    def _get_feature_extractor(self):
        """ Return a backbone for feature extraction """

        # if "resnet" in self.backbone:
        #     if self.fpn:
        #         feature_extractor = resnet_fpn_backbone(
        #             self.backbone,
        #             pretrained=self.pretrained,
        #             trainable_layers=5
        #         )
        #
        #         self.in_features = 256
        #
        #     else:
        #         feature_extractor, self.in_features = create_torchvision_backbone(
        #             self.backbone,
        #             self.pretrained,
        #             self.replace_state_dict_layer
        #         )

        # else:
        # only timm models are handled
        feature_extractor = timm.create_model(self.backbone, pretrained=self.pretrained, num_classes=0)
        self.in_features = feature_extractor.num_features

        return feature_extractor

    def _build_feature_extractor(self):
        feature_extractor = self._get_feature_extractor()

        if self.fpn:
            return feature_extractor
        else:
            return torch.nn.Sequential(
                feature_extractor,
                torch.nn.AdaptiveAvgPool2d(output_size=(1, 1)),
                torch.nn.Flatten(),
            )

    def _build_lstm(self):
        return torch.nn.LSTM(
            input_size=self.in_features,
            hidden_size=self.hidden_size,
            num_layers=self.num_lstm_layer,
            batch_first=True,
            dropout=0.05
        )

    def _build_head(self):
        return torch.nn.Sequential(
            torch.nn.Flatten(start_dim=1),
            torch.nn.Linear(self.hidden_size * self.num_frame_per_clip, self.num_classes),
            torch.nn.Softmax(dim=1)
        )

    def forward(self, x):
        # input: [B, T, C, W, H]
        if self.fpn:

            features = {'0': [], '1': [], '2': [], '3': [], 'pool': []}
            for s in range(x.size(1)):
                frame_features = self.feature_extractor(x[:, s, :, :, :])
                for k, v in frame_features.items():
                    v = F.adaptive_avg_pool2d(v, output_size=(1, 1))
                    v = self.flatten(v)
                    features[k].append(v)

            fpn_classification = []
            for fpn_feature, lstm, head in zip(features.values(), self.lstm_list, self.head_list):
                fpn_feature = torch.stack(fpn_feature, dim=1)
                output, _ = lstm(fpn_feature)
                output = head(output)

                fpn_classification.append(output)

            outputs_classification = torch.stack(fpn_classification, dim=1)

            # outputs_classification = outputs_classification.permute((0, 2, 1))
            # outputs_classification = F.avg_pool1d(outputs_classification, kernel_size=5)
            # outputs_classification = outputs_classification.permute((0, 2, 1))
            # outputs_classification = self.flatten(outputs_classification)

            return self.classification_head_fpn(outputs_classification)

        else:
            features = []
            for s in range(x.size(1)):
                features.append(self.feature_extractor(x[:, s, :, :, :]))
            features = torch.stack(features, dim=1)
            x, _ = self.lstm(features)
            x = self.head(x)
            return x

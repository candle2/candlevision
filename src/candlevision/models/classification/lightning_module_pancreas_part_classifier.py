from abc import ABC
from typing import List

import torch
import torchmetrics
import numpy as np
import pytorch_lightning as pl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import torchvision

from candlevision.models.backbones.torchvision_backbones import create_torchvision_backbone
from candlevision.models.classification.lstm_classifier import LSTMClassifier


class PancreasPartsClassifier(pl.LightningModule):

    def __init__(self,
                 name_classes: List[str],
                 patient_val_ids: List[int],
                 model: torch.nn.Module,
                 weight_classes: torch.Tensor = None,
                 patient_test_ids: List[int] = None,
                 learning_rate: float = 0.001,
                 weight_decay: float = 0.001,
                 nb_epochs: int = 20,
                 type_average: str = "macro"):
        super(PancreasPartsClassifier, self).__init__()

        self.model = model
        self.patient_val_ids = patient_val_ids
        self.patient_test_ids = patient_test_ids
        self.name_classes = name_classes
        self.num_classes = len(name_classes)
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        self.nb_epochs = nb_epochs
        self.criterion = torch.nn.CrossEntropyLoss(weight=self.weight_classes) if weight_classes else torch.nn.CrossEntropyLoss()

        # global metrics
        self.val_frame_wise_accuracy = torchmetrics.Accuracy(num_classes=self.num_classes, average=type_average)
        self.max_val_frame_wise_accuracy = 0
        self.val_confusion_matrix = torchmetrics.ConfusionMatrix(num_classes=self.num_classes, normalize='true')

        self.test_frame_wise_accuracy = torchmetrics.Accuracy(num_classes=self.num_classes, average=type_average)
        self.test_frame_wise_precision = torchmetrics.Precision(num_classes=self.num_classes, average=type_average)
        self.test_frame_wise_recall = torchmetrics.Recall(num_classes=self.num_classes, average=type_average)

        self.test_confusion_matrix = torchmetrics.ConfusionMatrix(num_classes=self.num_classes, normalize='true')

        # per patient metrics
        self.val_metrics_per_patient = {id_patient: torchmetrics.Accuracy(num_classes=self.num_classes, average=type_average) for id_patient in self.patient_val_ids}
        self.max_val_patient_wise_accuracy = 0
        if self.patient_test_ids is not None:
            self.test_metrics_per_patient = {id_patient: torchmetrics.Accuracy(num_classes=self.num_classes, average=type_average) for id_patient in self.patient_test_ids}

    def training_step(self, batch, batch_idx):
        images, labels, _ = batch
        images, labels = torch.stack(images), torch.stack(labels)

        output = self.model(images)

        loss = self.criterion(output, labels)

        self.log("loss", loss)

        return loss

    def training_epoch_end(self, outputs) -> None:
        self.log("trainable_parameters", sum(p.numel() for p in self.parameters() if p.requires_grad))

    def validation_step(self, batch, batch_idx):
        images, labels, patient_ids = batch
        images, labels = torch.stack(images), torch.stack(labels)

        output = self.model(images)

        self.val_frame_wise_accuracy(output, labels)
        self.val_confusion_matrix(output, labels)

        # update metrics per patient
        patient_ids = np.array(patient_ids)
        for patient_val_id in self.patient_val_ids:
            idx = np.where(patient_ids == patient_val_id)[0]
            if len(idx) != 0:
                self.val_metrics_per_patient[patient_val_id].update(output[idx].cpu(), labels[idx].cpu())

    def validation_epoch_end(self, outputs):
        val_frame_wise_acc = self.val_frame_wise_accuracy.compute()
        self.val_frame_wise_accuracy.reset()
        self.log("val_frame_wise_accuracy", val_frame_wise_acc)
        self.max_val_frame_wise_accuracy = max(self.max_val_frame_wise_accuracy, val_frame_wise_acc.item())
        self.log("max_val_frame_wise_accuracy", self.max_val_frame_wise_accuracy)

        # log metrics per patient
        average_val_accuracy_per_patient = []
        for patient_val_id in self.patient_val_ids:
            value = self.val_metrics_per_patient[patient_val_id].compute()
            average_val_accuracy_per_patient.append(value)
            self.log(f"PATIENT/accuracy_patient_n°{patient_val_id}", value)
            self.val_metrics_per_patient[patient_val_id].reset()

        self.log("val_average_accuracy_per_patient", np.mean(average_val_accuracy_per_patient))
        self.max_val_patient_wise_accuracy = max(self.max_val_patient_wise_accuracy, np.mean(average_val_accuracy_per_patient))
        self.log("max_val_patient_wise_accuracy", self.max_val_patient_wise_accuracy)

        # CONFUSION MATRIX
        # Set up plot
        fig = plt.figure()
        ax = fig.add_subplot(111)
        result_confusion_matrix = self.val_confusion_matrix.compute().cpu().numpy()
        cax = ax.matshow(result_confusion_matrix)
        fig.colorbar(cax)

        # Set up axes
        ax.set_xticklabels([''] + self.name_classes, rotation=90)
        ax.set_yticklabels([''] + self.name_classes)

        # Force label at every tick
        ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
        ax.yaxis.set_major_locator(ticker.MultipleLocator(1))

        # display value for each cell in the confusion matrix
        for (i, j), z in np.ndenumerate(result_confusion_matrix):
            ax.text(j, i, '{:0.2f}'.format(z), ha='center', va='center', fontsize=12, color='black')

        plt.tight_layout()

        self.logger.experiment.add_figure("Val Confusion matrix", fig, self.current_epoch)
        self.val_confusion_matrix.reset()

    def test_step(self, batch, batch_idx):
        images, labels, patient_ids = batch
        images, labels = torch.stack(images), torch.stack(labels)

        output = self.model(images)

        self.test_frame_wise_accuracy(output, labels)
        self.test_frame_wise_precision(output, labels)
        self.test_frame_wise_recall(output, labels)
        self.test_confusion_matrix(output, labels)

        # update metrics per patient
        patient_ids = np.array(patient_ids)
        for patient_test_id in self.patient_test_ids:
            idx = np.where(patient_ids == patient_test_id)[0]
            if len(idx) != 0:
                self.test_metrics_per_patient[patient_test_id].update(output[idx].cpu(), labels[idx].cpu())

    def test_epoch_end(self, outputs):
        test_frame_wise_acc = self.test_frame_wise_accuracy.compute()
        test_frame_wise_pre = self.test_frame_wise_precision.compute()
        test_frame_wise_rec = self.test_frame_wise_recall.compute()
        self.test_frame_wise_accuracy.reset()
        self.test_frame_wise_precision.reset()
        self.test_frame_wise_recall.reset()
        self.log("test_frame_wise_accuracy", test_frame_wise_acc)
        self.log("test_frame_wise_precision", test_frame_wise_pre)
        self.log("test_frame_wise_recall", test_frame_wise_rec)

        # log metrics per patient
        average_test_accuracy_per_patient = []
        for patient_test_id in self.patient_test_ids:
            value = self.test_metrics_per_patient[patient_test_id].compute()
            average_test_accuracy_per_patient.append(value)
            self.log(f"PATIENT/accuracy_patient_n°{patient_test_id}", value)
            self.test_metrics_per_patient[patient_test_id].reset()

        self.log("average_test_accuracy_per_patient", np.mean(average_test_accuracy_per_patient))

        # CONFUSION MATRIX
        # Set up plot
        fig = plt.figure()
        ax = fig.add_subplot(111)
        result_confusion_matrix = self.test_confusion_matrix.compute().cpu().numpy()
        cax = ax.matshow(result_confusion_matrix, cmap='Wistia')
        fig.colorbar(cax)

        # Set up axes
        ax.set_xticklabels([''] + self.name_classes, rotation=90)
        ax.set_yticklabels([''] + self.name_classes)

        # Force label at every tick
        ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
        ax.yaxis.set_major_locator(ticker.MultipleLocator(1))

        # display value for each cell in the confusion matrix
        for (i, j), z in np.ndenumerate(result_confusion_matrix):
            ax.text(j, i, '{:0.2f}'.format(z), ha='center', va='center', fontsize=12, color='black')
        plt.tight_layout()
        self.logger.experiment.add_figure("Test Confusion matrix", fig, self.current_epoch)
        self.test_confusion_matrix.reset()

    def configure_optimizers(self):
        parameters = list(self.parameters())
        trainable_parameters = list(filter(lambda p: p.requires_grad, parameters))
        optimizer = torch.optim.Adam(
            params=trainable_parameters,
            lr=self.learning_rate,
            weight_decay=self.weight_decay
        )

        lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer=optimizer, T_max=self.nb_epochs)

        return [optimizer], [lr_scheduler]

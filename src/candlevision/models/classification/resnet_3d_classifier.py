import torch
import pytorchvideo.models.resnet


class Resnet3D(torch.nn.Module):
    def __init__(self, model_num_class: int = 6, pretrained: bool = True):
        super().__init__()
        self.model_num_class = model_num_class
        self.pretrained = pretrained

        if self.pretrained:
            self.model = torch.hub.load("facebookresearch/pytorchvideo", model="slow_r50", pretrained=True)
            in_features = self.model.blocks[-1].proj.in_features
            self.model.blocks[-1].proj = torch.nn.Linear(in_features, out_features=self.model_num_class)
        else:
            self.model = pytorchvideo.models.resnet.create_resnet(
                model_depth=50,
                model_num_class=self.model_num_class,
            )

    def forward(self, x):
        x = x.permute((0, 2, 1, 3, 4))
        return self.model(x)


class SlowFast(torch.nn.Module):
    def __init__(self, model_num_class: int, pretrained: bool = True, ratio_fast_slow: int = 4):
        super().__init__()
        self.model_num_class = model_num_class
        self.pretrained = pretrained
        self.ratio_fast_slow = ratio_fast_slow
        self.model = pytorchvideo.models.slowfast.create_slowfast()
        state_dict = torch.hub.load_state_dict_from_url(
            "https://dl.fbaipublicfiles.com/pytorchvideo/model_zoo/ssv2/SLOWFAST_8x8_R50.pyth")
        self.model.load_state_dict(state_dict["model_state"])
        in_features = self.model.blocks[-1].proj.in_features
        self.model.blocks[-1].proj = torch.nn.Linear(in_features, out_features=self.model_num_class)

    def forward(self, x):
        x = x.permute((0, 2, 1, 3, 4))
        slow = x[:, :, ::self.ratio_fast_slow, :, :]
        return self.model([slow, x])


class Resnet2Dplus1(torch.nn.Module):
    def __init__(self, model_num_class: int, pretrained: bool = True):
        super().__init__()
        self.pretrained = pretrained
        self.model_num_class = model_num_class
        self.model = torch.hub.load("facebookresearch/pytorchvideo", model="r2plus1d_r50", pretrained=self.pretrained)
        in_features = self.model.blocks[-1].proj.in_features
        self.model.blocks[-1].proj = torch.nn.Linear(in_features, out_features=self.model_num_class)

    def forward(self, x):
        x = x.permute((0, 2, 1, 3, 4))
        return self.model(x)


class X3D(torch.nn.Module):
    def __init__(self, model_num_class: int, pretrained: bool = True):
        super().__init__()
        self.pretrained = pretrained
        self.model_num_class = model_num_class
        self.model = torch.hub.load("facebookresearch/pytorchvideo", model="x3d_l", pretrained=self.pretrained)
        in_features = self.model.blocks[-1].proj.in_features
        self.model.blocks[-1].proj = torch.nn.Linear(in_features, out_features=self.model_num_class)

    def forward(self, x):
        x = x.permute((0, 2, 1, 3, 4))
        return self.model(x)


if __name__ == '__main__':
    t = torch.randn((2, 32, 3, 224, 224))
    model = SlowFast(model_num_class=8)
    print(model(t).size())

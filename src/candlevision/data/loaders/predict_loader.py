import torch

from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms
from typing import List


def _collate_fn(batch: List[torch.Tensor]) -> tuple:
    return tuple(zip(*batch))


class PredictDetectEusDataModule(LightningDataModule):
    """ Data module for prediction pipelines.

    """

    def __init__(self,
                 predict_dataset: Dataset = None,
                 num_classes: int = 2,
                 num_workers: int = 0,
                 normalize: bool = False,
                 resize: tuple = (600, 600),
                 batch_size: int = 2,
                 pin_memory: bool = False,
                 drop_last: bool = False,
                 ) -> None:
        """
        :param int num_workers: how many workers to use for loading data
        :param bool normalize: if true applies image normalization
        :param tuple resize: set the size at which the images will be resize
        :param int batch_size: how many samples per batch to load
        :param bool pin_memory: if true, the data loader will copy Tensors into CUDA pinned memory before returning them
        :param bool drop_last: if true, drops the last incomplete batch
        """

        super().__init__()
        self.predict_dataset = predict_dataset
        self.num_classes = num_classes
        self.num_workers = num_workers
        self.normalize = normalize
        self.resize = resize
        self.batch_size = batch_size
        self.pin_memory = pin_memory
        self.drop_last = drop_last

        self.predict_dataset.transform = self.predict_transforms


    @property
    def predict_transforms(self):
        if self.normalize:
            return transforms.Compose([
                transforms.Resize((self.resize[0], self.resize[1])),
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.559, 0.574, 0.566], std=[0.393, 0.395, 0.399])])
        else:
            return transforms.Compose([
                transforms.Resize((self.resize[0], self.resize[1])),
                transforms.ToTensor()])

    def predict_dataloader(self):
        """ The predict dataloader """
        return DataLoader(dataset=self.predict_dataset, num_workers=self.num_workers)

    def _data_loader(self, dataset: Dataset, shuffle: bool = False) -> DataLoader:
        """ Create DataLoader from Dataset

        :param torch.utils.data.Dataset dataset: the dataset
        :param bool shuffle: if true the dataset will be shuffle
        :return: a dataloader
        """
        return DataLoader(dataset=dataset,
                          batch_size=self.batch_size,
                          shuffle=shuffle,
                          num_workers=self.num_workers,
                          drop_last=self.drop_last,
                          pin_memory=self.pin_memory,
                          collate_fn=_collate_fn)

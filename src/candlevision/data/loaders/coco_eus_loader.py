import torch
import albumentations as A
from albumentations.core.composition import PerChannel
from albumentations.pytorch.transforms import ToTensorV2
from torch.utils.data import DataLoader, Dataset
from candlevision.models.detection.my_transforms import Rescale_0_1
from .coco_loader import DetectDataModule
from candlevision.data.dataset import ApeusDataset


class DetectEusDataModule(DetectDataModule):
    """ LightDataModule to handle the APEUS data"""

    def __init__(self,
                 train_dataset: ApeusDataset = None,
                 val_dataset: ApeusDataset = None,
                 num_workers: int = 0,
                 normalize: bool = False,
                 resize: tuple = (600, 600),
                 batch_size: int = 2,
                 pin_memory: bool = False,
                 drop_last: bool = False,
                 subfolder: bool = False
                 ):
        super().__init__(train_dataset, val_dataset, num_workers, normalize,
                         resize, batch_size, pin_memory, drop_last, subfolder)

        self.patient_ids = self.val_dataset.patient_ids()
        self.num_patients = self.val_dataset.num_patients()

    @property
    def train_transforms(self):
        if self.normalize:
            return A.Compose([
                A.Resize(self.resize[0], self.resize[1]),
                A.HorizontalFlip(p=0.5),
                A.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225], max_pixel_value=255.0),
                ToTensorV2(),
                # Rescale_0_1()
            ],
                bbox_params=A.BboxParams(format='coco', label_fields=['category_ids', 'patient_id']))
        else:
            return A.Compose([
                A.Resize(self.resize[0], self.resize[1]),
                A.HorizontalFlip(p=0.5),
                A.OneOf([
                    A.Sequential([
                        A.MultiplicativeNoise(always_apply=True),
                        A.IAASharpen(always_apply=True),
                        A.IAASuperpixels(p_replace=0.03, n_segments=25, always_apply=True),
                    ]),
                    A.Sequential([
                        A.ToGray(always_apply=True),
                        A.CLAHE(clip_limit=2, always_apply=True),
                    ]),
                    A.Sequential([
                        A.IAAEmboss(alpha=(0.5, 0.8), p=1),
                        A.Cutout(num_holes=64, max_h_size=8, max_w_size=8, p=1),
                    ]),
                    A.Equalize(p=1),
                    A.Solarize(always_apply=True)
                ], p=0.8),

                ToTensorV2(),
                Rescale_0_1(),
            ],
                bbox_params=A.BboxParams(format='coco', label_fields=['category_ids']))

    @property
    def val_transforms(self):
        if self.normalize:
            return A.Compose([
                A.Resize(self.resize[0], self.resize[1]),
                # for APEUS : mean=(0.559, 0.574, 0.566), std=(0.393, 0.395, 0.399)
                A.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225], max_pixel_value=255.0),
                ToTensorV2(),
                # Rescale_0_1()
            ],
                bbox_params=A.BboxParams(format='coco', label_fields=['category_ids']))
        else:
            return A.Compose([
                A.Resize(self.resize[0], self.resize[1]),
                ToTensorV2(),
                Rescale_0_1()
            ],
                bbox_params=A.BboxParams(format='coco', label_fields=['category_ids']))

    def _data_loader(self, dataset: Dataset, shuffle: bool = False) -> DataLoader:
        """ Create DataLoader from Dataset

        :param torch.utils.data.Dataset dataset: the dataset
        :param bool shuffle: if true the dataset will be shuffle
        :return: a dataloader
        """
        return DataLoader(dataset=dataset,
                          batch_size=self.batch_size,
                          shuffle=shuffle,
                          num_workers=self.num_workers,
                          drop_last=self.drop_last,
                          pin_memory=self.pin_memory,
                          collate_fn=self._collate_fn)

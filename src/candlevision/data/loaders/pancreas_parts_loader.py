import torch
from typing import List
from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader, Dataset

from candlevision.data.dataset import PancreasPartsDataset


class PancreasPartsDataModule(LightningDataModule):
    def __init__(self,
                 train_dataset: PancreasPartsDataset,
                 val_dataset: PancreasPartsDataset,
                 test_dataset: PancreasPartsDataset = None,
                 num_workers: int = 0,
                 batch_size: int = 2,
                 pin_memory: bool = False,
                 drop_last: bool = False,
                 ):
        super().__init__()
        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.test_dataset = test_dataset
        self.num_workers = num_workers
        self.batch_size = batch_size
        self.pin_memory = pin_memory
        self.drop_last = drop_last

    def train_dataloader(self) -> DataLoader:
        """ The Train dataloader """
        return self._data_loader(self.train_dataset, shuffle=True)

    def val_dataloader(self) -> DataLoader:
        """ The validation dataloader """
        return self._data_loader(self.val_dataset, shuffle=False)

    def test_dataloader(self) -> DataLoader:
        """ The test dataloader """
        return self._data_loader(self.test_dataset, shuffle=False)

    def _data_loader(self, dataset: Dataset, shuffle: bool = False) -> DataLoader:
        """ Create DataLoader from Dataset

        :param torch.utils.data.Dataset dataset: the dataset
        :param bool shuffle: if true the dataset will be shuffle
        :return: a dataloader
        """
        return DataLoader(dataset=dataset,
                          batch_size=self.batch_size,
                          shuffle=shuffle,
                          num_workers=self.num_workers,
                          drop_last=self.drop_last,
                          pin_memory=self.pin_memory,
                          collate_fn=self._collate_fn)

    @staticmethod
    def _collate_fn(batch: List[torch.Tensor]) -> tuple:
        return tuple(zip(*batch))

import cv2
import torch
import random
import albumentations
import numpy as np
import pandas as pd
from pathlib import Path
from typing import Union, List

from candlevision.models.classification.transform_video import VideoTransform
from candlevision.models.self_supervised.simclr import ImageTransformer


class PancreasPartsDataset(torch.utils.data.Dataset):
    """
    Load the data for a classification task.

    This class assumes that the data are structured as follows:

    .. code-block:: bash

        path_save_dir
        ├── _ patient_1
        |   ├── _ class_1
        |       ├── _ clip_1
        |           ├── frame_1.png
        |           ├── ...
        |           └── frame_2.png
        |       ├── _ clip_2
        |       └── _ clip_3
        |   ├── _ class_2
        |   └── _ class_4
        |
        └── _ patient_2
            ├── _ class_2
            ├── _ class_4
            └── _ class_6

    It will return a transform image, a label and a patient_id

    """

    def __init__(self, path_images_dir: str,
                 transform: Union[None, albumentations.Compose, ImageTransformer],
                 patient_filter: list = None, ):
        """

        :param path_images_dir: path to path save dir
        :param patient_filter: list of patient ids to ignore
        :param transform: transform to apply to the image
        """
        self.path_images_dir = path_images_dir
        self.patient_filter = patient_filter
        self.transform = transform
        self.dataset = self.correct_dataset(self.load_dataset())
        self.classes = sorted([c for c in self.dataset.classes.unique()])
        self.num_classes = len(self.classes)
        self.classes_to_labels = self.get_classes_to_labels()
        self.labels_to_classes = self.get_labels_to_labels()
        self.patient_ids = self.dataset.patient_ids.unique()

        # weight values for each classes to give to CrossEntropyLoss to correct the unbalance dataset
        # order : [Liver, artefacts, body, elastography, endoscopy-view, head, other, tail]
        self.weight_classes = None #torch.tensor([3, 0.5, 2, 0.5, 0.5, 2, 0.5, 4])
        # 'liver': 2.25, 'body': 0.5, 'elastography': 0.5, 'endo': 0.5, 'head': 0.5, 'tail': 1.75,

        if self.num_classes != 6:
            raise Exception(
                f"This dataset must have 6 classes: {self.num_classes} have been found. Please edit correct_dataset "
                f"method "
            )

    @classmethod
    def init_before_split(cls, path_images_dir: str):
        return cls(path_images_dir, None)

    def load_dataset(self):
        """ Return a dataframe containing the path, the label and the patient_id of each images """
        path_images = []
        classes = []
        patient_ids = []
        clips = []
        for dir_patient in Path(self.path_images_dir).iterdir():

            if self.patient_filter and int(dir_patient.stem) in self.patient_filter:
                continue

            for path in sorted(dir_patient.glob("*/*/*.png")):
                split_path = str(path).split("/")
                path_images.append(path)
                clips.append(split_path[-2])
                classes.append(split_path[-3])
                patient_ids.append(int(split_path[-4]))

        dict_data = {
            "path_images": path_images,
            "classes": classes,
            "patient_ids": patient_ids,
            "clips": clips
        }
        return pd.DataFrame(data=dict_data)

    @staticmethod
    def correct_dataset(dataframe: pd.DataFrame) -> pd.DataFrame:
        """ Apply several post-processing """
        dataframe.replace("endo-view", "endoscopy-view", inplace=True)
        dataframe.drop(dataframe[dataframe.classes == 'mediastin'].index, inplace=True)
        dataframe.drop(dataframe[dataframe.classes == 'Contrast'].index, inplace=True)
        dataframe.drop(dataframe[dataframe.classes == 'elastography'].index, inplace=True)
        dataframe.drop(dataframe[dataframe.classes == 'endoscopy-view'].index, inplace=True)
        dataframe.sort_values(by=["path_images"], inplace=True)
        dataframe.reset_index(inplace=True)
        dataframe.drop(columns=["index"], inplace=True)

        return dataframe

    def get_classes_to_labels(self):
        """ Return a dictionary with classes(str): labels(int) """
        classes_to_labels = {}
        for i, c in enumerate(self.classes):
            classes_to_labels[c] = i

        return classes_to_labels

    def get_labels_to_labels(self):
        labels_to_classes = {}
        for key, item in self.classes_to_labels.items():
            labels_to_classes[item] = key

        return labels_to_classes

    def __len__(self):
        """ Return the length of the dataset, ie the number of images """
        return len(self.dataset)

    def __getitem__(self, item):
        data = self.dataset.iloc[item]
        image = cv2.imread(str(data.path_images))
        label = torch.tensor(self.classes_to_labels[data.classes])
        patient_id = data.patient_ids

        return self.transform(image)['image'], label, patient_id


class VideoPancreasPartsDataset(PancreasPartsDataset):
    """
    This dataset requires the same structure as PancreasPartsDataset.
    But contrary to PancreasPartsDataset, this dataset returns a list of consecutive frames
    """

    def __init__(self,
                 num_frame_per_clip: int,
                 path_images_dir: str,
                 transform: Union[None, albumentations.Compose, VideoTransform],
                 patient_filter: list = None):
        """

        :param num_frame_per_clip: number of frame to gather in each clip | if there are less frames available than the
        number stated, black frame will be added for a padding purpose.
        :param path_images_dir: path to path save dir
        :param patient_filter: list of patient ids to ignore
        :param transform: transform to apply to the image
        """
        super().__init__(path_images_dir, transform, patient_filter)
        self.num_frame_per_clip = num_frame_per_clip
        self.dataset["clip_ids"] = self.reindex_clips()

        if self.num_frame_per_clip <= 1:
            raise ValueError(f"num_frame_per_clip must be superior to 1 !")

    def __len__(self):
        """ the number of clip in the dataset """
        return int(self.dataset.groupby(["patient_ids", "classes"])["clips"].nunique().sum())

    def reindex_clips(self) -> List[int]:
        """ Return a list of indexes for each clip """
        new_clip_ids = [0]
        prov_clip_idx = 0
        previous_label = self.dataset["classes"][0]
        previous_patient_id = self.dataset["patient_ids"][0]
        previous_clip = self.dataset["clips"][0]

        for label, patient_id, clip in zip(self.dataset["classes"][1:],
                                           self.dataset["patient_ids"][1:],
                                           self.dataset["clips"][1:]):
            if patient_id == previous_patient_id and clip == previous_clip and label == previous_label:
                previous_label, previous_patient_id, previous_clip = label, patient_id, clip
                new_clip_ids.append(prov_clip_idx)

            else:
                prov_clip_idx += 1
                new_clip_ids.append(prov_clip_idx)
                previous_label, previous_patient_id, previous_clip = label, patient_id, clip

        return new_clip_ids

    def __getitem__(self, item):
        data_clip = self.dataset[self.dataset["clip_ids"] == item]
        frames = []
        label = torch.tensor(self.classes_to_labels[data_clip.iloc[0].classes])
        patient_id = data_clip.iloc[0].patient_ids

        if len(data_clip) > self.num_frame_per_clip:
            n = random.randint(0, len(data_clip) - self.num_frame_per_clip)
            data_clip = data_clip[n:(n + self.num_frame_per_clip)]

            for path in data_clip["path_images"]:
                frames.append((cv2.imread(str(path))))

        else:
            num_missing_frames = self.num_frame_per_clip - len(data_clip)
            for path in data_clip["path_images"]:
                frames.append((cv2.imread(str(path))))

            size_frame = frames[0].shape
            for _ in range(num_missing_frames):
                frames.append(np.zeros(size_frame))

        additional_targets = {f"image{k}": frame for k, frame in enumerate(frames[1:])}
        transformed = self.transform(image=frames[0], **additional_targets)
        transform_frames = [transformed['image']]
        for key in additional_targets.keys():
            transform_frames.append(transformed[key])

        return torch.stack(transform_frames), label, patient_id


# class PredictPancreasPartsDataset(torch.utils.data.Dataset):
#     def __init__(self, path_frames: list, transform):
#         self.transform = transform
#         self.path_frames = path_frames
#
#     def __len__(self):
#         return 1
#
#     def __getitem__(self, item):
#         frames = []
#         for path_frame in self.path_frames:
#             frames.append((cv2.imread(str(path_frame))))
#
#         additional_targets = {f"image{k}": frame for k, frame in enumerate(frames[1:])}
#         transformed = self.transform(image=frames[0], **additional_targets)
#         transform_frames = [transformed['image']]
#         for key in additional_targets.keys():
#             transform_frames.append(transformed[key])
#
#         return torch.stack(transform_frames)

class PredictPancreasPartsDataset(torch.utils.data.Dataset):
    def __init__(self, frames: List[np.array], transform):
        self.transform = transform
        self.frames = frames

    def __len__(self):
        return 1

    def __getitem__(self, item):
        additional_targets = {f"image{k}": frame for k, frame in enumerate(self.frames[1:])}
        transformed = self.transform(image=self.frames[0], **additional_targets)
        transform_frames = [transformed['image']]
        for key in additional_targets.keys():
            transform_frames.append(transformed[key])

        return torch.stack(transform_frames)




if __name__ == '__main__':
    transform = VideoTransform(
        num_frame_per_clip=8,
        spatial_transform=False,
        pixel_level_transform=False,
    )
    pp_dataset = VideoPancreasPartsDataset(
        num_frame_per_clip=8,
        path_images_dir="/scratch/APEUS_DATA/pancreas_parts/dataset_v3",
        # patient_filter=[268, 274, 282, 255, 270, 264],
        transform=transform

    )

    dataset = pp_dataset.dataset
    frame_per_clip = dataset.groupby(["patient_ids", "classes", "clips"]).count()
    # print(frame_per_clip)
    duration_average_clip = np.mean(np.array([k for k in frame_per_clip["clip_ids"]]))
    print(duration_average_clip*5/30)

#     images, label, patient = pp_dataset.__getitem__(22)
#     print(f"{label=}")
#     print(f"{patient=}")
#     print(images.size())
    # print(sub.count())
    # print(pp_dataset.classes)
    # print(pp_dataset.num_classes)
    # print(pp_dataset.patient_ids)
    # print(pp_dataset.__len__())
    # print(pp_dataset.classes_to_labels)
    # print(pp_dataset.labels_to_classes)
    # row = pp_dataset.dataset.iloc[0]
    # print(row.path_images)
    # print(row.classes)
    # print(row.patient_ids)

    # print(pp_dataset.__getitem__(3025))

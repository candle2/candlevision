from torch.utils.data import DataLoader, random_split
import torch
import pytorch_lightning as pl
from pathlib import Path
from torch.utils.data import Dataset, Sampler
from PIL import Image
from torchvision import transforms
from itertools import groupby
import re
import random
from tqdm import tqdm

class videosampler(Sampler):
    def __init__(self, data_source, batch_size=3, drop_last=True):
        self.data_source = data_source
        self.batch_size = batch_size
        self.drop_last = drop_last
        self.groups = self.group_frames_per_images()
        print(f"n group: {len(self.groups)}, {len(self.groups)*self.batch_size}")
        
        # for g in self.groups:
            # for v in g:
            #     print(v)
            # print("\n==\n")

    def __iter__(self):
        random.shuffle(self.groups)
        for group in self.groups:
            # select a random subset of image of the desired size.
            random_index = random.randint(0, len(group)-self.batch_size)
            batch = group[random_index:random_index+self.batch_size]
            
            # print("****")
            # print(batch)

            yield from batch

    def __len__(self):

        return len(self.groups)


    def group_frames_per_images(self):
        """group frames of the dataset based on the videos they come from.
        video must be in the path of the image like so: patient_numvideo_idframe.png

        Returns:
            List: list of list of ordered frame
        """
        print("grouping..")
        
        images_path = []
        dict_image = {}
        
        
        for tensor_image, image_path in tqdm(self.data_source, desc="grouping by video name"):
            # store images path in list
            images_path.append(image_path)
            
            # find video name
            video_name = str(Path(image_path).name)
            video_name = "_".join(video_name.split("_")[:-1])
            
            # create list if video name not already referenced
            if video_name not in dict_image:
                dict_image[video_name] = []
                
            dict_image[video_name].append(image_path)

        # turn dictionnary of list in list of list
        collect_list = []
        for key in tqdm(dict_image, desc="converting to indexs"):
            #assert len(dict_image[key]) >= self.batch_size, f"video {key} does does't containt enough frames ({len(dict_image[key])} < {self.batch_size}) for batchs size! Try reducing the batch size."
            # skip if not enought frames in video to froms batches
            if(len(dict_image[key]) >= self.batch_size):
                continue
            # convert list of video in list of index
            indexs = [images_path.index(frame_path) for frame_path in dict_image[key]]
            
            collect_list.append(indexs)
        print("grouping done.")
        print(len(collect_list))
            
        return collect_list


class SlownessDataset(Dataset):
    def __init__(self, list_imgs):

        self.list_imgs = list_imgs
        
        self.transform = transforms.Compose([
            transforms.Resize((512, 512)),
            transforms.Grayscale(num_output_channels=1),
            transforms.ToTensor(),
            #transforms.Normalize([0.5], [0.5])
            transforms.Normalize([0], [1])
        ])

    def __len__(self):
        return len(self.list_imgs)
    
    def __getitem__(self, idx):
        # Get and load image
        image_path = self.list_imgs[idx]
        image = Image.open(image_path)
        image = self.transform(image)
      
        return image, image_path#Path(image_path).parent




class VideoDataModule(pl.LightningDataModule):

    def __init__(self):
        super().__init__()

        # Dataset configuration
        _DATA_PATH = Path("/DATA/APEUS/images")
        _CLIP_DURATION = 2  # Duration of sampled clip for each video
        self.batch_size = 16
        _NUM_WORKERS = 1  # Number of parallel processes fetching data
        
        def sorted_nicely(list_to_sort: list):
            convert = lambda text: int(text) if text.isdigit() else text
            alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
            return sorted(list_to_sort, key=alphanum_key)
        
        # get paths of ct images
        self.path_imgs = [str(x) for x in _DATA_PATH.glob("**/*.png")]
        #self.path_imgs = random.sample(self.path_imgs, 20000)
        self.path_imgs = sorted_nicely(self.path_imgs)
        
        self.val_path = self.path_imgs[-len(self.path_imgs)//5:]
        self.path_imgs = self.path_imgs[:-len(self.val_path)]
        
    def train_dataloader(self):
        train_dataset = SlownessDataset(self.path_imgs)
        return DataLoader(train_dataset, 
                          self.batch_size,
                          sampler=videosampler(train_dataset, batch_size=self.batch_size))
        
    def val_dataloader(self):
        val_dataset = SlownessDataset(self.val_path)
        return DataLoader(val_dataset, 
                          self.batch_size,
                          sampler=videosampler(val_dataset, batch_size=self.batch_size))
        

if __name__ == "__main__":

    test = VideoDataModule()
    print(test.path_imgs)
    for i in test.path_imgs:
        print(i)
        
    loader = test.train_dataloader()
    
    for n, i in enumerate(loader):
        print(i[1])
        print(i[0].max())
        print(i[0].min())
        print(i[0].mean())
        print(i[0].shape)
        print(n)
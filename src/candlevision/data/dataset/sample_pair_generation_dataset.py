import cv2
import random
import torchvision
import numpy as np
from collections import defaultdict
from pathlib import Path
from typing import Union, List, Tuple, Any
from torch.utils.data import Dataset
from candlevision.utils.utils import eus_layout_crop, my_logger


class ApeusFramesPairSampler:
    """ Sample a positive pair of images from frames.

        Detail of the process:
        - Select a random frames from a video
        - pick up 3 random images from the previous selection (img1, img2, img3).
        - img2 is consider as an anchor
        - Blend img1 and img2 together as well as img3 and img2
        - return the result of the blending

        """

    def __init__(self, range_selection: int = 5, blending_factor: float = 0.3, fps: int = 1):
        """
        :param range_selection: number of frames in which 3 of them will be selected
        :param blending_factor: coefficient to blend a image and the anchor
        :param fps: value at which the frames have been extracted

        .. warning:: range_selection must be an odd number

        """
        self.clip_duration = range_selection
        self.blending_factor = blending_factor
        self.fps = fps

    def select_clip(self, num_frames: int):
        """ return the index of a clip according to the clip duration """
        # Calculate the number of clip
        num_clip = int(num_frames / self.fps) // self.clip_duration
        # Get a random index of clip
        index_clip = random.randint(0, num_clip - 1)
        index_start_frame = int(index_clip * self.clip_duration * self.fps)
        index_end_frame = int((index_clip + 1) * self.clip_duration * self.fps)
        return index_start_frame, index_end_frame

    def select_frames(self, frames: List[str]) -> List:
        """ select 3 frames from a list of images path according to the clip duration """
        # Get the boundaries of the clip
        index_start_frame, index_end_frame = self.select_clip(num_frames=len(frames) - 1)
        # Get the index of the first, the middle and the last frame from the clip
        # index_frames = list(np.random.choice(np.arange(index_start_frame, index_end_frame), size=3, replace=False))
        # index_frames = sorted(index_frames)
        index_frames = [index_start_frame,  index_start_frame + self.clip_duration // 2, index_end_frame]

        selected_frames = []
        for id_frame in index_frames:
            frame = cv2.imread(frames[id_frame])
            selected_frames.append(frame)

        return selected_frames

    def get_positive_pair(self, frames: list) -> Tuple[Any, Any]:
        """ from a list of frames names, select 3 of them and blend the 2nd frame into the two others """
        alpha = self.blending_factor
        beta = (1.0 - alpha)
        image_1, anchor, image_3 = self.select_frames(frames)
        sample_1 = cv2.addWeighted(image_1, alpha, anchor, beta, 0.0)
        sample_2 = cv2.addWeighted(image_3, alpha, anchor, beta, 0.0)

        return sample_1, sample_2


class SamplePairGeneration(Dataset):
    """ Generate a positive pair for self-supervised training

    To work, this class required a specific directories structure.

    .. code-block:: bash

        path_save_dir
        ├── _ procedure_1
        |   ├── _ VID001_part_0
        |       ├── frame_1.png
        |       ├── frame_2.png
        |       └── frame_3.png
        |   ├── _ VID001_part_1
        |   └── _ VID001_part_2
        |
        └── _ procedure_2
          ├── _ VID001_part_0
          ├── _ VID002_part_0
          └── _ VID003_part_0

    """

    def __init__(self,
                 path_dir_data: str = None,
                 transform: torchvision.transforms.Compose = None,
                 positive_pair_sampler: ApeusFramesPairSampler = None,
                 extension_factor: int = 1):
        """

        :param path_dir_data: path of the videos repository
        :param transform: data augmentation applied to the pair of images
        :param positive_pair_sampler: strategy to sample the positive pair
        :param extension_factor: As the length of our dataset corresponds to the number of
        patients, we can't do large batches (len(batch)>56). That's an issue because self-supervised methods require
        large batches to work properly. To solve this issue, we introduce a factor to artificially extend the number of
        patients inside the dataset. extension_factor >= 1.
        """

        self.path_dir_data = Path(path_dir_data)
        self.transform = transform
        self.pair_sampler = positive_pair_sampler
        self.extension_factor = extension_factor
        self.data = self.generate_dict_data()
        self.ids_procedure = self.get_ids_procedure()

    def generate_dict_data(self) -> defaultdict:
        """ Create a dictionary containing all the path to frames
        {
            ID_PROCEDURE_1 (int):
                 {
                    ID_VIDEO_1 (int):
                        {
                            ID_FRAME_1 (int): path_frame_1,
                            ID_FRAME_2 (int): path_frame_2
                        }
                    ID_VIDEO_2 (int): name_video,
                },
        }
        """
        dataset = defaultdict(dict)
        my_logger().info("Dataset => Generating frames path")
        for index_procedure, path_procedure in enumerate(Path(self.path_dir_data).iterdir()):
            if path_procedure.is_dir():
                for index_video, path_video in enumerate(path_procedure.iterdir()):
                    if path_video.is_dir():
                        path_frames = []
                        for path_frame in path_video.glob("*.png"):
                            path_frames.append(path_frame)
                        dataset[index_procedure][index_video] = path_frames

        return dataset

    def get_ids_procedure(self) -> list:
        """ Return the list of patient ids """
        ids_procedure = list(self.data.keys())
        random.shuffle(ids_procedure)
        return ids_procedure * self.extension_factor

    def get_index_video(self, index: int) -> int:
        """ Return a random videos index from a specific procedure """
        videos = self.data[self.ids_procedure[index]]
        return random.randint(0, len(videos) - 1)

    def get_frames(self, index: int):
        """  Return a list of frames from a random video from a procedure """
        return [str(f) for f in self.data[self.ids_procedure[index]][self.get_index_video(index)]]

    def __len__(self):
        return len(self.ids_procedure)

    def __getitem__(self, index: int):
        list_frames = self.get_frames(index)
        img1, img2 = self.pair_sampler.get_positive_pair(list_frames)

        img1, img2 = self.transform(img1), self.transform(img2)
        return img1, img2

# if __name__ == '__main__':
#     pair = ApeusFramesPairSampler()
#     sp = SamplePairGeneration(
#         path_dir_data="/media/apeus/SSL_FRAMES",
#         positive_pair_sampler=pair
#     )
#
#     print(sp.get_index_video(0))
#     print()

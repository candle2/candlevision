import cv2
import albumentations as A
from PIL import Image
from torch.utils.data import Dataset

from candlevision.utils.utils import eus_layout_crop


class PredictVideoDataset(Dataset):

    def __init__(self,
                 path_video: str = None,
                 transform: A.Compose = None):
        self.path_video = path_video
        self.video = cv2.VideoCapture(path_video)
        self.transform = transform

        # initialize crop
        _, frame = self.video.read()
        _, self.slice_height, self.slice_width = eus_layout_crop(frame)

    def __len__(self):
        return int(self.video.get(cv2.CAP_PROP_FRAME_COUNT))

    def __getitem__(self, idx: int = None):
        self.video.set(cv2.CAP_PROP_POS_FRAMES, idx)
        _, frame = self.video.read()
        crop_frame = frame[self.slice_height, self.slice_width, :]
        h, w = crop_frame.shape[:2]
        crop_frame = Image.fromarray(crop_frame)
        transformed = self.transform(crop_frame)
        crop_coordinates = (self.slice_height.start, self.slice_width.start, h, w)
        return transformed, crop_coordinates


class PredictImageDataset(Dataset):

    def __init__(self,
                 path_image: str = None,
                 transform: A.Compose = None,
                 activate_crop: bool = False):
        self.path_video = path_image
        self.image = cv2.imread(path_image)
        self.transform = transform
        self.activate_crop = activate_crop

        if self.activate_crop:
            self.image, self.slice_height, self.slice_width = eus_layout_crop(self.image)

    def __len__(self):
        return 1

    def __getitem__(self, idx: int = None):
        h, w = self.image.shape[:2]
        crop_frame = Image.fromarray(self.image)
        transformed = self.transform(crop_frame)

        if self.activate_crop:
            crop_coordinates = (self.slice_height.start, self.slice_width.start, h, w)

        else:
            crop_coordinates = (0, 0, h, w)

        return transformed, crop_coordinates


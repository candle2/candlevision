import torch
from typing import Tuple
from candlevision.data.dataset.coco_dataset import CocoDataset


class ApeusDataset(CocoDataset):

    def get_patient_id(self, idx: int = None) -> int:
        """ Gather an image and its annotations in a single dictionary

        :param idx: index of an image
        :return: a dict containing an image and the corresponding annotations
        """
        image_info = self.coco.loadImgs(self.image_ids[idx])[0]

        try:
            patient_id = int(image_info['filename'].split('_')[0])

        except ValueError:
            patient_id = int(image_info['filename'].split('_')[0][2:]) + 1000

        return patient_id

    def patient_ids(self) -> list:
        """
        :return: list of patient ids
        """
        # patient_ids = [int(image_info['filename'].split('_')[0]) for image_info in
        #                self.coco.loadImgs(self.coco.getImgIds())]
        patient_ids = []
        for image_info in self.coco.loadImgs(self.coco.getImgIds()):
            patient_id = image_info['filename'].split('_')[0]
            try:
                patient_id = int(patient_id)
                patient_ids.append(patient_id)
            except ValueError:
                patient_id = int(patient_id[2:]) + 1000
                patient_ids.append(patient_id)

        return list(set(patient_ids))

    def num_patients(self) -> int:
        """
        :return: the number of patients
        """
        return len(self.patient_ids())

    def __getitem__(self, idx: int = None) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        """ Gather an image and its annotations in a single dictionary

        :param idx: index of an image
        :return: a dict containing an image and the corresponding annotations
        """
        image = self.load_image(idx)
        target = self.load_annotations(idx)
        bboxes = target[:, :4]
        category_ids = target[:, 4]
        patient_id = [self.get_patient_id(idx) for _ in range(max(len(category_ids), 1))]

        if self.transform:
            # Applied transformation
            transformed = self.transform(image=image, bboxes=bboxes, category_ids=category_ids) # patient_id=patient_id)

            return transformed['image'], transformed['bboxes'], transformed['category_ids'], patient_id #transformed['patient_id']

        else:
            return image, bboxes, category_ids, patient_id
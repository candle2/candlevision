import os
import io
import json
import numpy as np
import pandas as pd
from fpdf import FPDF


class PDF(FPDF):
    def __init__(self,
                 path_data: str,
                 title: str = "APEUS WP2: Evaluation report"):
        super().__init__()
        self.path_data = path_data
        self.title = title

        # Read json containing the data from the evaluation
        with open(self.path_data + "evaluation_results.json") as json_file:
            self.data = json.load(json_file)

    def header(self):
        # Add Camma logo
        self.image(
            name=os.getcwd()[:-10] + "configs/logo/CammaLogo.jpg",
            w=30,
        )
        # Add IHU logo
        self.image(
            name=os.getcwd()[:-10] + "configs/logo/logo_ihu_fr1.jpg",
            w=35,
            x=170,
            y=10
        )
        # Position at 1.2 cm from top
        self.set_y(12)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Text color in gray
        self.set_text_color(128)
        # Page number
        self.cell(0, 10, 'APEUS WP2: evaluation results', 0, 0, 'C')

    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Text color in gray
        self.set_text_color(128)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()), 0, 0, 'C')

    def first_page(self):
        self.add_page()
        self.set_y(45)
        # Arial bold 15
        self.set_font('Arial', 'B', 22)
        # Calculate width of title and position
        w = self.get_string_width(self.title) + 6
        self.set_x((210 - w) / 2)
        # Colors of frame, background and text
        self.set_draw_color(0, 80, 180)
        self.set_fill_color(255, 255, 255)
        self.set_text_color(0, 0, 0)
        # Thickness of frame (1 mm)
        self.set_line_width(1)
        # Title
        self.cell(w, 12, self.title, 1, 1, 'C', 1)
        # Line break
        self.ln(10)

        global_map = self._compute_mAP()

        self.set_font('Arial', 'B', 14)
        # Colors of frame and thickness
        self.set_draw_color(0, 80, 180)
        self.set_line_width(0.5)

        self.cell(
            w=76,
            h=10,
            txt=f"GLOBAL metrics (patient-wise)",
            border=1,
            ln=1,
            align='L',
        )
        self.ln()
        self.set_font('Arial', 'B', 12)
        self.cell(
            w=30,
            h=10,
            txt=f"mAP: {np.around(global_map, decimals=2)}",
            border=0,
            ln=0,
        )
        self.ln()

        # get the name of the label from the json file
        labels = [label[3:] for label in list(self.data[list(self.data.keys())[0]].keys()) if 'AP_' in label]

        for label in labels:
            self.set_font('Arial', 'BU', 13)
            self.cell(
                w=40,
                h=10,
                txt=f"label: {label}",
                border=0,
                ln=1,
            )

            label_results = self._compute_label_metrics(label)
            self.set_font('Arial', 'B', 12)
            for key, value in label_results.items():
                self.cell(
                    w=40,
                    h=10,
                    txt=f"{key}: {np.round(value, decimals=2)}",
                    border=0,
                    ln=0,
                )
            self.ln()

    def _compute_mAP(self):
        """ Compute the mA (patient-wise) """
        global_map = []
        for data_patient in self.data.values():
            global_map.append(data_patient["mAP"])

        global_map = np.array(global_map)

        return global_map.mean()

    def _compute_label_metrics(self, label: str = 'lesion'):
        """ Compute the AP, precision, sensitivy and f1-score for a given label (patient-wise) """

        global_ap = []
        precision = []
        sensitivity = []
        f1_score = []

        for data_patient in self.data.values():
            results = []
            global_ap.append(data_patient[f'AP_{label}'])

            # extraction of the precision, sensitivity and f1-score from the string report
            data_string = data_patient["report"]
            data = io.StringIO(data_string)
            df = pd.read_csv(data)
            # Get the row index for the corresponding label
            row_index_label = [k for k in range(len(df.index - 3)) if label in df.iloc[k][0]]
            try:
                string_label_result = df.iloc[row_index_label[0]][0].split('  ')
                # Transform string to float
                for value in string_label_result:
                    try:
                        results.append(float(value))
                    except ValueError:
                        continue
            except IndexError:
                results = [0, 0, 0]

            # Check if there is at least one prediction for this label
            if results[-1] != 0:
                precision.append(results[0])
                sensitivity.append(results[1])
                f1_score.append(results[2])

        # Index to remove with the -1 values. Otherwise the average will not return a correct value
        global_ap_lesion = np.array(global_ap)
        id_lesion = global_ap_lesion > -1

        return {
            'AP': global_ap_lesion[id_lesion].mean(),
            'precision': np.array(precision).mean(),
            'sensitivity': np.array(sensitivity).mean(),
            'f1_score': np.array(f1_score).mean(),
        }

    def report_body(self):

        for k, (id_patient, data_patient) in enumerate(self.data.items()):
            if k % 3 == 0:
                self.add_page()
                self.set_y(22)

            self.patient_result(id_patient, data_patient)

    def patient_result(self, id_patient: int, data_patient):

        self.ln()
        self.set_font('Arial', 'B', 16)
        # Colors of frame and thickness
        self.set_draw_color(0, 80, 180)
        self.set_line_width(1)
        self.cell(
            w=40,
            h=10,
            txt=f"PATIENT n°{id_patient}",
            border=1,
            ln=1,
            align='L',
        )

        self.set_font('Arial', size=12)
        self.set_draw_color(0, 0, 0)
        self.set_line_width(0.5)
        self.ln()
        report_position_x, report_position_y = self.get_x(), self.get_y()
        self.rect(report_position_x, report_position_y, 198, 56)
        for keys, values in data_patient.items():
            if keys not in ['fig', 'report']:
                self.cell(
                    w=25,
                    h=8,
                    txt=f"{keys}: ",
                    border=1,
                    ln=0,
                )

                self.cell(
                    w=12,
                    h=8,
                    txt=f"{values:.2f}",
                    border=1,
                    ln=0
                )
            elif keys == 'fig':
                self.image(
                    name=self.path_data + f"PR_curve_patient_{id_patient}.jpg",
                    x=self.get_x() + 5,
                    y=self.get_y() + 1,
                    w=80,
                    h=54,
                )

            else:
                self.set_x(report_position_x)
                self.set_y(report_position_y)
                self.ln()
                self.multi_cell(
                    w=111,
                    h=8,
                    txt=values.replace('\n', '         \n') + "         ",
                    border=1,
                    align='R'
                )

    def print_report(self):
        self.first_page()
        self.report_body()


if __name__ == '__main__':
    for k in range(1, 6):
        pdf = PDF(
            path_data=f"/home/afleurentin/Bureau/Benchmark/centernet/centernet_fold_{k}/"
        )
        # pdf._compute_label_metrics('lesion')
        pdf.print_report()
        pdf.output(f'/home/afleurentin/Bureau/Benchmark/centernet/centernet_fold_{k}/results.pdf', 'F')

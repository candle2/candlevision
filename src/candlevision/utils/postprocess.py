import itertools
import torch
import torchvision
import numpy as np
from typing import *
from APEUS_data.EUS_video.Annotation.ms_coco_annotation import CocoAnnotation


def filter_predictions_faster_eus(predictions: List[Dict[str, List[Dict[str, np.array]]]],
                                  name_labels: dict,
                                  threshold_score: float = 0.5,
                                  threshold_nms: float = 0.1) -> Dict[str, List[np.array]]:
    """ Post processing of the predictions from Faster RCNN

    Input bounding boxes format: xyxy | Output bounding boxes format: xyxy

    :param predictions: predictions return by Faster RCNN
    :param name_labels: a dictionary with the names of the labels i.e. {1: "parenchyma", 2: "lesion"}
    :param threshold_score: keep only the predictions with a confidence score higher than this threshold
    :param threshold_nms:
    :return: a dictionary with {"boxes": process_boxes, "labels": process_labels, "scores": process_scores}
    """
    # initialization of the lists which will contain the post process predictions for each frame
    all_frame_bboxes = []
    all_frame_labels = []
    all_frame_scores = []
    all_frame_crop_coordinates = []

    for prediction in predictions:
        boxes = prediction["predictions"][0]['boxes']
        labels = prediction["predictions"][0]['labels']
        scores = prediction["predictions"][0]['scores']
        crop_coordinates = prediction["crop_coordinates"]

        # Applied non maxima suppression
        idxs = torchvision.ops.batched_nms(boxes, scores, labels, threshold_nms)
        # Keep only the predictions with a confidence higher than the threshold
        selected_idxs = scores[idxs] > threshold_score
        idxs = idxs[selected_idxs]

        # convert the category_id to the real name of the label
        labels_sel = [name_labels[k] for k in labels[idxs].cpu().numpy()]
        crop_coordinates = [int(coord[0]) for coord in crop_coordinates]

        all_frame_bboxes.append(boxes[idxs].cpu().numpy())
        all_frame_labels.append(labels_sel)
        all_frame_scores.append(scores[idxs].cpu().numpy())
        all_frame_crop_coordinates.append(crop_coordinates)

    return {"boxes": all_frame_bboxes,
            "labels": all_frame_labels,
            "scores": all_frame_scores,
            "crop_coordinates": all_frame_crop_coordinates}


def convert_bbox_original_size(predictions: Dict[str, np.array],
                               resize_width: int,
                               resize_height: int) -> Dict[str, np.array]:
    """

    :param predictions: a dictionary with the bounding boxes coordinates and the crop coordinates
    :param resize_width: width of the image after transformation when feed the neural network
    :param resize_height: height of the image after transformation when feed the neural network
    :return:
    """
    frame_boxes = predictions["boxes"]
    for i, boxes in enumerate(frame_boxes):
        x1, y1, h, w = predictions["crop_coordinates"][i]
        if len(boxes) != 0:
            for j, box in enumerate(boxes):

                frame_boxes[i][j][0] = box[0] * (w / resize_width) + y1
                frame_boxes[i][j][1] = box[1] * (h / resize_height) + x1
                frame_boxes[i][j][2] = box[2] * (w / resize_width) + y1
                frame_boxes[i][j][3] = box[3] * (h / resize_height) + x1

    return {"boxes": frame_boxes,
            "labels": predictions["labels"],
            "scores": predictions["scores"]}


def save_pred_to_coco(predictions: dict, path_save: str, name_video: str, image_width: int = 0, image_height: int = 0):
    """ Save the predictions in a COCO json file

    :param predictions: a dictionary with {"boxes": process_boxes, "labels": process_labels}
    :param path_save: path where the COCO file will be saved
    :param name_video: name of the original video
    :param image_width: image width
    :param image_height: image
    """
    all_boxes = predictions["boxes"]
    all_labels = predictions["labels"]

    coco = CocoAnnotation()

    # Add the categories
    flatten_labels = list(itertools.chain.from_iterable(all_labels))
    categories = list(set(flatten_labels))  # get unique value from flatten labels
    for category in categories:
        coco.add_category(category)

    # Add images and annotations
    for i, (boxes, labels) in enumerate(zip(all_boxes, all_labels)):
        coco.add_image(width=image_width, height=image_height, filename=f"{name_video}_frame_{i}")

        if len(boxes) != 0:
            for box, label in zip(boxes, labels):
                coco.add_category(label)
                coco.add_annotation(list(torchvision.ops.box_convert(torch.Tensor(box), in_fmt='xyxy', out_fmt='xywh')))

    coco.save(path_save)

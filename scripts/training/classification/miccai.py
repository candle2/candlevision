import os
import hydra
import torch
import warnings
import numpy as np
from pathlib import Path
import pytorch_lightning as pl
from omegaconf import DictConfig
from pytorch_lightning.callbacks import EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger
from sklearn.model_selection import StratifiedGroupKFold

from candlevision.data.dataset import VideoPancreasPartsDataset, PancreasPartsDataset
from candlevision.data.loaders import PancreasPartsDataModule
from candlevision.models.classification.resnet_2d_classifier import BasicClassifier
from candlevision.models.classification.transform_video import VideoTransform
from candlevision.models.classification import PancreasPartsClassifier, LSTMClassifier, MViT, X3D, Resnet2Dplus1, SlowFast, \
    Resnet3D
from candlevision.utils.utils import check_log_exist

warnings.filterwarnings("ignore")

os.environ["CUDA_VISIBLE_DEVICES"] = "1"

TEST_ID_PATIENTS = [257, 202, 271, 274, 281, 250, 253]
VAL_ID_PATIENTS = [85, 264, 266, 269, 272, 275, 276, 248]
TRAIN_ID_PATIENTS = [164, 261, 262, 263, 277, 278, 280, 249, 255, 260, 267, 279, 246, 247, 282, 252, 254, 256, 97, 259, 265, 268, 270, 243, 258]


@hydra.main(config_path="../../configs/classification/", config_name="x3d")
def main(cfg: DictConfig):
    # # Generate the fold
    # data = PancreasPartsDataset.init_before_split(cfg.dataset.path_images_dir).dataset
    # y = data.classes.to_numpy()
    # groups = data.patient_ids.to_numpy()
    # X = np.ones((len(y), 2))
    # folds_generator = StratifiedGroupKFold(n_splits=4)

    # Set up the training for each fold
    # for k, (train_ids, val_ids) in enumerate(folds_generator.split(X, y, groups), start=1):

    # CHECK IF LOG ALREADY EXISTS
    # log_version = check_log_exist(os.path.join(cfg.logger.save_dir, cfg.logger.name),
    #                               cfg.logger.version + f"_fold_{k}")

    backbones = [
        # "efficientnet_b0",
        # "efficientnet_b1",
        # "efficientnet_b2",
        # "resnet18",
        # "resnet26",
        # "resnet34",
        # "convnext_small",
        # "beit_base_patch16_224",
        # "cait_s24_224",
        # "cait_xxs24_224",
        # "cait_xxs36_224",
        # "convit_tiny",
        # "convit_small",
        # "vit_tiny_patch16_224",
        # "swin_tiny_patch4_window7_224",
        # "twins_svt_small",
        # "tnt_s_patch16_224",
        # "xcit_small_12_p8_224",
        # "xcit_tiny_24_p8_224",
        # "deit_tiny_patch16_224",
        # "mvit",
        "x3d",
    ]

    for backbone in backbones:
        # cfg.model.backbone = backbone
        cfg.logger.version = "miccai_v2_" + backbone

        frame_accuracy = []
        frame_precision = []
        frame_recall = []
        for k in range(15):

            log_version = check_log_exist(os.path.join(cfg.logger.save_dir, cfg.logger.name), cfg.logger.version)

            # SETUP TRAINING DATASET
            train_transform = VideoTransform(
                num_frame_per_clip=cfg.dataset.num_frame_per_clip,
                **cfg.augmentation.train
            )

            train_dataset = VideoPancreasPartsDataset(
                patient_filter=TEST_ID_PATIENTS + VAL_ID_PATIENTS,
                transform=train_transform,
                **cfg.dataset
            )

            # SETUP VALIDATION DATASET
            val_transform = VideoTransform(
                num_frame_per_clip=cfg.dataset.num_frame_per_clip,
                **cfg.augmentation.val
            )

            val_dataset = VideoPancreasPartsDataset(
                patient_filter=TEST_ID_PATIENTS + TRAIN_ID_PATIENTS,
                transform=val_transform,
                **cfg.dataset
            )

            # SETUP TEST DATASET
            test_dataset = VideoPancreasPartsDataset(
                path_images_dir=cfg.dataset.path_images_dir,
                patient_filter=VAL_ID_PATIENTS + TRAIN_ID_PATIENTS,
                num_frame_per_clip=cfg.dataset.num_frame_per_clip,
                transform=val_transform
            )

            # CREATE LIGHTNING DATAMODULE
            datamodule = PancreasPartsDataModule(
                train_dataset=train_dataset,
                val_dataset=val_dataset,
                test_dataset=test_dataset,
                **cfg.loader
            )

            # SET UP MODEL
            # lstm_classifier = LSTMClassifier(
            #     num_classes=train_dataset.num_classes,
            #     num_frame_per_clip=cfg.dataset.num_frame_per_clip,
            #     **cfg.model
            # )

            # lstm_classifier = MViT(
            #     model_num_class=train_dataset.num_classes,
            #     name_model='mvit_base_16x4'
            # )

            # lstm_classifier = Resnet3D(
            #     model_num_class=train_dataset.num_classes,
            #     **cfg.model
            # )

            # lstm_classifier = Resnet2Dplus1(
            #     model_num_class=train_dataset.num_classes,
            #     **cfg.model
            # )

            # lstm_classifier = SlowFast(
            #     model_num_class=train_dataset.num_classes,
            #     **cfg.model
            # )

            # lstm_classifier = MViT(
            #     model_num_class=train_dataset.num_classes,
            #     **cfg.model
            # )

            lstm_classifier = X3D(
                model_num_class=train_dataset.num_classes,
                **cfg.model
            )

            checkpoint_path = str([p for p in Path("/home/apeus/PycharmProjects/models/pancreas_parts/miccai/").glob(
                f"{backbone}*")][0])

            state_dict = torch.load(checkpoint_path)['state_dict']
            for key in list(state_dict.keys()):
                state_dict[key.replace('model.model.', 'model.')] = state_dict.pop(key)

            lstm_classifier.load_state_dict(state_dict, strict=True)

            # classifier = BasicClassifier(
            #     num_classes=train_dataset.num_classes,
            #     name_model=cfg.model.backbone,
            #     pretrained=False
            # )

            # CREATE LIGHTNING MODULE
            lightning_module = PancreasPartsClassifier(
                patient_val_ids=val_dataset.patient_ids,
                patient_test_ids=test_dataset.patient_ids,
                weight_classes=train_dataset.weight_classes,
                name_classes=train_dataset.classes,
                model=lstm_classifier,
                nb_epochs=cfg.trainer.max_epochs,
                **cfg.lightning_module,
            )

            # CALLBACKS
            logger = TensorBoardLogger(
                save_dir=cfg.logger.save_dir,
                name=cfg.logger.name,
                version=log_version,
                default_hp_metric=cfg.logger.default_hp_metric,
            )

            # log hparams into Tensorboard
            logger.log_hyperparams(cfg)

            checkpoint_callback = pl.callbacks.ModelCheckpoint(
                filename=f'{log_version}' + '_{epoch}',
                **cfg.callbacks.model_checkpoint
            )

            early_stopping_callback = EarlyStopping(
                monitor=cfg.callbacks.model_checkpoint.monitor,
                patience=int(cfg.trainer.max_epochs * 0.25),
                **cfg.callbacks.early_stopping
            )

            # SETUP TRAINER
            trainer = pl.Trainer(
                callbacks=[checkpoint_callback, early_stopping_callback],
                progress_bar_refresh_rate=50,
                # gradient_clip_val=0.3,
                logger=logger,
                **cfg.trainer
            )

            # trainer.fit(lightning_module, datamodule=datamodule)
            # checkpoint_path = str([p for p in Path("/home/apeus/PycharmProjects/models/pancreas_parts/dataset_v3/").glob(f"basic_{cfg.model.backbone}*")][0])
            results = trainer.test(lightning_module, datamodule=datamodule, verbose=False, ckpt_path=None)
            frame_accuracy.append(results[0]["test_frame_wise_accuracy"])
            frame_precision.append(results[0]["test_frame_wise_precision"])
            frame_recall.append(results[0]["test_frame_wise_recall"])

        frame_accuracy = np.array(frame_accuracy)
        frame_precision = np.array(frame_precision)
        frame_recall = np.array(frame_recall)

        print(f"[INFO] BACKBONE RESULTS: {backbone}")
        print(f"[INFO] frame_accuracy: {np.mean(frame_accuracy)} | std: {np.std(frame_accuracy)}")
        print(f"[INFO] frame_precision: {np.mean(frame_precision)} | std: {np.std(frame_precision)}")
        print(f"[INFO] frame_recall: {np.mean(frame_recall)} | std: {np.std(frame_recall)}")



if __name__ == '__main__':
    main()
    # print("ok")
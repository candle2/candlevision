import os
import hydra
import torch
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pytorch_lightning as pl
import timm
from omegaconf import DictConfig
from pytorch_lightning.loggers import TensorBoardLogger
from sklearn.model_selection import StratifiedGroupKFold

from candlevision.data.dataset import PancreasPartsDataset
from candlevision.data.loaders import PancreasPartsDataModule
from candlevision.models.self_supervised.simclr import ImageTransformer
from candlevision.models.classification import PancreasPartsClassifier, ResnetClassifier, ViT, DeiT, EfficientNet
from candlevision.utils.utils import check_log_exist

os.environ["CUDA_VISIBLE_DEVICES"] = "2"

TEST_ID_PATIENTS = [257, 202, 271, 274, 281, 250, 253]
VAL_ID_PATIENTS = [85, 264, 266, 269, 272, 275, 276, 248]
TRAIN_ID_PATIENTS = [164, 261, 262, 263, 277, 278, 280, 249, 255, 260, 267, 279, 246, 247, 282, 252, 254, 256, 97, 259, 265, 268, 270, 243, 258]


def display_fold_distribution(n_splits: int = 5):
    # Create folds
    # dataset = PancreasPartsDataset.init_before_split("/scratch/APEUS_DATA/pancreas_parts/dataset_v3")
    dataset = PancreasPartsDataset(
        path_images_dir="/scratch/APEUS_DATA/pancreas_parts/dataset_v3",
        # patient_filter=[257, 202, 271, 274, 281, 250, 253],
        transform=None
    )
    print(len(dataset))
    data = dataset.dataset
    y = data.classes.to_numpy()
    print(dataset.classes)
    groups = data.patient_ids.to_numpy()
    X = np.ones((len(y), 2))

    folds_generator = StratifiedGroupKFold(n_splits=n_splits)

    bar_colors = ["blue", "black", "green", "orange", "pink", "yellow", "gray", "red"]

    fig, axs = plt.subplots(1, n_splits, sharey=True, tight_layout=True)
    for k, (_, val_ids) in enumerate(folds_generator.split(X, y, groups)):
        print(f"fold n°{k} | number patients: {len(set(groups[val_ids]))} | list id patient: {set(groups[val_ids])}")
        _, _, patches = axs[k].hist(sorted(y[val_ids]), bins=8)
        for i, c in enumerate(bar_colors):
            patches[i].set_facecolor(c)
        plt.setp(axs[k].get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")
    plt.show()
    # plt.savefig("/home/apeus/PycharmProjects/candlevision/figures/pancreas_parts_dataset_v2_fold_distrib.png")


def distrib():
    dataset = PancreasPartsDataset(
        path_images_dir="/scratch/APEUS_DATA/pancreas_parts/dataset_v3",
        patient_filter=VAL_ID_PATIENTS + TRAIN_ID_PATIENTS,
        transform=None
    )
    bar_colors = ["blue", "black", "green", "orange", "pink", "yellow", "gray", "red"]


    df = dataset.dataset
    df = df[["classes", "clips", "patient_ids"]].drop_duplicates()
    classes = df.groupby(["classes"]).count()
    print(classes)
    d = {'classes': [c for c in classes.index], "val": [k for k in classes["clips"]]}
    data = pd.DataFrame(d)
    ax = data.plot.bar(x='classes', y='val', color=bar_colors, legend=False, width=1.0, edgecolor='black')
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
    plt.tight_layout()
    plt.show()
    # plt.savefig("/home/apeus/PycharmProjects/candlevision/figures/pancreas_parts_dataset_v3_test_distrib.png")


@hydra.main(config_path="../../configs/classification", config_name="basic")
def main(cfg: DictConfig):
    # # Generate the fold
    # data = PancreasPartsDataset(
    #     path_images_dir=cfg.dataset.path_images_dir,
    #     patient_filter=[257, 202, 271, 274, 281, 250, 253],
    #     transform=None
    # )
    # data = data.dataset
    # y = data.classes.to_numpy()
    # groups = data.patient_ids.to_numpy()
    # X = np.ones((len(y), 2))
    # folds_generator = StratifiedGroupKFold(n_splits=4)
    #
    # # Set up the training for each fold
    # for k, (train_ids, val_ids) in enumerate(folds_generator.split(X, y, groups), start=1):
    # CHECK IF LOG ALREADY EXISTS
    # log_version = check_log_exist(os.path.join(cfg.logger.save_dir, cfg.logger.name),
    #                                cfg.logger.version + f"_fold_{k}")

    # backbones = [
    #     "efficientnet_b0",
    #     "efficientnet_b1",
    #     "efficientnet_b2",
    #     "resnet18",
    #     "resnet26",
    #     "resnet34",
    #     "convnext_small",
    # ]

    backbones = [
        # "beit_base_patch16_224",
        # "cait_s24_224",
        # "cait_xxs24_224",
        # "cait_xxs36_224",
        # "convit_tiny",
        # "convit_small",
        # "vit_tiny_patch16_224",
        # "swin_tiny_patch4_window7_224",
        # "twins_svt_small",
        # "tnt_s_patch16_224",
        # "xcit_small_12_p8_224",
        # "xcit_tiny_24_p8_224",
        "deit_tiny_patch16_224",
    ]

    for backbone in backbones:
        cfg.model.backbone = backbone
        cfg.logger.version = "basic_" + backbone

        log_version = check_log_exist(os.path.join(cfg.logger.save_dir, cfg.logger.name), cfg.logger.version)

        # SETUP TRAINING DATASET
        train_transform = ImageTransformer(**cfg.augmentation.train)

        train_dataset = PancreasPartsDataset(
            path_images_dir=cfg.dataset.path_images_dir,
            patient_filter=TEST_ID_PATIENTS + VAL_ID_PATIENTS,
            transform=train_transform
        )

        # SETUP VALIDATION DATASET
        val_transform = ImageTransformer(**cfg.augmentation.val)

        val_dataset = PancreasPartsDataset(
            path_images_dir=cfg.dataset.path_images_dir,
            patient_filter=TEST_ID_PATIENTS + TRAIN_ID_PATIENTS,
            transform=val_transform
        )

        # SETUP TEST DATASET
        test_dataset = PancreasPartsDataset(
            path_images_dir=cfg.dataset.path_images_dir,
            patient_filter=VAL_ID_PATIENTS + TRAIN_ID_PATIENTS,
            transform=val_transform
        )

        # CREATE LIGHTNING DATAMODULE
        datamodule = PancreasPartsDataModule(
            train_dataset=train_dataset,
            val_dataset=val_dataset,
            test_dataset=test_dataset,
            **cfg.loader
        )

        # SET UP MODEL
        # classifier = ResnetClassifier(
        #     backbone=cfg.model.backbone,
        #     pretrained=cfg.model.pretrained,
        #     num_classes=train_dataset.num_classes,
        #     replace_state_dict_layer=cfg.model.replace_state_dict_layer
        # )

        classifier = timm.create_model(cfg.model.backbone, pretrained=True, num_classes=6)
        # checkpoint = "/home/apeus/PycharmProjects/models/pancreas_parts/dataset_v3/basic_resnet18_epoch=6.ckpt"
        # classifier.load_state_dict(torch.load(checkpoint)['state_dict'], strict=True)

        # CREATE LIGHTNING MODULE
        lightning_module = PancreasPartsClassifier(
            patient_val_ids=val_dataset.patient_ids,
            patient_test_ids=test_dataset.patient_ids,
            weight_classes=train_dataset.weight_classes,
            model=classifier,
            name_classes=train_dataset.classes,
            nb_epochs=cfg.trainer.max_epochs,
            **cfg.lightning_module,
        )

        # CALLBACKS
        logger = TensorBoardLogger(
            save_dir=cfg.logger.save_dir,
            name=cfg.logger.name,
            version=log_version,
            default_hp_metric=cfg.logger.default_hp_metric,
        )

        # log hparams into Tensorboard
        logger.log_hyperparams(cfg)

        checkpoint_callback = pl.callbacks.ModelCheckpoint(
            filename=f'{cfg.logger.version}' + '_{epoch}',
            **cfg.callbacks.model_checkpoint
        )

        # SETUP TRAINER
        trainer = pl.Trainer(
            callbacks=[checkpoint_callback],
            logger=logger,
            # gradient_clip_val=0.3,
            **cfg.trainer
        )

        trainer.fit(lightning_module, datamodule=datamodule)
        trainer.test(lightning_module, datamodule=datamodule, verbose=False, ckpt_path="best")


if __name__ == '__main__':
    # main()
    display_fold_distribution()
    # distrib()
    # print(timm.list_models("tnt*", pretrained=True))
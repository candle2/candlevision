import os

import hydra
import numpy as np
import pytorch_lightning as pl
import matplotlib.pyplot as plt
from omegaconf import DictConfig
from pytorch_lightning.callbacks import EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger
from sklearn.model_selection import StratifiedGroupKFold

from candlevision.data.dataset.starhe_dataset import StarheDataset
from candlevision.data.loaders import PancreasPartsDataModule
from candlevision.models.classification import MViT, PancreasPartsClassifier, X3D
from candlevision.models.classification.transform_video import VideoTransform
from candlevision.utils.utils import check_log_exist

os.environ["CUDA_VISIBLE_DEVICES"] = "1"

PATIENT_ID_TO_INFO = {
    10: '05-0004-D-N', 4: '05-0010-H-J', 36: '02-0015-S-V', 38: '02-0013-B-A', 8: '05-0006-C-G', 1: '05-0014-B-E',
    43: '02-0007-R-M', 40: '02-0011-H-P', 21: '02-0030-S-D', 29: '02-0022-C-D', 31: '02-0020-G-L', 56: '01-0018-B-A',
    9: '05-0005-R-A', 0: '05-0015-B-D', 45: '02-0005-A-M', 39: '02-0012-M-J', 37: '02-0014-E-A', 13: '05-0001-G-B',
    5: '05-0009-V-S', 27: '02-0024-L-J', 42: '02-0008-M-P', 6: '05-0008-R-J', 34: '02-0017-D-K', 26: '02-0025-T-S',
    54: '01-0020-M-H', 50: '01-0024-M-B', 24: '02-0027-B-J', 49: '02-0001-S-B', 12: '05-0002-R-D', 46: '02-0004-M-Z',
    28: '02-0023-E-A', 30: '02-0021-T-M', 11: '05-0003-S-Y', 48: '02-0002-P-D', 64: '01-0010-R-L', 47: '02-0003-C-M',
    20: '02-0031-M-P', 7: '05-0007-N-L', 53: '01-0021-D-F', 61: '01-0013-G-D', 3: '05-0011-L-J', 41: '02-0010-N-V',
    52: '01-0022-D-J', 23: '02-0028-Z-A', 2: '05-0012-G-B', 58: '01-0016-D-J', 33: '02-0018-T-H', 19: '02-0032-H-F',
    62: '01-0012-D-J', 51: '01-0023-P-J', 16: '04-0004-R-J', 14: '04-0006-U-E', 66: '01-0007-D-M', 25: '02-0026-B-G',
    65: '01-0008-D-Y', 67: '01-0006-L-G', 15: '04-0005-H-G', 17: '04-0001-M-S', 68: '01-0004-P-G', 55: '01-0019-A-J',
    57: '01-0017-G-J', 32: '02-0019-D-D', 35: '02-0016-L-M', 44: '02-0006-R-G', 60: '01-0014-G-S', 63: '01-0011-B-M',
    22: '02-0029-P-J', 59: '01-0015-R-H', 18: '02-0033-D-S', 69: '01-0001-V-M'
}


def display_fold_distribution(n_splits: int = 4):
    dataset = StarheDataset(
        num_frame_per_clip=16,
        path_images_dir="/home/afleurentin/Bureau/STARHE",
        transform=None
    )
    data = dataset.dataset
    y = data.classes.to_numpy()
    groups = data.patient_ids.to_numpy()
    X = np.ones((len(y), 2))

    folds_generator = StratifiedGroupKFold(n_splits=n_splits)

    bar_colors = ["blue", "black"]

    fig, axs = plt.subplots(1, n_splits, sharey=True, tight_layout=True)
    for k, (_, val_ids) in enumerate(folds_generator.split(X, y, groups)):
        print(f"fold n°{k} | number patients: {len(set(groups[val_ids]))} | list id patient: {set(groups[val_ids])}")
        _, _, patches = axs[k].hist(sorted(y[val_ids]), bins=8)
        for i, c in enumerate(bar_colors):
            patches[i].set_facecolor(c)
        plt.setp(axs[k].get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
    plt.show()


@hydra.main(config_path="../../configs/classification/", config_name="x3d")
def main(cfg: DictConfig):

    # SETUP TRAINING TRANSFORM
    train_transform = VideoTransform(
        num_frame_per_clip=cfg.dataset.num_frame_per_clip,
        **cfg.augmentation.train
    )

    # SETUP VALIDATION TRANSFORM
    val_transform = VideoTransform(
        num_frame_per_clip=cfg.dataset.num_frame_per_clip,
        **cfg.augmentation.val
    )

    dataset = StarheDataset(
        num_frame_per_clip=16,
        path_images_dir=cfg.dataset.path_images_dir,
        transform=None
    )

    data = dataset.dataset
    y = data.classes.to_numpy()
    groups = data.patient_ids.to_numpy()
    X = np.ones((len(y), 2))
    folds_generator = StratifiedGroupKFold(n_splits=4)

    for k, (train_ids, val_ids) in enumerate(folds_generator.split(X, y, groups)):
        log_version = check_log_exist(os.path.join(cfg.logger.save_dir, cfg.logger.name),
                                      cfg.logger.version + f"_fold_{k}")

        # SETUP TRAINING DATASET
        train_dataset = StarheDataset(
            patient_filter=[PATIENT_ID_TO_INFO[k] for k in set(groups[val_ids])],
            transform=train_transform,
            **cfg.dataset
        )

        # SETUP VALIDATION DATASET
        val_dataset = StarheDataset(
                patient_filter=[PATIENT_ID_TO_INFO[k] for k in set(groups[train_ids])],
                transform=val_transform,
                **cfg.dataset
        )

        # CREATE LIGHTNING DATAMODULE
        datamodule = PancreasPartsDataModule(
            train_dataset=train_dataset,
            val_dataset=val_dataset,
            **cfg.loader
        )

        # SET UP MODEL
        # lstm_classifier = MViT(
        #     model_num_class=train_dataset.num_classes,
        #     name_model='mvit_base_16x4',
        #     pretrained="/home/apeus/PycharmProjects/models/pancreas_parts/miccai/mvit_16x4_2_epoch=50.ckpt"
        # )

        lstm_classifier = X3D(
            model_num_class=train_dataset.num_classes,
            **cfg.model
        )

        # CREATE LIGHTNING MODULE
        lightning_module = PancreasPartsClassifier(
            patient_val_ids=val_dataset.patient_ids,
            name_classes=train_dataset.classes,
            model=lstm_classifier,
            nb_epochs=cfg.trainer.max_epochs,
            **cfg.lightning_module,
        )

        # CALLBACKS
        logger = TensorBoardLogger(
            save_dir=cfg.logger.save_dir,
            name=cfg.logger.name,
            version=log_version,
            default_hp_metric=cfg.logger.default_hp_metric,
        )

        # log hparams into Tensorboard
        logger.log_hyperparams(cfg)

        checkpoint_callback = pl.callbacks.ModelCheckpoint(
            filename=f'{log_version}' + '_{epoch}',
            **cfg.callbacks.model_checkpoint
        )

        early_stopping_callback = EarlyStopping(
            monitor=cfg.callbacks.model_checkpoint.monitor,
            patience=int(cfg.trainer.max_epochs * 0.25),
            **cfg.callbacks.early_stopping
        )

        # SETUP TRAINER
        trainer = pl.Trainer(
            callbacks=[checkpoint_callback, early_stopping_callback],
            progress_bar_refresh_rate=50,
            # gradient_clip_val=0.05,
            logger=logger,
            **cfg.trainer
        )

        trainer.fit(lightning_module, datamodule=datamodule)

if __name__ == '__main__':
    display_fold_distribution()
    # main()
import functools
import os
from pathlib import Path

import pytorch_lightning as pl
from pytorch_lightning import Trainer, seed_everything
from pytorch_lightning.callbacks import EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger
from candlevision.data.loaders import DetectEusDataModule, DetectDataModule
from candlevision.data.dataset import ApeusDataset, CocoDataset
from candlevision.models.detection import faster_rcnn
from candlevision.models.detection.object_detector import ObjectDetector, FeatureExtractorFreezeUnfreeze
from candlevision.utils.utils import ConfigReader, my_logger, check_log_exist

os.environ["CUDA_VISIBLE_DEVICES"] = "3"


def main(cfg: ConfigReader):
    seed_everything(17, workers=True)

    # verify the name of the log version
    cfg.logger.version = check_log_exist(os.path.join(cfg.logger.save_dir, cfg.logger.name), cfg.logger.version)

    # DEFINE DATASET
    train_dataset = ApeusDataset(path_images_dir=cfg.dataset.dir,
                                 path_json=cfg.dataset.path_json_train,
                                 subfolder=cfg.dataset.subfolder)

    val_dataset = ApeusDataset(path_images_dir=cfg.dataset.dir,
                               path_json=cfg.dataset.path_json_val,
                               subfolder=cfg.dataset.subfolder)

    # DEFINE DATALOADER
    datamodule = DetectEusDataModule(train_dataset=train_dataset,
                                     val_dataset=val_dataset,
                                     num_workers=cfg.datamodule.num_workers,
                                     normalize=cfg.datamodule.normalized,
                                     resize=cfg.datamodule.resize,
                                     batch_size=cfg.datamodule.batch_size,
                                     pin_memory=cfg.datamodule.pin_memory,
                                     drop_last=cfg.datamodule.drop_last)

    # DEFINE MODEL
    model = faster_rcnn(backbone=cfg.model.backbone,
                        fpn=cfg.model.fpn,
                        trainable_backbone_layers=cfg.model.trainable_backbone_layers,
                        pretrained=cfg.model.pretrained,
                        num_classes=datamodule.num_classes,
                        image_mean=cfg.model.image_mean,
                        image_std=cfg.model.image_std,
                        min_size=cfg.model.image_min_size,
                        max_size=cfg.model.image_max_size,
                        rpn_pre_nms_top_n_train=cfg.model.rpn_pre_nms_top_n_train,
                        rpn_pre_nms_top_n_test=cfg.model.rpn_pre_nms_top_n_test,
                        rpn_post_nms_top_n_train=cfg.model.rpn_post_nms_top_n_train,
                        rpn_post_nms_top_n_test=cfg.model.rpn_post_nms_top_n_test)

    # DEFINE LIGHTNING MODULE
    detector = ObjectDetector(datamodule=datamodule,
                              model=model,
                              learning_rate=cfg.detector.learning_rate,
                              weight_decay=cfg.detector.weight_decay,
                              nb_epochs=cfg.trainer.max_epochs,
                              num_gpus=cfg.trainer.gpus,
                              metric=cfg.detector.metric,
                              path_save_evaluation_result=cfg.detector.path_save_evaluation_result,
                              score_threshold=cfg.detector.score_threshold)

    # CALLBACKS
    checkpoint_callback = pl.callbacks.ModelCheckpoint(
        dirpath=cfg.checkpoint.checkpoint_path,
        monitor=cfg.checkpoint.monitor,
        save_top_k=cfg.checkpoint.save_top_k,
        mode=cfg.checkpoint.mode,
        filename=f'{cfg.logger.version}' + f'_{cfg.checkpoint.fold}' + '_{epoch}',
        save_weights_only=cfg.checkpoint.save_weights_only
    )

    early_stop_callback = EarlyStopping(monitor=cfg.checkpoint.monitor,
                                        min_delta=0.0,
                                        verbose=True,
                                        patience=cfg.early_stopping.patience,
                                        mode=cfg.early_stopping.mode,
                                        check_on_train_epoch_end=cfg.early_stopping.check_on_train_epoch_end)

    logger = TensorBoardLogger(save_dir=cfg.logger.save_dir,
                               name=cfg.logger.name,
                               version=cfg.logger.version,
                               default_hp_metric=True)

    # log hparams into Tensorboard
    logger.log_hyperparams(cfg._dict_params)

    training_strategy = FeatureExtractorFreezeUnfreeze(
        unfreeze_backbone_at_epoch=cfg.training_strategy.unfreeze_backbone_at_epoch,
        unfreeze_fpn_at_epoch=cfg.training_strategy.unfreeze_fpn_at_epoch,
        unfreeze_layer_rate=cfg.training_strategy.unfreeze_layer_rate,
    )

    trainer = Trainer(
        weights_summary=cfg.trainer.weights_summary,
        progress_bar_refresh_rate=cfg.trainer.progress_bar_refresh_rate,
        num_sanity_val_steps=cfg.trainer.num_sanity_val_steps,
        accelerator=cfg.trainer.accelerator,
        gpus=cfg.trainer.gpus,
        max_epochs=cfg.trainer.max_epochs,
        auto_lr_find=cfg.trainer.auto_lr_find,
        callbacks=[checkpoint_callback, early_stop_callback, training_strategy],
        logger=logger,
        deterministic=True,
        benchmark=True,
    )

    trainer.fit(detector, datamodule=datamodule)


if __name__ == "__main__":

    path_backbone_pretrained = "/home/apeus/projects/models/simclr/test/resnet50_fpn"
    for i, name_backbone in enumerate(sorted(Path(path_backbone_pretrained).glob("run-6-pretrained-b128-cd3-spatial-pixel-p6_epoch=499*"))):
        cfg_params = ConfigReader(config_file="/configs/faster/faster-rcnn_fold_2.yml")
        cfg_params.logger.version = f"finetuning-imagenet-fpn-5-layers-{name_backbone.stem}"
        cfg_params.model.trainable_backbone_layers = 5
        cfg_params.model.pretrained = f"{name_backbone}"
        my_logger().info(cfg_params.logger.version)
        main(cfg_params)
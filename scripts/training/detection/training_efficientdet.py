import pytorch_lightning as pl
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger
from candlevision.data.loaders import DetectEusDataModule
from candlevision.data.dataset import ApeusDataset
from candlevision.models.detection.efficientDet import EfficientDetBuilder, EfficientDet, FocalLoss
from candlevision.models.detection.object_detector import ObjectDetector
from candlevision.utils.utils import ConfigReader


# os.environ["CUDA_VISIBLE_DEVICES"] = "1, 2, 3"

def main(cfg: ConfigReader):
    # seed_everything(17, workers=True)

    # DEFINE DATASET
    train_dataset = ApeusDataset(path_images_dir=cfg.dataset.dir,
                                 path_json=cfg.dataset.path_json_train,
                                 subfolder=cfg.dataset.subfolder)

    val_dataset = ApeusDataset(path_images_dir=cfg.dataset.dir,
                               path_json=cfg.dataset.path_json_val,
                               subfolder=cfg.dataset.subfolder)

    # DEFINE DATALOADER
    datamodule = DetectEusDataModule(train_dataset=train_dataset,
                                     val_dataset=val_dataset,
                                     num_workers=cfg.datamodule.num_workers,
                                     normalize=cfg.datamodule.normalized,
                                     resize=cfg.datamodule.resize,
                                     batch_size=cfg.datamodule.batch_size,
                                     pin_memory=cfg.datamodule.pin_memory,
                                     drop_last=cfg.datamodule.drop_last)

    # input_sizes = [512, 640, 768, 896, 1024, 1280, 1280, 1536, 1536]
    model = EfficientDetBuilder(num_classes=datamodule.num_classes, compound_coef=cfg.model.compound_coef)

    # freeze backbone if train head_only
    if cfg.model.head_only:
        def freeze_backbone(m):
            classname = m.__class__.__name__
            for ntl in ['EfficientNet', 'BiFPN']:
                if ntl in classname:
                    for param in m.parameters():
                        param.requires_grad = False

        model.apply(freeze_backbone)

    loss = FocalLoss(
        alpha=cfg.loss.alpha,
        gamma=cfg.loss.gamma,
        upper_threshold_objectness=cfg.loss.upper_threshold_objectness,
        lower_threshold_objectness=cfg.loss.lower_threshold_objectness
    )

    model = EfficientDet(model=model,
                         loss=loss,
                         weights_path=cfg.model.pretrained)

    # DEFINE LIGHTNING MODULE
    detector = ObjectDetector(datamodule=datamodule,
                              model=model,
                              learning_rate=cfg.detector.learning_rate,
                              weight_decay=cfg.detector.weight_decay,
                              nb_epochs=cfg.trainer.max_epochs,
                              num_gpus=cfg.trainer.gpus,
                              metric=cfg.detector.metric,
                              path_save_evaluation_result=cfg.detector.path_save_evaluation_result,
                              score_threshold=cfg.detector.score_threshold)

    # CALLBACKS
    checkpoint_callback = pl.callbacks.ModelCheckpoint(
        dirpath=cfg.checkpoint.checkpoint_path,
        monitor=cfg.checkpoint.monitor,
        save_top_k=cfg.checkpoint.save_top_k,
        mode=cfg.checkpoint.mode,
        filename=f'{cfg.logger.version}' + f'_{cfg.checkpoint.fold}' + '_{epoch}',
        save_weights_only=cfg.checkpoint.save_weights_only
    )

    early_stop_callback = EarlyStopping(monitor=cfg.checkpoint.monitor,
                                        min_delta=0.0,
                                        verbose=True,
                                        patience=cfg.early_stopping.patience,
                                        mode=cfg.early_stopping.mode,
                                        check_on_train_epoch_end=cfg.early_stopping.check_on_train_epoch_end)

    logger = TensorBoardLogger(save_dir=cfg.logger.save_dir,
                               name=cfg.logger.name,
                               version=cfg.logger.version,
                               default_hp_metric=True)

    # log hparams into Tensorboard
    logger.log_hyperparams(cfg._dict_params)

    trainer = Trainer(weights_summary=cfg.trainer.weights_summary,
                      progress_bar_refresh_rate=cfg.trainer.progress_bar_refresh_rate,
                      num_sanity_val_steps=cfg.trainer.num_sanity_val_steps,
                      accelerator=cfg.trainer.accelerator,
                      gpus=cfg.trainer.gpus,
                      max_epochs=cfg.trainer.max_epochs,
                      auto_lr_find=cfg.trainer.auto_lr_find,
                      callbacks=[checkpoint_callback, early_stop_callback],
                      logger=logger,
                      deterministic=True,
                      benchmark=True,
                      )

    trainer.fit(detector, datamodule=datamodule)


if __name__ == "__main__":
    # FOLD 1
    cfg_params = ConfigReader(config_file="/home/apeus/PycharmProjects/candlevision/configs/efficientDet/efficientdet_fold_1.yml")
    main(cfg_params)

    # FOLD 2
    cfg_params = ConfigReader(config_file="/home/apeus/PycharmProjects/candlevision/configs/efficientDet/efficientdet_fold_2.yml")
    main(cfg_params)

    # FOLD 3
    cfg_params = ConfigReader(config_file="/home/apeus/PycharmProjects/candlevision/configs/efficientDet/efficientdet_fold_3.yml")
    main(cfg_params)

    # FOLD 4
    cfg_params = ConfigReader(config_file="/home/apeus/PycharmProjects/candlevision/configs/efficientDet/efficientdet_fold_4.yml")
    main(cfg_params)

    # FOLD 5
    cfg_params = ConfigReader(config_file="/home/apeus/PycharmProjects/candlevision/configs/efficientDet/efficientdet_fold_5.yml")
    main(cfg_params)

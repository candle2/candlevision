import os
import pytorch_lightning as pl
from pytorch_lightning import Trainer, seed_everything
from pytorch_lightning.loggers import TensorBoardLogger
from candlevision.data.dataset import ApeusFramesPairSampler, SamplePairGeneration
from candlevision.data.loaders import SimCLRDataModule
from candlevision.models.self_supervised import ContrastiveLoss
from candlevision.models.self_supervised.simclr import ImageEmbedding
from candlevision.models.self_supervised.simclr import SimCLR
from candlevision.models.self_supervised.simclr import SimCLRDataTransform
from candlevision.models.self_supervised.simclr.transform_v2 import ImageTransformer
from candlevision.utils.utils import ConfigReader, my_logger

os.environ["CUDA_VISIBLE_DEVICES"] = "3"


def train(cfg: ConfigReader):
    """Training script from SimCLR

    :param cfg: config object
    """
    seed_everything(42, workers=True)
    # PAIR SAMPLING AND TRANSFORMATION
    pair_sampler = ApeusFramesPairSampler(
        range_selection=cfg.sampler.clip_duration,
        blending_factor=cfg.sampler.blending_factor,
        fps=cfg.sampler.fps
    )

    pair_transform = SimCLRDataTransform(
        resize_input=cfg.transform.resize,
        gaussian_blur=cfg.transform.gaussian_blur,
        jitter_strength=cfg.transform.jitter_strength,
        normalize=cfg.transform.normalized
    )

    pair_transform = ImageTransformer(
        resize=cfg.transform.resize,
        spatial_transform=cfg.transform.spatial_transform,
        probability_spatial_transform=cfg.transform.probability_spatial_transform,
        pixel_level_transform=cfg.transform.pixel_level_transform,
        probability_pixel_level_transform=cfg.transform.probability_pixel_level_transform,
    )

    # TRAINING AND VALIDATION DATASET
    train_dataset = SamplePairGeneration(path_dir_data=cfg.dataset.path_json_train,
                                         positive_pair_sampler=pair_sampler,
                                         transform=pair_transform,
                                         extension_factor=cfg.dataset.extension_factor_train)

    # val_dataset = SamplePairGeneration(path_dir_data=cfg.dataset.path_json_val,
    #                                    positive_pair_sampler=pair_sampler,
    #                                    transform=pair_transform,
    #                                    extension_factor=cfg.dataset.extension_factor_val)

    # LIGHTNING DATA MODULE
    datamodule = SimCLRDataModule(train_dataset=train_dataset,
                                  # val_dataset=val_dataset,
                                  batch_size=cfg.datamodule.batch_size,
                                  num_workers=cfg.datamodule.num_workers,
                                  drop_last=cfg.datamodule.drop_last)

    # LOSS AND MODEL
    loss_function = ContrastiveLoss(batch_size=cfg.datamodule.batch_size,
                                    temperature=cfg.loss.temperature)

    image_embedder = ImageEmbedding(name_model=cfg.model.backbone,
                                    embedding_size=cfg.model.embedding_size,
                                    pretrained=cfg.model.pretrained,
                                    fpn=cfg.model.fpn)

    # LIGHTNING MODULE
    simclr_module = SimCLR(model=image_embedder,
                           loss=loss_function,
                           learning_rate=cfg.simclr.learning_rate,
                           nb_steps=cfg.trainer.max_steps)

    # CALLBACKS
    checkpoint_callback = pl.callbacks.ModelCheckpoint(dirpath=cfg.checkpoint.checkpoint_path,
                                                       filename=f'{cfg.logger.version}' + '_{epoch}',
                                                       save_top_k=cfg.checkpoint.save_top_k,
                                                       save_weights_only=cfg.checkpoint.save_weights_only,
                                                       every_n_epochs=cfg.checkpoint.every_n_epochs)

    logger = TensorBoardLogger(save_dir=cfg.logger.save_dir,
                               name=cfg.logger.name,
                               version=cfg.logger.version,
                               default_hp_metric=True)

    logger.log_hyperparams(cfg._dict_params)

    trainer = Trainer(weights_summary=cfg.trainer.weights_summary,
                      progress_bar_refresh_rate=cfg.trainer.progress_bar_refresh_rate,
                      num_sanity_val_steps=cfg.trainer.num_sanity_val_steps,
                      # accelerator=cfg.trainer.accelerator,
                      gpus=cfg.trainer.gpus,
                      max_epochs=cfg.trainer.max_epochs,
                      auto_lr_find=cfg.trainer.auto_lr_find,
                      callbacks=[checkpoint_callback],
                      logger=logger,
                      deterministic=True,
                      benchmark=True,
                      log_every_n_steps=10
                      )

    trainer.fit(simclr_module, datamodule=datamodule)


if __name__ == '__main__':
    """
    On the xDemo server, if we have TypeError it might be because the harddrive is not mounted
    """
    cfg_params = ConfigReader(config_file="/configs/simclr/training_config_simclr.yml")
    cfg_params.checkpoint.checkpoint_path = "/home/apeus/projects/models/simclr/test/resnet18"
    cfg_params.model.backbone = "resnet18"
    cfg_params.model.fpn = False
    cfg_params.transform.spatial_transform = True
    cfg_params.transform.pixel_level_transform = True
    cfg_params.transform.probability_pixel_level_transform = 0.6
    cfg_params.logger.version = "run-1-resnet-18-pretrained-b256-cd3-spatial-pixel-p6"
    my_logger().info(cfg_params.logger.version)
    train(cfg_params)

import cv2
import pytorch_lightning as pl
from candlevision.utils.visualize import Visualize
from candlevision.data.dataset import PredictVideoDataset, PredictImageDataset
from candlevision.data.loaders import PredictDetectEusDataModule
from candlevision.models.detection import faster_rcnn, EfficientDet, EfficientDetBuilder
from candlevision.models.detection.object_detector import ObjectDetector
from candlevision.utils.utils import ConfigReader
from candlevision.utils import postprocess


def main(params):
    # DATASET
    dataset = PredictImageDataset(path_image=params.path_data,
                                  activate_crop=False)

    # DATAMODULE
    datamodule = PredictDetectEusDataModule(predict_dataset=dataset,
                                            num_classes=2,
                                            num_workers=params.num_workers,
                                            normalize=params.normalize,
                                            resize=params.resize,
                                            batch_size=params.batch_size,
                                            pin_memory=params.pin_memory,
                                            drop_last=params.drop_last)

    # FASTER
    # model = faster_rcnn(backbone=params.backbone,
    #                     fpn=True,
    #                     trainable_backbone_layers=5,
    #                     pretrained=params.pretrained,
    #                     num_classes=datamodule.num_classes,
    #                     min_size=800,
    #                     max_size=800)
    # input_sizes = [512, 640, 768, 896, 1024, 1280, 1280, 1536, 1536]
    model = EfficientDetBuilder(num_classes=2, compound_coef=2)

    model = EfficientDet(model=model,
                         weights_path="/home/afleurentin/PycharmProjects/models/efficientDet/efficientDet_fold_4_epoch=1-step=2447.ckpt")

    detector = ObjectDetector(datamodule=datamodule,
                              model=model,
                              learning_rate=params.learning_rate,
                              nb_epochs=params.max_steps,
                              num_gpus=params.gpus)

    trainer = pl.Trainer(weights_summary=None,
                         progress_bar_refresh_rate=1,
                         num_sanity_val_steps=0,
                         gpus=params.gpus)

    predictions = trainer.predict(detector, datamodule=datamodule)

    name_labels = {1: "paren", 2: "lesion"}
    new_predictions = postprocess.filter_predictions_faster_eus(predictions,
                                                                name_labels=name_labels,
                                                                threshold_score=0.35,
                                                                threshold_nms=0.1)

    final_predictions = postprocess.convert_bbox_original_size(new_predictions, 800, 800)

    ####################################################################################
    #                                  VIDEO
    ####################################################################################
    #
    # dict_correlation_label = {
    #
    #     "paren_hete": "paren",
    #     "paren_hete_mild": "paren",
    #     "paren_hete_mod": "paren",
    #     "paren_hete_sev": "paren",
    #
    #     "paren_homo_iso": "paren",
    #
    #     "paren_homo_hypo": "paren",
    #     "paren_homo_hypo_mild": "paren",
    #     "paren_homo_hypo_mod": "paren",
    #     "paren_homo_hypo_sev": "paren",
    #
    #     "paren_homo_hype": "paren",
    #     "paren_homo_hype_mild": "paren",
    #     "paren_homo_hype_mod": "paren",
    #     "paren_homo_hype_sev": "paren",
    #
    #     "lesi_soli": "lesion",
    #     "lesi_soli_beni": "lesion",
    #     "lesi_soli_susp": "lesion",
    #     "lesi_soli_mali": "lesion",
    #
    #     "lesi_cyst": "lesion",
    #     "lesi_cyst_beni": "lesion",
    #     "lesi_cyst_susp": "lesion",
    #     "lesi_cyst_mali": "lesion",
    # }
    #
    # vid = cv2.VideoCapture(params.path_video)
    # video = Visualize(video=vid)
    # dict_colors = {'lesion': [0, 0, 255], 'paren': [0, 255, 0]}
    # new_video_path = "/home/afleurentin/Bureau/resultat_inference/" + params.path_video.split('/')[-1][:-4] + "_mirror.mp4"
    # path_ground_truth = "/home/afleurentin/Bureau/dataset_v1/annotations/" + params.path_video.split('/')[-1][:-4] + ".json"
    # video.mirror_predictions_ground_truth_apeus(path_new_video=new_video_path,
    #                                             path_gt=path_ground_truth,
    #                                             correlation_label=dict_correlation_label,
    #                                             boxes=final_predictions["boxes"],
    #                                             class_names=final_predictions["labels"],
    #                                             scores=final_predictions["scores"],
    #                                             dict_colors=dict_colors,
    #                                             font_scale=0.8)

    ####################################################################################
    #                                  IMAGE
    ####################################################################################

    image = cv2.imread(params.path_data)
    viz = Visualize(image=image)

    print(final_predictions)
    dict_colors = {'lesion': [0, 0, 255], 'paren': [0, 255, 0]}
    viz.draw_image_annotations(boxes=final_predictions["boxes"][0],
                               class_names=final_predictions["labels"][0],
                               scores=final_predictions["scores"][0],
                               dict_colors=dict_colors,
                               )

    cv2.imshow('test', viz.image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == '__main__':
    # params = Params(config_file="/home/afleurentin/PycharmProjects/candlevision/MyTest/config_inf/run_0.yml")
    # main(params)
    # params = Params(config_file="/home/afleurentin/PycharmProjects/candlevision/MyTest/config_inf/run_1.yml")
    # main(params)
    # params = Params(config_file="/home/afleurentin/PycharmProjects/candlevision/MyTest/config_inf/run_2.yml")
    # main(params)
    # params = Params(config_file="/home/afleurentin/PycharmProjects/candlevision/MyTest/config_inf/run_3.yml")
    # main(params)
    # params = Params(config_file="/home/afleurentin/PycharmProjects/candlevision/MyTest/config_inf/run_4.yml")
    # main(params)
    params = ConfigReader(config_file="/configs/inference/faster.yml")
    main(params)
    # params = Params(config_file="/MyTest/config_inf/run_6.yml")
    # main(params)
    # params = Params(config_file="/MyTest/config_inf/run_7.yml")
    # main(params)
    # params = Params(config_file="/MyTest/config_inf/run_8.yml")
    # main(params)

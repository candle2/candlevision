from typing import List, Tuple

import torch
import pandas as pd
import plotly.express as px
import datetime
from pathlib import Path

# import torch_tensorrt
from moviepy.editor import *
from tqdm import tqdm

import torchvision.transforms as transforms

# from nvidia.dali import pipeline_def
# import nvidia.dali.fn as fn

from candlevision.models.classification import MViT
from candlevision.models.self_supervised.simclr import ImageTransformer

VIDEO_PATH = "/home/afleurentin/Bureau/PPP/predictions/input_VT0257_01_VID001.mp4"
MODEL_MEMORY = 16
FPS_REDUCTION = 5
IDX_TO_LABEL = {
    0: "LIVER",
    1: "ARTEFACTS",
    2: "BODY",
    3: "HEAD",
    4: "OTHER",
    5: "TAIL"
}

LABEL_TO_COLOR = {
    "LIVER": 'blue',
    "ARTEFACTS": 'black',
    "BODY": 'green',
    "HEAD": 'yellow',
    "OTHER": 'gray',
    "TAIL": 'red',
}


# def main():
#     # Load video
#     video = VideoFileClip(VIDEO_PATH).subclip(120)
#     # video = video.set_fps(5)
#
#     # Setup test transform
#     test_transform = VideoTransform(
#         num_frame_per_clip=MODEL_MEMORY,
#         spatial_transform=False,
#         pixel_level_transform=False,
#     )
#
#     # Setup model
#     model = MViT(model_num_class=6)
#     softmax = torch.nn.Softmax(dim=1)
#     checkpoint_path = str([p for p in Path("/home/afleurentin/Bureau/PPP/miccai_models/").glob(
#         f"mvit*")][0])
#
#     state_dict = torch.load(checkpoint_path)['state_dict']
#     for key in list(state_dict.keys()):
#         state_dict[key.replace('model.model.', 'model.')] = state_dict.pop(key)
#         # state_dict[key.replace('model.', '')] = state_dict.pop(key)
#
#     model.load_state_dict(state_dict, strict=True)
#     model = model.cuda()
#     model.eval()
#
#     frames = []
#     for k, frame in tqdm(enumerate(video.iter_frames(), start=1)):
#         # initialisation phase
#         if k < MODEL_MEMORY:
#             frames.append(frame)
#
#         else:
#             frames.append(frame)
#             test_dataset = PredictPancreasPartsDataset(
#                 frames=frames,
#                 transform=test_transform
#             )
#
#             input = test_dataset.__getitem__(0)
#             input = input.unsqueeze(0)
#             input = input.cuda()
#
#             output = model(input)
#             output = softmax(output)
#             prediction_score, pred_label_idx = torch.topk(output, 1)
#             label = IDX_TO_LABEL[int(pred_label_idx[0])]
#             print(f"Predicted: {label} | Confidence: {float(prediction_score): .2f}")
#             frames.pop(0)


def rearrange_subclip(k: int, list_frames: list, fps_reduction: int = 5) -> list:
    """ rearrange the frames indexes for the next clip. It assumes that video are at 30 fps. """
    if k % (30 / fps_reduction) == 0:
        list_frames.pop(0)

    else:
        list_frames.pop()

    return list_frames


def main_single_frame_transform():
    # Load video
    video = VideoFileClip(VIDEO_PATH).subclip(100, 120)

    # Setup test transform
    test_transform = ImageTransformer(
        spatial_transform=False,
        pixel_level_transform=False,
    )

    # Setup model
    model = MViT(model_num_class=6)
    softmax = torch.nn.Softmax(dim=1)
    checkpoint_path = str([p for p in Path("/home/afleurentin/Bureau/PPP/miccai_models/").glob(
        f"mvit*")][0])

    state_dict = torch.load(checkpoint_path)['state_dict']
    for key in list(state_dict.keys()):
        state_dict[key.replace('model.model.', 'model.')] = state_dict.pop(key)
        # state_dict[key.replace('model.', '')] = state_dict.pop(key)

    model.load_state_dict(state_dict, strict=True)
    # model = torch_tensorrt.compile(model,
    #                                inputs=[torch_tensorrt.Input((1, 16, 3, 224, 224))],
    #                                enabled_precisions={torch_tensorrt.dtype.half}  # Run with FP16
    #                                )
    model.eval()

    model = model.cuda()

    # Start looping through the video
    predictions = []
    subclip_frames = []
    for k, frame in tqdm(enumerate(video.iter_frames(), start=1), total=int(video.duration * video.fps) + 1):
        # initialisation phase
        if k < MODEL_MEMORY * int(30 / FPS_REDUCTION):
            if k % int(30 / FPS_REDUCTION) == 0:
                transformed_frame = test_transform(frame)["image"]
                subclip_frames.append(transformed_frame)
            else:
                continue
        else:
            transformed_frame = test_transform(frame)["image"]
            subclip_frames.append(transformed_frame)
            input = torch.stack(subclip_frames)
            input = input.unsqueeze(0)
            input = input.cuda()

            output = model(input)
            output = softmax(output)
            prediction_score, pred_label_idx = torch.topk(output, 1)
            subclip_frames = rearrange_subclip(k, subclip_frames, fps_reduction=FPS_REDUCTION)
            predictions.append(pred_label_idx)

    timeline_prediction = create_timeline_prediction(predictions)
    display_timeline_predictions(timeline_prediction)


def create_timeline_prediction(predictions: list) -> List[Tuple[int, str]]:
    timeline_prediction = []
    for i, label_id in enumerate(predictions, start=MODEL_MEMORY * int(30 / FPS_REDUCTION)):
        if i == MODEL_MEMORY * int(30 / FPS_REDUCTION):
            timeline_prediction.append((i, IDX_TO_LABEL[int(label_id[0])]))
            old_label_id = label_id

        elif i == len(predictions) + MODEL_MEMORY * int(30 / FPS_REDUCTION) - 1:
            timeline_prediction.append((i, IDX_TO_LABEL[int(label_id[0])]))

        elif old_label_id == label_id:
            continue

        else:
            timeline_prediction.append((i - 1, IDX_TO_LABEL[int(old_label_id[0])]))
            timeline_prediction.append((i, IDX_TO_LABEL[int(label_id[0])]))
            old_label_id = label_id
    return timeline_prediction


def display_timeline_predictions(timeline_prediction: list):
    start_preds = timeline_prediction[0::2]
    end_preds = timeline_prediction[1::2]

    data_df = []
    for start_pred, end_pred in zip(start_preds, end_preds):
        start_id_frame, label = start_pred
        end_id_frame, _ = end_pred
        data_df.append(dict(label=label,
                            start="2022-03-03 " + str(datetime.timedelta(milliseconds=int(start_id_frame / 30 * 1000))),
                            end="2022-03-03 " + str(datetime.timedelta(milliseconds=int(end_id_frame / 30 * 1000)))))

    df = pd.DataFrame(data_df)
    print(df)
    fig = px.timeline(df, x_start="start", x_end="end", y="label", color="label", color_discrete_map=LABEL_TO_COLOR)
    fig.update_yaxes(autorange="reversed")  # otherwise tasks are listed from the bottom up
    fig.show()


if __name__ == '__main__':
    main_single_frame_transform()
    # display_timeline_predictions(timeline_pred)

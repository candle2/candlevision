import json
import time

import numpy as np
import torch
import cv2
from vidgear.gears import CamGear

from candlevision.models.classification import MViT
from candlevision.models.self_supervised.simclr import ImageTransformer





def rearrange_subclip(k: int, list_frames: list, fps_reduction: int = 5) -> list:
    """ rearrange the frames indexes for the next clip. It assumes that video are at 30 fps. """
    if k % (30 / fps_reduction) == 0:
        list_frames.pop(0)

    else:
        list_frames.pop()

    return list_frames


def setup_model(checkpoint_path: str):
    # Setup model
    model = MViT(
        model_num_class=6,
        pretrained="/home/afleurentin/Bureau/PPP/miccai_models/mvit_16x4_2_epoch=50.ckpt"
    )
    softmax = torch.nn.Softmax(dim=1)

    model = model.cuda()
    return model.eval()


def run_with_vidgear(model, transform, video_path: str):

    avg_fps =[]

    annotation = {}

    IDX_TO_LABEL = {
        0: "Liver",
        1: "artefacts",
        2: "body",
        3: "head",
        4: "other",
        5: "tail"
    }

    softmax = torch.nn.Softmax(dim=1)

    # Open suitable video stream, such as webcam on first index(i.e. 0)
    stream = CamGear(source=video_path).start()

    frame_counter = 1
    subclip_frames = []

    # loop over
    while True:
        start_time = time.time()
        # read frames from stream
        frame = stream.read()

        # check for frame if Nonetype
        if frame is None:
            break

        # cv2.imshow("output", frame[170:940, 80:1200, :])
        transformed_frame = transform(frame[170:940, 80:1200, :])['image']
        #
        # initialisation phase
        if frame_counter < MODEL_MEMORY * int(30 / FPS_REDUCTION):
            if frame_counter % int(30 / FPS_REDUCTION) == 0:
                subclip_frames.append(transformed_frame)
            annotation[frame_counter] = "initialisation"
        else:
            with torch.no_grad():
                subclip_frames.append(transformed_frame)
                input = torch.stack(subclip_frames)
                input = input.unsqueeze(0)
                input = input.cuda()

                output = model(input)
                output = softmax(output)
                prediction_score, pred_label_idx = torch.topk(output, 1)
                subclip_frames = rearrange_subclip(frame_counter, subclip_frames, fps_reduction=FPS_REDUCTION)
                annotation[frame_counter] = IDX_TO_LABEL[int(pred_label_idx[0])]

        frame_counter += 1

        # # check for 'q' key if pressed
        # key = cv2.waitKey(1) & 0xFF
        # if key == ord("q"):
        #     break

        if len(avg_fps) > 100:
            avg_fps.pop(0)
        avg_fps.append(1.0 / (time.time() - start_time))

        print(np.mean(avg_fps))

    # safely close video stream
    stream.stop()

    with open("/home/afleurentin/Bureau/PPP/predictions/annotation_SJ0281.json", "w") as json_file:
        json.dump(annotation, json_file)


if __name__ == '__main__':
    VIDEO_PATH = "/home/afleurentin/Bureau/PPP/videos/SJ0281_01_VID001.mp4"

    MODEL_MEMORY = 16
    FPS_REDUCTION = 5

    preprocess = ImageTransformer(
        spatial_transform=False,
        pixel_level_transform=False,
    )

    dp_model = setup_model(checkpoint_path="/home/afleurentin/Bureau/PPP/miccai_models/")
    run_with_vidgear(dp_model, preprocess, VIDEO_PATH)

import time
from vidgear.gears import CamGear, WriteGear
from pathlib import Path
from moviepy.editor import *
import numpy as np
from apeus_data.eus_video.Annotation.indexity_annotation import IndexAnnotation
from candlevision.models.classification import MViT
import cv2
import torch, torchvision

# to install PyGObject aka gi
# python3 -m pip install --no-use-pep517  cm-rgb
from candlevision.models.self_supervised.simclr import ImageTransformer

ROOT = "/scratch/APEUS_DATA/pancreas_parts/videos"
VIDEO_PATH = "/home/afleurentin/Bureau/PPP/predictions/input_VT0257_01_VID001.mp4"
# 257 202 281 250_2 250_3 253

MODEL_MEMORY = 16
FPS_REDUCTION = 5
IDX_TO_LABEL = {
    0: "LIVER",
    1: "ARTEFACTS",
    2: "BODY",
    3: "HEAD",
    4: "OTHER",
    5: "TAIL"
}

LABEL_TO_COLOR = {
    "LIVER": 'blue',
    "ARTEFACTS": 'black',
    "BODY": 'green',
    "HEAD": 'yellow',
    "OTHER": 'gray',
    "TAIL": 'red',
}

preprocess = ImageTransformer(
    spatial_transform=False,
    pixel_level_transform=False,
)


def get_ground_truth_label(mosaic_annotation: IndexAnnotation, num_frame: int, fps: int) -> str:
    """ time in milliseconds """
    list_label = mosaic_annotation.get_label(int(num_frame * 1000 * 1 / fps))

    mosaic_label_to_label = {
        "Liver": "LIVER",
        "artefacts": "ARTEFACTS",
        "body": "BODY",
        "head": "HEAD",
        "other": "OTHER",
        "tail": "TAIL"
    }

    if len(list_label) == 0:
        return ""
    else:
        return mosaic_label_to_label[str(list_label[0])]


def rearrange_subclip(k: int, list_frames: list, fps_reduction: int = 5) -> list:
    """ rearrange the frames indexes for the next clip. It assumes that video are at 30 fps. """
    if k % (30 / fps_reduction) == 0:
        list_frames.pop(0)

    else:
        list_frames.pop()

    return list_frames


def setup_model(checkpoint_path: str):
    # Setup model
    model = MViT(model_num_class=6)
    softmax = torch.nn.Softmax(dim=1)
    checkpoint_path = str([p for p in Path(checkpoint_path).glob(
        f"mvit*")][0])

    state_dict = torch.load(checkpoint_path)['state_dict']
    for key in list(state_dict.keys()):
        state_dict[key.replace('model.model.', 'model.')] = state_dict.pop(key)
        # state_dict[key.replace('model.', '')] = state_dict.pop(key)

    model.load_state_dict(state_dict, strict=True)
    model = model.cuda()
    return model.eval()


def run_with_vidgear(model, video_path: str, annotation_path: str, delay: int):
    softmax = torch.nn.Softmax(dim=1)
    video = VideoFileClip(video_path)
    mosaic_annotation = IndexAnnotation(path_json=annotation_path)

    # Open suitable video stream, such as webcam on first index(i.e. 0)
    stream = CamGear(source=video_path).start()

    # Define WriteGear Object with suitable output filename for e.g. `Output.mp4`
    writer = WriteGear(output_filename=video_path[:-4] + "_predictions.mp4", **{"-input_framerate": 29.97})

    frame_counter = 1
    subclip_frames = []

    # loop over
    while True:
        # read frames from stream
        frame = stream.read()

        # check for frame if Nonetype
        if frame is None:
            break

        transformed_frame = preprocess(frame[170:940, 80:1200, :])['image']

        # initialisation phase
        if frame_counter < MODEL_MEMORY * int(30 / FPS_REDUCTION):
            if frame_counter % int(30 / FPS_REDUCTION) == 0:
                subclip_frames.append(transformed_frame)
            else:
                frame_counter += 1
                writer.write(frame)
                # Show output window
                # cv2.imshow("Output", frame)
                continue
        else:
            subclip_frames.append(transformed_frame)
            input = torch.stack(subclip_frames)
            input = input.unsqueeze(0)
            input = input.cuda()

            output = model(input)
            output = softmax(output)
            prediction_score, pred_label_idx = torch.topk(output, 1)
            subclip_frames = rearrange_subclip(frame_counter, subclip_frames, fps_reduction=FPS_REDUCTION)
            cv2.putText(img=frame, text="Predicted:", org=(100, 200),
                        fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=1.0,
                        color=(234, 159, 27), thickness=2)
            cv2.putText(img=frame, text=IDX_TO_LABEL[int(pred_label_idx[0])], org=(100, 250),
                        fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=1.5,
                        color=(234, 159, 27), thickness=3)

        cv2.putText(img=frame, text="Ground Truth:", org=(950, 200),
                    fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=1.0,
                    color=(16, 189, 22), thickness=2)

        cv2.putText(img=frame, text=get_ground_truth_label(mosaic_annotation, frame_counter + delay, video.fps),
                    org=(950, 250),
                    fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=1.5,
                    color=(16, 189, 22), thickness=3)

        frame[0:155, :, :] = 0

        cv2.putText(img=frame, text="Automatic pancreas part detection in endoscopic ultrasound videos | ID 1922 ",
                    org=(100, 80),
                    fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=0.75,
                    color=(255, 255, 255), thickness=1)

        cv2.putText(img=frame, text="CONFIDENTIAL",
                    org=(280, 900),
                    fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=3,
                    color=(0, 0, 255), thickness=3)

        frame_counter += 1
        # Show output window

        cv2.imshow("Output", frame)
        # write frame to writer
        writer.write(frame)

        # check for 'q' key if pressed
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            break

    # close output window
    cv2.destroyAllWindows()

    # safely close video stream
    stream.stop()

    # safely close writer
    writer.close()


if __name__ == '__main__':
    dp_model = setup_model(checkpoint_path="/home/afleurentin/Bureau/PPP/miccai_models/")

    # for id_vid in ["257_01", "202_01", "281_01", "250_02", "250_03", "253_01"]:
    # for video_path in Path("/home/afleurentin/Bureau/PPP/miccai_videos").glob("*.mp4"):
    #     run_with_vidgear(dp_model, str(video_path))
    #
    # video = VideoFileClip("/home/afleurentin/Bureau/PPP/videos/IG0253_01_VID001.mp4").subclip(285, 310)
    # video.write_videofile("/home/afleurentin/Bureau/PPP/miccai_videos/input_IG0253_01_VID001.mp4")

    # run_with_vidgear(
    #     model=dp_model,
    #     video_path="/home/afleurentin/Bureau/PPP/miccai_videos/input_IG0253_01_VID001.mp4",
    #     annotation_path="/home/afleurentin/Bureau/PPP/annotations/IG0253_01_VID001.json",
    #     delay=8541
    # )

    # run_with_vidgear(
    #     model=dp_model,
    #     video_path="/home/afleurentin/Bureau/PPP/miccai_videos/input_VT0257_01_VID001.mp4",
    #     annotation_path="/home/afleurentin/Bureau/PPP/annotations/VT0257_01_VID001.json",
    #     delay=1792
    # )
    #
    # run_with_vidgear(
    #     model=dp_model,
    #     video_path="/home/afleurentin/Bureau/PPP/miccai_videos/input_SJ0281_01_VID001.mp4",
    #     annotation_path="/home/afleurentin/Bureau/PPP/annotations/SJ0281_01_VID001.json",
    #     delay=7193
    # )
    #
    # run_with_vidgear(
    #     model=dp_model,
    #     video_path="/home/afleurentin/Bureau/PPP/miccai_videos/input_NJ0250_02_VID001.mp4",
    #     annotation_path="/home/afleurentin/Bureau/PPP/annotations/NJ0250_02_VID001.json",
    #     delay=36683
    # )

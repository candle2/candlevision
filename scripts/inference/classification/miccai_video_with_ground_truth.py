import cv2
from moviepy.editor import *
from vidgear.gears import CamGear, WriteGear
from apeus_data.eus_video.Annotation.indexity_annotation import IndexAnnotation


def get_ground_truth_label(mosaic_annotation: IndexAnnotation, timestamp: int, fps: int) -> str:
    """ time in milliseconds """
    list_label = mosaic_annotation.get_label(timestamp * round(1000 * 1/fps * 3/2))

    mosaic_label_to_label = {
        "Liver": "LIVER",
        "artefacts": "ARTEFACTS",
        "body": "BODY",
        "head": "HEAD",
        "other": "OTHER",
        "tail": "TAIL"
    }

    if len(list_label) == 0:
        return ""
    else:
        return mosaic_label_to_label[str(list_label[0])]


def add_ground_truth(video_path: str, annotation_path):
    # Open suitable video stream, such as webcam on first index(i.e. 0)
    video = VideoFileClip(video_path)
    stream = CamGear(source=video_path).start()

    # Define WriteGear Object with suitable output filename for e.g. `Output.mp4`
    writer = WriteGear(output_filename=video_path[:-4] + "_gt.mp4", **{"-input_framerate": 30})

    mosaic_annotation = IndexAnnotation(path_json=annotation_path)

    frame_counter = 1892

    # loop over
    while True:
        # read frames from stream
        frame = stream.read()

        # check for frame if Nonetype
        if frame is None:
            break

        cv2.putText(img=frame, text="Ground Truth:", org=(950, 200),
                    fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=1.0,
                    color=(0, 255, 0), thickness=2)

        cv2.putText(img=frame, text=get_ground_truth_label(mosaic_annotation, frame_counter, video.fps), org=(1050, 250),
                    fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=1.5,
                    color=(0, 255, 0), thickness=3)

        cv2.putText(img=frame, text="Automatic pancreas part detection in endoscopic ultrasound videos | ID 1922 ", org=(100, 80),
                    fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=0.75,
                    color=(255, 255, 255), thickness=1)

        # Show output window
        # cv2.imshow("Output", frame)
        # write frame to writer
        writer.write(frame)

        # check for 'q' key if pressed
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            break

        frame_counter += 1

    # close output window
    cv2.destroyAllWindows()

    # safely close video stream
    stream.stop()

    # safely close writer
    writer.close()


if __name__ == '__main__':
    add_ground_truth(
        video_path="/home/afleurentin/Bureau/PPP/miccai_videos/input_VT0257_01_VID001_predictions.mp4",
        annotation_path="/home/afleurentin/Bureau/PPP/annotations/VT0257_01_VID001.json"
    )

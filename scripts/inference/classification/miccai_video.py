import numpy as np
from moviepy.editor import *
import moviepy.video.fx.all as vfx

clip_1 = VideoFileClip("/home/afleurentin/Bureau/PPP/miccai_videos/input_NJ0250_02_VID001_predictions.mp4")
clip_1 = clip_1.subclip(t_start=4)
clip_1 = (clip_1.fx(vfx.fadein, duration=2).fx(vfx.fadeout, duration=2))

clip_2 = VideoFileClip("/home/afleurentin/Bureau/PPP/miccai_videos/input_SJ0281_01_VID001_predictions.mp4")
clip_2 = clip_2.subclip(t_start=4, t_end=20)
clip_2 = (clip_2.fx(vfx.fadein, duration=2).fx(vfx.fadeout, duration=2))

clip_3 = VideoFileClip("/home/afleurentin/Bureau/PPP/miccai_videos/input_VT0257_01_VID001_predictions.mp4")
clip_3 = clip_3.subclip(t_start=10, t_end=40)
clip_3 = (clip_3.fx(vfx.fadein, duration=2).fx(vfx.fadeout, duration=2))

clip_4 = VideoFileClip("/home/afleurentin/Bureau/PPP/miccai_videos/input_IG0253_01_VID001_predictions.mp4")
clip_4 = clip_4.subclip(t_start=4)
clip_4 = (clip_4.fx(vfx.fadein, duration=2).fx(vfx.fadeout, duration=2))

background = np.zeros((1080, 1250, 3))

title = TextClip("Automatic pancreas part detection\n in endoscopic ultrasound videos",
                 fontsize=50,
                 color='white')

title = title.set_pos('center').set_duration(5)

paper_id = TextClip("PAPER ID: 1922",
                    fontsize=30,
                    color='white')

paper_id = paper_id.set_pos('bottom').set_duration(5)

first_page = CompositeVideoClip([ImageClip(background), title, paper_id])
first_page.duration = 4
first_page = (first_page.fx(vfx.fadein, duration=1).fx(vfx.fadeout, duration=1))

mvit = TextClip("Predictions from MViT model",
                fontsize=50,
                color='white')

mvit = mvit.set_pos('center').set_duration(5)
second_page = CompositeVideoClip([ImageClip(background), mvit])
second_page.duration = 4
second_page = (second_page.fx(vfx.fadein, duration=1).fx(vfx.fadeout, duration=1))

miccai_video = concatenate_videoclips([first_page, second_page, clip_1, clip_2, clip_3, clip_4])
miccai_video.write_videofile("/home/afleurentin/Bureau/PPP/miccai_videos/miccai_video.mp4", fps=30)

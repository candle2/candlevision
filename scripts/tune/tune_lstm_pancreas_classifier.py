import os
import torch
import hydra
import pytorch_lightning as pl
from pytorch_lightning.callbacks import EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler
from ray.tune.integration.pytorch_lightning import TuneReportCallback

from candlevision.data.dataset import VideoPancreasPartsDataset
from candlevision.data.loaders import PancreasPartsDataModule
from candlevision.models.classification.transform_video import VideoTransform
from candlevision.models.classification import PancreasPartsClassifier, LSTMClassifier
from candlevision.utils.utils import check_log_exist

os.environ["CUDA_VISIBLE_DEVICES"] = "3"

TEST_ID_PATIENTS = [257, 202, 271, 274, 281, 250, 253]
VAL_ID_PATIENTS = [258, 264, 266, 269, 272, 275, 276, 248]
TRAIN_ID_PATIENTS = [164, 261, 262, 263, 277, 278, 280, 249, 255, 260, 267, 279, 246, 247, 282, 252, 254, 256, 97, 259,
                     265, 268, 270, 243, 85]


def train_lstm(config):
    cfg = config['hydraconfig']
    cfg.model.backbone = config["backbone"]
    cfg.logger.version = config["backbone"]

    # CHECK IF LOG ALREADY EXISTS
    log_version = check_log_exist(os.path.join(cfg.logger.save_dir, cfg.logger.name), cfg.logger.version)

    # SETUP TRAINING DATASET
    train_transform = VideoTransform(
        resize=cfg.augmentation.train.resize,
        num_frame_per_clip=cfg.dataset.num_frame_per_clip,
        spatial_transform=cfg.augmentation.train.spatial_transform,
        probability_spatial_transform=cfg.augmentation.train.probability_spatial_transform,
        pixel_level_transform=cfg.augmentation.train.pixel_level_transform
    )

    train_dataset = VideoPancreasPartsDataset(
        patient_filter=TEST_ID_PATIENTS + VAL_ID_PATIENTS,
        transform=train_transform,
        num_frame_per_clip=cfg.dataset.num_frame_per_clip,
        path_images_dir=cfg.dataset.path_images_dir,
    )

    # SETUP VALIDATION DATASET
    val_transform = VideoTransform(
        num_frame_per_clip=cfg.dataset.num_frame_per_clip,
        **cfg.augmentation.val
    )

    val_dataset = VideoPancreasPartsDataset(
        patient_filter=TEST_ID_PATIENTS + TRAIN_ID_PATIENTS,
        transform=val_transform,
        num_frame_per_clip=cfg.dataset.num_frame_per_clip,
        path_images_dir=cfg.dataset.path_images_dir,
    )

    # SETUP TEST DATASET
    test_dataset = VideoPancreasPartsDataset(
        path_images_dir=cfg.dataset.path_images_dir,
        patient_filter=VAL_ID_PATIENTS + TRAIN_ID_PATIENTS,
        num_frame_per_clip=cfg.dataset.num_frame_per_clip,
        transform=val_transform
    )

    # CREATE LIGHTNING DATAMODULE
    datamodule = PancreasPartsDataModule(
        train_dataset=train_dataset,
        val_dataset=val_dataset,
        test_dataset=test_dataset,
        batch_size=cfg.loader.batch_size,
        num_workers=cfg.loader.num_workers,
        drop_last=cfg.loader.drop_last,
        pin_memory=cfg.loader.pin_memory
    )

    # SET UP MODEL
    lstm_classifier = LSTMClassifier(
        num_classes=train_dataset.num_classes,
        num_frame_per_clip=cfg.dataset.num_frame_per_clip,
        pretrained=cfg.model.pretrained,
        backbone=cfg.model.backbone,
        fpn=cfg.model.fpn,
        hidden_size=cfg.model.hidden_size,
        num_lstm_layer=cfg.model.num_lstm_layer,
        replace_state_dict_layer=cfg.model.replace_state_dict_layer
    )

    # CREATE LIGHTNING MODULE
    lightning_model = PancreasPartsClassifier(
        patient_val_ids=val_dataset.patient_ids,
        weight_classes=train_dataset.weight_classes,
        name_classes=train_dataset.classes,
        model=lstm_classifier,
        nb_epochs=cfg.trainer.max_epochs,
        learning_rate=cfg.lightning_module.learning_rate,
        weight_decay=cfg.lightning_module.weight_decay
    )

    # CALLBACKS
    logger = TensorBoardLogger(
        save_dir=cfg.logger.save_dir,
        name=cfg.logger.name,
        version=log_version,
        default_hp_metric=cfg.logger.default_hp_metric,
    )

    # log hparams into Tensorboard
    logger.log_hyperparams(cfg)

    checkpoint_callback = pl.callbacks.ModelCheckpoint(
        filename=f'{log_version}' + '_{epoch}',
        **cfg.callbacks.model_checkpoint
    )

    early_stopping_callback = EarlyStopping(
        monitor=cfg.callbacks.model_checkpoint.monitor,
        patience=int(cfg.trainer.max_epochs * 0.25),
        **cfg.callbacks.early_stopping
    )

    tune_report_callbacks = TuneReportCallback(
        {
            "max_acc_frame": "max_val_frame_wise_accuracy",
            "max_acc_patient": "max_val_patient_wise_accuracy",
        },
        on="validation_end"
    )

    # SETUP TRAINER
    trainer = pl.Trainer(
        callbacks=[checkpoint_callback, early_stopping_callback, tune_report_callbacks],
        logger=logger,
        enable_progress_bar=False,
        **cfg.trainer
    )

    trainer.fit(lightning_model, datamodule=datamodule)
    trainer.test(lightning_model, datamodule=datamodule, verbose=False, ckpt_path="best")


@hydra.main(config_path="../configs/classification", config_name="lstm")
def train_tune(cfg):
    reporter = CLIReporter(
        parameter_columns=["backbone"],
        metric_columns=["max_acc_patient", "max_acc_frame", "training_iteration"],
    )

    config = {
        "backbone": tune.choice([
            "efficientnet_b0",
            "efficientnet_b1",
            "efficientnet_b1_pruned",
            "beit_base_patch16_224",
            "beit_base_patch16_224_in22k",
            "cait_s24_224",
            "cait_xxs24_224",
            "cait_xxs36_224",
            "convit_tiny",
            "convit_small",
            "vit_tiny_patch16_224",
            "vit_tiny_r_s16_p8_224",
            "deit_tiny_distilled_patch16_224",
            "deit_tiny_patch16_224"
        ]),
        "hydraconfig": cfg
    }
    analysis = tune.run(
        train_lstm,
        config=config,
        resources_per_trial={"cpu": 32, "gpu": 1},
        metric="max_acc_frame",
        mode="max",
        verbose=1,
        num_samples=20,
        progress_reporter=reporter,
        name="test_backbone_lstm"
    )

    print("Best hyperparameters found were: ", analysis.best_config)
    print("Best trail", analysis.best_trial)


if __name__ == '__main__':
    # train_tune()
    print("ok")

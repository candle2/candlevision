import os
import torch
import warnings
import numpy as np
from pathlib import Path

from captum.attr import IntegratedGradients, NoiseTunnel, GradientShap, Occlusion
from captum.attr import visualization as viz
from matplotlib import pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

from candlevision.data.dataset.pancreas_parts_dataset import PredictPancreasPartsDataset
from candlevision.models.classification.resnet_2d_classifier import BasicClassifier
from candlevision.models.classification.transform_video import VideoTransform
from candlevision.models.classification import PancreasPartsClassifier, LSTMClassifier, MViT, X3D, Resnet2Dplus1, SlowFast, \
    Resnet3D

warnings.filterwarnings("ignore")

os.environ["CUDA_VISIBLE_DEVICES"] = "2"


def main():
    path_frames = sorted(Path("/scratch/APEUS_DATA/pancreas_parts/dataset_v3/257/tail/clip_1").glob("*.png"))[2:2+16]

    # SETUP TEST DATASET
    test_transform = VideoTransform(
        num_frame_per_clip=16,
        spatial_transform=False,
        pixel_level_transform=False,
    )

    test_dataset = PredictPancreasPartsDataset(
        path_frames=path_frames,
        transform=test_transform
    )

    input = test_dataset.__getitem__(0)
    input = input.unsqueeze(0)

    model = MViT(model_num_class=6)

    # model = LSTMClassifier(
    #     num_classes=6,
    #     num_frame_per_clip=16,
    #     pretrained=True,
    #     backbone="xcit_tiny_24_p8_224",
    #     hidden_size=512,
    #     num_lstm_layer=2
    # )
    checkpoint_path = str([p for p in Path("/home/apeus/PycharmProjects/models/pancreas_parts/miccai/").glob(
        f"mvit*")][0])

    state_dict = torch.load(checkpoint_path)['state_dict']
    for key in list(state_dict.keys()):
        state_dict[key.replace('model.model.', 'model.')] = state_dict.pop(key)
        # state_dict[key.replace('model.', '')] = state_dict.pop(key)

    model.load_state_dict(state_dict, strict=True)

    # model = BasicClassifier(
    #         num_classes=6,
    #         name_model="efficientnet_b0",
    #         pretrained=False
    #     )

    model = model.cuda()
    input = input.cuda()

    print(model(input))

    integrated_gradients = IntegratedGradients(model)

    attributions_ig = integrated_gradients.attribute(input, target=5, n_steps=10)

    for k in range(int(input.size(1)/4)):
        transformed_img = input[0, k*4, ::]
        attention_map, _ = viz.visualize_image_attr_multiple(
            np.transpose(attributions_ig[0, k*4, ::].squeeze().cpu().detach().numpy(), (1, 2, 0)),
            np.transpose(transformed_img.squeeze().cpu().detach().numpy(), (1, 2, 0)),
            ["original_image", "heat_map"],
            ["all", "absolute_value"],
            cmap="plasma",
            show_colorbar=True
        )
        attention_map.savefig(f"/home/apeus/PycharmProjects/candlevision/figures/attention_map_257_clip_1_{k}.png")
    #     plt.show()

    # noise_tunnel = NoiseTunnel(integrated_gradients)
    # attributions_ig_nt = noise_tunnel.attribute(input, nt_samples=2, nt_type='smoothgrad_sq', target=5)
    # print("done")
    # for k in range(input.size(1)):
    #     transformed_img = input[0, k, ::]
    #     _ = viz.visualize_image_attr_multiple(np.transpose(attributions_ig_nt[0, k, ::].squeeze().cpu().detach().numpy(), (1, 2, 0)),
    #                                           np.transpose(transformed_img.squeeze().cpu().detach().numpy(), (1, 2, 0)),
    #                                           ["original_image", "heat_map"],
    #                                           ["all", "positive"],
    #                                           cmap="plasma",
    #                                           show_colorbar=True)
    #
    #     plt.show()

    # gradient_shap = GradientShap(model)
    #
    # # Defining baseline distribution of images
    # rand_img_dist = torch.cat([input * 0, input * 1])
    #
    # attributions_gs = gradient_shap.attribute(input,
    #                                           n_samples=30,
    #                                           stdevs=0.0001,
    #                                           baselines=rand_img_dist,
    #                                           target=5)
    # for k in range(input.size(1)):
    #     transformed_img = input[0, k, ::]
    #     _ = viz.visualize_image_attr_multiple(np.transpose(attributions_gs[0, k, ::].squeeze().cpu().detach().numpy(), (1, 2, 0)),
    #                                           np.transpose(transformed_img.squeeze().cpu().detach().numpy(), (1, 2, 0)),
    #                                           ["original_image", "heat_map"],
    #                                           ["all", "absolute_value"],
    #                                           cmap="plasma",
    #                                           show_colorbar=True)
    #     plt.show()



if __name__ == '__main__':
    main()
    # print("ok")

import argparse

import timm
import torch
import numpy as np
import cv2

from candlevision.models.self_supervised.simclr import ImageTransformer
from vit_rollout import VITAttentionRollout
from vit_grad_rollout import VITAttentionGradRollout


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--use_cuda', action='store_true', default=True,
                        help='Use NVIDIA GPU acceleration')
    parser.add_argument('--image_path', type=str,
                        default="/home/afleurentin/Bureau/PPP/datasets/dataset_v3/248/body/clip_1/SF0248_01_VID001_00044.png",
                        help='Input image path')
    parser.add_argument('--head_fusion', type=str, default='max',
                        help='How to fuse the attention heads for attention rollout. \
                        Can be mean/max/min')
    parser.add_argument('--discard_ratio', type=float, default=0.30,
                        help='How many of the lowest 14x14 attention paths should we discard')
    parser.add_argument('--category_index', type=int, default=None,
                        help='The category index for gradient rollout')
    args = parser.parse_args()
    args.use_cuda = args.use_cuda and torch.cuda.is_available()
    if args.use_cuda:
        print("Using GPU")
    else:
        print("Using CPU")

    return args


def show_mask_on_image(img, mask):
    img = np.float32(img) / 255
    heatmap = cv2.applyColorMap(np.uint8(255 * mask), cv2.COLORMAP_JET)
    heatmap = np.float32(heatmap) / 255
    cam = heatmap + np.float32(img)
    cam = cam / np.max(cam)
    return np.uint8(255 * cam)


def load_checkpoint(model: torch.nn.Module, checkpoint_path: str) -> torch.nn.Module:
    """

    :param model: torch model
    :param checkpoint_path: path to weights
    :return: a torch model with weights from the checkpoints
    """
    state_dict = torch.load(checkpoint_path)['state_dict']
    for key in list(state_dict.keys()):
        state_dict[key.replace('model.', '')] = state_dict.pop(key)
    try:
        model.load_state_dict(state_dict, strict=True)
    except RuntimeError as e:
        # logger.warning(f"Ignoring: {e}")
        model.load_state_dict(state_dict, strict=False)

    return model


def get_model(name_model: str, path_checkpoint: str) -> torch.nn.Module:
    model = timm.create_model(name_model, num_classes=6, pretrained=False)
    model = load_checkpoint(model, path_checkpoint)

    model = model

    return model


if __name__ == '__main__':
    args = get_args()
    model = get_model(
        name_model="xcit_small_12_p8_224",
        path_checkpoint="/home/afleurentin/Bureau/PPP/miccai_models/basic_xcit_small_12_p8_224_epoch=0.ckpt"
    )
    # model = get_model(
    #     name_model="twins_svt_small",
    #     path_checkpoint="/home/afleurentin/Bureau/PPP/miccai_models/basic_twins_svt_small_epoch=3.ckpt"
    # )
    # model = get_model(
    #     name_model="vit_tiny_patch16_224",
    #     path_checkpoint="/home/afleurentin/Bureau/PPP/miccai_models/basic_vit_tiny_patch16_224_epoch=1.ckpt"
    # )
    model.eval()

    if args.use_cuda:
        model = model.cuda()

    transform = ImageTransformer(spatial_transform=False, pixel_level_transform=False)

    # img = Image.open(args.image_path)
    img = cv2.imread(args.image_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    input_tensor = transform(img)["image"].unsqueeze(0)

    if args.use_cuda:
        input_tensor = input_tensor.cuda()

    if args.category_index is None:
        print("Doing Attention Rollout")
        attention_rollout = VITAttentionRollout(model, head_fusion=args.head_fusion,
                                                discard_ratio=args.discard_ratio)
        mask = attention_rollout(input_tensor)
        name = "attention_rollout_{:.3f}_{}.png".format(args.discard_ratio, args.head_fusion)
    else:
        print("Doing Gradient Attention Rollout")
        grad_rollout = VITAttentionGradRollout(model, discard_ratio=args.discard_ratio)
        mask = grad_rollout(input_tensor, args.category_index)
        name = "grad_rollout_{}_{:.3f}_{}.png".format(args.category_index,
                                                      args.discard_ratio, args.head_fusion)

    np_img = np.array(img)[:, :, ::-1]
    mask = cv2.resize(mask, (np_img.shape[1], np_img.shape[0]))
    mask = show_mask_on_image(np_img, mask)
    cv2.imshow("Input Image", np_img)
    cv2.imshow(name, mask)
    # cv2.imwrite("input.png", np_img)
    # cv2.imwrite(name, mask)
    cv2.waitKey(-1)

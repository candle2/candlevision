import pickle
import json
import pandas as pd
import fiftyone as fo
from tqdm import tqdm
from fiftyone import ViewField as F

from candlevision.data.dataset import ApeusDataset


def gen_pred(data, image_id: int = 0):
    for class_id in range(len(data[image_id])):
        for pred_id in range(len(data[image_id][class_id])):
            pred = data[image_id][class_id][pred_id]
            coord = pred[:4]
            score = pred[4]
            yield coord, class_id, score


def eval_centernet(path_data: str,
                   path_images_dir: str,
                   path_json: str,
                   path_save: str,
                   subfolder: bool,
                   confidence_threshold: float):
    data = pickle.load(open(path_data, "rb"))
    val_dataset = ApeusDataset(
        path_images_dir=path_images_dir,
        path_json=path_json,
        subfolder=subfolder
    )

    classes = val_dataset.labels
    evaluator = fo.Dataset()

    # Loop over the bounding boxes and labels in the data batch
    for k, (image, image_boxes, image_labels, id_patient) in tqdm(enumerate(val_dataset), total=len(val_dataset)):
        # For each image, we create a sample
        sample_image = fo.Sample(filepath=f"image_{k}", patient_id=id_patient[0])
        width, height = image.shape[:2]

        # GROUND TRUTH
        ground_truth = []
        # Loop over each bounding box and label
        for box, label in zip(image_boxes, image_labels):
            x, y, w, h = box
            rel_box = [x / width, y / height, w / width, h / height]

            ground_truth.append(
                fo.Detection(
                    label=classes[int(label - 1)],
                    bounding_box=rel_box,
                )
            )

        # Save sample ground truth in the dataset
        sample_image["ground_truth"] = fo.Detections(detections=ground_truth)

        # PREDICTIONS
        detections = []
        predictions_generator = gen_pred(data, image_id=k)

        if len(list(predictions_generator)) == 0:
            evaluator.add_sample(sample_image)
            print("ERREUR no predictions")
            continue

        predictions_generator = gen_pred(data, image_id=k)
        for box, label, score in predictions_generator:
            x1, y1, x2, y2 = box
            rel_box = [x1 / width, y1 / height, (x2 - x1) / width, (y2 - y1) / height]

            detections.append(
                fo.Detection(
                    label=classes[int(label)],
                    bounding_box=rel_box,
                    confidence=score
                )
            )

        sample_image["predictions"] = fo.Detections(detections=detections)
        evaluator.add_sample(sample_image)

    result_evaluation = {}

    for k in val_dataset.patient_ids():
        print(f"EVALUATION PATIENT n°{k}")
        patient_results = {}
        patient_wise_evaluator = evaluator.match(F("patient_id") == k)

        results = patient_wise_evaluator.evaluate_detections("predictions",
                                                             gt_field="ground_truth",
                                                             eval_key="eval",
                                                             compute_mAP=True,
                                                             classwise=True,
                                                             iou_threshs=[0.5])
        # mAP  and AP per class
        patient_results['mAP'] = results.mAP()

        classes = list(val_dataset.labels.values())
        for c in classes:
            patient_results['AP_' + c] = results._classwise_AP[results._get_class_index(c)]

        # Precision Recall curves
        plot = results.plot_pr_curves()
        name_figure = path_save + f"/PR_curve_patient_{k}.jpg"
        patient_results['fig'] = name_figure
        plot.write_image(name_figure)

        # Precision, recall, f1-score
        report_evaluator = patient_wise_evaluator.filter_labels("predictions",
                                                                F("confidence") > confidence_threshold)

        report_results = report_evaluator.evaluate_detections("predictions",
                                                              gt_field="ground_truth",
                                                              eval_key="eval",
                                                              compute_mAP=True,
                                                              classwise=True,
                                                              iou_threshs=[0.5])

        print("DATAFRAME: \n")
        report = report_results.report()
        df_report = pd.DataFrame(report).transpose()
        df_report = df_report.round(2)
        patient_results['report'] = df_report.to_string()

        result_evaluation[k] = patient_results

    with open(path_save + "/evaluation_results.json", 'w') as json_file:
        json.dump(result_evaluation, json_file)


if __name__ == '__main__':
    for k in range(1, 6):
        eval_centernet(
            path_data=f"/home/afleurentin/Bureau/Benchmark/centernet/centernet_fold_{k}/centernet_fold_{k}.pkl",
            path_images_dir="/home/afleurentin/Bureau/datasets/dataset_v2/frames",
            path_json=f"/home/afleurentin/Bureau/datasets/dataset_v2/annotations/val_coco_2_labels_fold_{k}_without_tn.json",
            subfolder=True,
            path_save=f"/home/afleurentin/Bureau/Benchmark/centernet/centernet_fold_{k}",
            confidence_threshold=0.30
        )

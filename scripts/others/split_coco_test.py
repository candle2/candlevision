from candlevision.utils.utils_coco_json import split_coco_json

split_coco_json(path_json="/home/apeus/Desktop/dataset_v2_cleaned/coco.json",
                val_split=['WH0060', '000020', '000034', '000022', '000032', '000056', '000016', 'KA0063', '000057', '000007'],
                save_dir="/home/apeus/Desktop/dataset_v2_cleaned/annotations")

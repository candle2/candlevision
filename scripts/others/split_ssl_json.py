import json
import copy

# list_patient_val = ['31', '46', '29', '56', '20', '25', '11', '21', '47']
list_patient_val = ['26', '27', '9', '32', '34', '35']
# list_patient_val = ['30', '38', '36', '3', '39', '6', '51']
# list_patient_val = ['33', '18', '22', '28', '24']

fold = "2"
path_json = "/home/apeus/Desktop/dataset_v1/json_ssl/ssl_frames.json"

with open(path_json) as json_file:
    data = json.load(json_file)

all_patient_ids = list(data.keys())
print(f"number of patient in the all dataset: {len(all_patient_ids)}")

list_patient_train = [i for i in all_patient_ids if i not in list_patient_val]

train_data, val_data = data, copy.deepcopy(data)

for p in list_patient_val:
    train_data.pop(p)

with open(f"/home/apeus/Desktop/dataset_v1/json_ssl/train_ssl_fold_{fold}.json", 'w') as json_file:
    json.dump(train_data, json_file)

for q in list_patient_train:
    val_data.pop(q)

with open(f"/home/apeus/Desktop/dataset_v1/json_ssl/val_ssl_fold_{fold}.json", 'w') as json_file:
    json.dump(val_data, json_file)

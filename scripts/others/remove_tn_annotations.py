from pathlib import Path
from candlevision.utils.utils_coco_json import remove_tn_coco_json

path_anno_dir = "/home/apeus/Desktop/dataset_v2_cleaned/annotations/"
path_annos = Path(path_anno_dir).glob("*.json")

for n in path_annos:
    remove_tn_coco_json(
        path_json=n,
        save_path=str(n)[:-5] + "_without_tn.json"
    )



import os

from pathlib import Path
from moviepy.editor import VideoFileClip


class FrameExtractor:
    """ Extract frames from videos and save them.

    To work, the FrameExtractor expect the following repository structure:

    .. code-block:: bash

        path_dir_video
        ├── _ procedure_1
        |   ├── procedure_1_VID001_part_0.mp4
        |   ├── procedure_1_VID001_part_1.mp4
        |   └── procedure_1_VID001_part_2.mp4
        |
        └── _ procedure_2
          ├── procedure_2_VID001_part_0.mp4
          ├── procedure_2_VID002_part_0.mp4
          └── procedure_2_VID003_part_0.mp4

    And the following dataset will be generated:

    .. code-block:: bash

        path_save_dir
        ├── _ procedure_1
        |   ├── _ VID001_part_0
        |       ├── frame_1.png
        |       ├── frame_2.png
        |       └── frame_3.png
        |   ├── _ VID001_part_1
        |   └── _ VID001_part_2
        |
        └── _ procedure_2
          ├── _ VID001_part_0
          ├── _ VID002_part_0
          └── _ VID003_part_0

    .. warning:: The videos must be crop or process beforehand because no preprocessing (except resizing) is done at
    this point

    """

    def __init__(self,
                 path_dir_procedure: str = None,
                 path_save_dir: str = None,
                 fps: int = 1,
                 frame_size: int = 224):
        """
        :param path_dir_procedure: path of the directory where the videos are saved
        :param path_save_dir: directory where the frames will be saved
        :param fps: extraction frame rate
        :param frame_size: value of resize

        """
        self.path_dir_procedure = path_dir_procedure
        self.root_save_dir = path_save_dir
        self.fps = fps
        self.frame_size = frame_size

        self.json_data = {}
        self.video = None

    def videos_path(self) -> list:
        """ return a list of videos path """
        videos_path = []
        for procedure_dir in Path(self.path_dir_procedure).iterdir():
            if procedure_dir.is_dir():
                for path_video in procedure_dir.glob("*.mp4"):
                    videos_path.append(path_video)
        return videos_path

    @staticmethod
    def get_id_procedure(path: Path) -> str:
        """ return the id of a procedure """
        return str(path.stem)[:9]

    @staticmethod
    def get_id_video(path: Path) -> str:
        """ return the id of a video """
        return str(path.stem)[10:]

    def preprocess(self):
        """ resize the video """
        self.video = self.video.resize((self.frame_size, self.frame_size))

    def __call__(self, *args, **kwargs):
        for path_video in self.videos_path()[1400:]:
            # load video and resize
            self.video = VideoFileClip(str(path_video))
            self.preprocess()
            # create save path
            save_path = os.path.join(
                self.root_save_dir,
                self.get_id_procedure(path_video),
                self.get_id_video(path_video)
            )
            # create new folder
            os.makedirs(save_path, exist_ok=True)
            # extract frames
            self.video.write_images_sequence(
                nameformat=f"{os.path.join(save_path, str(path_video.stem))}_%05d.png",
                fps=self.fps
            )


if __name__ == '__main__':
    path_dir_vid = "/media/afleurentin/APEUS_WP2$/SSL_DATASET"
    path_save_dir = "/media/afleurentin/APEUS_WP2$/SSL_FRAMES"
    extractor = FrameExtractor(
        path_dir_procedure=path_dir_vid,
        path_save_dir=path_save_dir
    )
    extractor()


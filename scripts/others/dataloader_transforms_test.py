from candlevision.data.dataset import ApeusDataset
from candlevision.data.dataset.predict_dataset import PredictVideoDataset
from candlevision.data.dataset.coco_dataset import CocoDataset
from candlevision.data.loaders.coco_eus_loader import DetectDataModule, DetectEusDataModule
from candlevision.utils.utils import ConfigReader
import cv2


cfg = ConfigReader(
        config_file="/home/afleurentin/PycharmProjects/candlevision/configs/faster/config-faster-rcnn-1GPU.yml"
)

# DEFINE DATASET
train_dataset = ApeusDataset(path_images_dir=cfg.dataset.dir,
                             path_json=cfg.dataset.path_json_train,
                             subfolder=cfg.dataset.subfolder)

val_dataset = ApeusDataset(path_images_dir=cfg.dataset.dir,
                           path_json=cfg.dataset.path_json_val,
                           subfolder=cfg.dataset.subfolder)

# DEFINE DATALOADER
datamodule = DetectEusDataModule(train_dataset=train_dataset,
                                 val_dataset=val_dataset,
                                 num_workers=cfg.datamodule.num_workers,
                                 normalize=cfg.datamodule.normalized,
                                 resize=cfg.datamodule.resize,
                                 batch_size=cfg.datamodule.batch_size,
                                 pin_memory=cfg.datamodule.pin_memory,
                                 drop_last=cfg.datamodule.drop_last)


train = datamodule.train_dataloader()
print(len(train))
it = iter(train)

images, boxes, labels, _ = next(it)

cv2.imshow("test", images[0])
cv2.waitKey(0)

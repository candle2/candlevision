import yaml


def generate_folds_config(num_fold: int = 5):
    """ Generate multiple configuration files for faster RCNN training

    :param num_fold: number of folds desired
    """

    for k in range(1, num_fold + 1):
        dict_file = {
                "dataset": {
                    "dir": "/home/apeus/Desktop/dataset_v2_cleaned/frames",
                    "path_json_train": f"/home/apeus/Desktop/dataset_v2_cleaned/annotations/train_coco_2_labels_fold_{k}_without_tn.json",
                    "path_json_val": f"/home/apeus/Desktop/dataset_v2_cleaned/annotations/val_coco_2_labels_fold_{k}_without_tn.json",
                    "subfolder": True,
                },

                "datamodule": {
                    "num_workers": 4,
                    "normalize": False,
                    "resize": [800, 800],
                    "batch_size": 16,
                    "pin_memory": True,
                    "drop_last": False,
                },

                "model": {
                    "backbone": "resnet50",
                    "pretrained": f"/home/apeus/PycharmProjects/models/dataset_v2/retinanet/fold_{k}/",
                    "trainable_backbone_layers": 5,
                    "image_mean": [0.485, 0.456, 0.406],
                    "image_std": [0.229, 0.224, 0.225],
                    "image_min_size": 600,
                    "image_max_size": 800,
                    "detections_per_img": 100,
                    "fg_iou_thresh": 0.4,
                    "bg_iou_thresh": 0.2,
                },

                "detector": {
                    "learning_rate": 0.0001,
                    "weight_decay": 0.0001,
                    "metric": "voxel51",  # or 'voxel51'
                    "path_save_evaluation_result": f"/home/apeus/Desktop/bench_retinanet/fold_{k}",  # only for voxel51
                    "score_threshold": 0.4,  # only for voxel51
                },

                "checkpoint": {
                    "fold": f"fold_{k}",
                    "checkpoint_path": f"/home/apeus/PycharmProjects/models/dataset_v2/retinanet/fold_{k}/",
                    "monitor": "GLOBAL METRICS/Mean Average Precision Patient-wise [ IoU=0.50 | area= all | maxDets=100 ]",
                    "save_top_k": 1,
                    "mode": "max",
                    "save_weights_only": True,
                },

                "early_stopping": {
                    "patience": 20,
                    "mode": "max",
                    "check_on_train_epoch_end": False,
                },

                "logger": {
                    "save_dir": "/home/apeus/PycharmProjects/candlevision/logs/dataset_v2_cleaned/retinanet/",
                    "name": f"fold_{k}",
                    "version": "imagenet-2-labels-adamw-warmup-1",
                },

                "trainer": {
                    "weights_summary": "top",  # option : "full", "top", None
                    "progress_bar_refresh_rate": 100,
                    "num_sanity_val_steps": 0,
                    "accelerator": "ddp",
                    "fast_dev_run": False,
                    "gpus": 1,
                    "max_epochs": 2,
                    "auto_lr_find": False,
                },

            }

        with open(f"/home/apeus/PycharmProjects/candlevision/configs/retinanet/retinanet_fold_{k}.yml", "w") as file:
            config = yaml.dump(dict_file, file)


if __name__ == '__main__':
    generate_folds_config(num_fold=5)

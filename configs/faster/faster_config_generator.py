import yaml


def generate_folds_config(num_fold: int = 5):
    """ Generate multiple configuration files for faster RCNN training

    :param num_fold: number of folds desired
    """

    for k in range(1, num_fold + 1):
        dict_file = {
                "dataset": {
                    "dir": "/home/apeus/dataset_v2/frames",
                    "path_json_train": f"/home/apeus/dataset_v2/annotations/train_coco_2_labels_fold_{k}.json",
                    "path_json_val": f"/home/apeus/dataset_v2/annotations/val_coco_2_labels_fold_{k}_without_tn.json",
                    "subfolder": True,
                },

                "datamodule": {
                    "num_workers": 4,
                    "normalize": False,
                    "resize": [800, 800],
                    "batch_size": 16,
                    "pin_memory": True,
                    "drop_last": False,
                },

                "model": {
                    "backbone": "resnet50",
                    "fpn": True,
                    "pretrained": True,
                    "trainable_backbone_layers": 5,
                    "image_mean": [0.559, 0.574, 0.566],
                    "image_std": [0.393, 0.395, 0.399],
                    "image_min_size": 600,
                    "image_max_size": 800,
                    "rpn_pre_nms_top_n_train": 500,
                    "rpn_pre_nms_top_n_test": 500,
                    "rpn_post_nms_top_n_train": 200,
                    "rpn_post_nms_top_n_test": 200,
                },

                "detector": {
                    "learning_rate": 0.0001,
                    "weight_decay": 0.0002,
                    "metric": "coco",  # or 'voxel51'
                    "path_save_evaluation_result": f"/home/apeus/Desktop/score_influence/fold_{k}/test_15",  # only for voxel51
                    "score_threshold": 0.15,  # only for voxel51
                },

                "checkpoint": {
                    "fold": f"fold_{k}",
                    "checkpoint_path": f"/home/apeus/PycharmProjects/models/dataset_v2/pretrained_simclr/fold_{k}/",
                    "monitor": "GLOBAL METRICS/Mean Average Precision Patient-wise [ IoU=0.50 | area= all | maxDets=100 ]",
                    "save_top_k": 1,
                    "mode": "max",
                    "save_weights_only": True,
                },

                "early_stopping": {
                    "patience": 30,
                    "mode": "max",
                    "check_on_train_epoch_end": False,
                },

                "training_strategy": {
                    "unfreeze_backbone_at_epoch": 2,
                    "unfreeze_fpn_at_epoch": 1,
                    "unfreeze_layer_rate": 1,
                },

                "logger": {
                    "save_dir": "/home/apeus/PycharmProjects/candlevision/logs/dataset_v2_cleaned/pretrained_simclr/",
                    "name": f"fold_{k}",
                    "version": "transfert-imagenet-3",
                },

                "trainer": {
                    "weights_summary": "top",  # option : "full", "top", None
                    "progress_bar_refresh_rate": 100,
                    "num_sanity_val_steps": 0,
                    "accelerator": None,
                    "fast_dev_run": False,
                    "gpus": 1,
                    "max_epochs": 30,
                    "auto_lr_find": False,
                },

            }

        with open(f"/home/apeus/projects/candlevision/configs/faster/faster-rcnn_fold_{k}.yml", "w") as file:
            config = yaml.dump(dict_file, file)


if __name__ == '__main__':
    generate_folds_config(num_fold=5)

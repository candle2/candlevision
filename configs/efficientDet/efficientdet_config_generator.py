import yaml


def generate_folds_config(num_fold: int = 5):
    """ Generate multiple configuration files for faster RCNN training

    :param num_fold: number of folds desired
    """

    for k in range(1, num_fold + 1):
        dict_file = {
                "dataset": {
                    "dir": "/home/apeus/Desktop/dataset_v2_cleaned/frames",
                    "path_json_train": f"/home/apeus/Desktop/dataset_v2_cleaned/annotations/train_coco_2_labels_fold_{k}.json",
                    "path_json_val": f"/home/apeus/Desktop/dataset_v2_cleaned/annotations/val_coco_2_labels_fold_{k}_without_tn.json",
                    "subfolder": True,
                },

                "datamodule": {
                    "num_workers": 4,
                    "normalize": True,
                    "resize": [768, 768], # input_sizes = [512, 640, 768, 896, 1024, 1280, 1280, 1536, 1536]
                    "batch_size": 4,
                    "pin_memory": True,
                    "drop_last": True,
                },

                "model": {
                    "compound_coef": 2, # determine which backbone will be used
                    "pretrained": "/home/apeus/PycharmProjects/models/efficientDet/pretrained/efficientdet-d2.pth",
                    "head_only": False,
                },

                "loss": {
                    "alpha": 0.25,
                    "gamma": 1.5,
                    "upper_threshold_objectness": 0.5,
                    "lower_threshold_objectness": 0.4
                },

                "detector": {
                    "learning_rate": 0.0005,
                    "weight_decay": 0.01,
                    "metric": "coco",  # or 'voxel51'
                    "path_save_evaluation_result": "/home/afleurentin/Bureau/test_voxel51",  # only for voxel51
                    "score_threshold": 0.25,  # only for voxel51
                },

                "checkpoint": {
                    "fold": f"fold_{k}",
                    "checkpoint_path": f"/home/apeus/PycharmProjects/models/dataset_v2/efficientdet/fold_{k}/",
                    "monitor": "GLOBAL METRICS/Mean Average Precision Patient-wise [ IoU=0.50 | area= all | maxDets=100 ]",
                    "save_top_k": 1,
                    "mode": "max",
                    "save_weights_only": True,
                },

                "early_stopping": {
                    "patience": 20,
                    "mode": "max",
                    "check_on_train_epoch_end": False,
                },

                "logger": {
                    "save_dir": "/home/apeus/PycharmProjects/candlevision/logs/dataset_v2_cleaned/efficientdet/",
                    "name": f"fold_{k}",
                    "version": "imagenet-2-labels-adamw-warmup-1",
                },

                "trainer": {
                    "weights_summary": "top",  # option : "full", "top", None
                    "progress_bar_refresh_rate": 10,
                    "num_sanity_val_steps": 0,
                    "accelerator": "ddp",
                    "fast_dev_run": False,
                    "gpus": 4,
                    "max_epochs": 20,
                    "auto_lr_find": False,
                },

            }

        with open(f"/home/apeus/PycharmProjects/candlevision/configs/efficientDet/efficientdet_fold_{k}.yml", "w") as file:
            config = yaml.dump(dict_file, file)


if __name__ == '__main__':
    generate_folds_config(num_fold=5)

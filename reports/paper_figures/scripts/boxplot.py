import io
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from getpass import getpass
from office365.runtime.auth.authentication_context import AuthenticationContext
from office365.sharepoint.client_context import ClientContext
from office365.sharepoint.files.file import File

###############################################################################################
#                   Connection to Sharepoint to get data from excel
###############################################################################################

# define the url the sharepoint
url = 'https://ihustrasbourgeu.sharepoint.com/sites/PRJ_APEUS_WP2_IA'
# define path to the database
relative_url = '/sites/PRJ_APEUS_WP2_IA/Documents%20partages/Results_Benchmark.xlsx'

# Enter your username
username = 'antoine.fleurentin@ihu-strasbourg.eu'
# Enter your password
password = getpass('Password:')

ctx_auth = AuthenticationContext(url)
print(ctx_auth)

if ctx_auth.acquire_token_for_user(username, password):
    ctx = ClientContext(url, ctx_auth)
    web = ctx.web
    ctx.load(web)
    ctx.execute_query()
    print("Web title: {0}".format(web.properties['Title']))
else:
    print(ctx_auth.get_last_error())

response = File.open_binary(ctx, relative_url)

# save data to BytesIO stream
bytes_file_obj = io.BytesIO()
bytes_file_obj.write(response.content)
bytes_file_obj.seek(0)  # set file object to start

###############################################################################################
#                                       Dataframe
###############################################################################################

# read file into pandas dataframe
df = pd.read_excel(bytes_file_obj, engine='openpyxl',sheet_name='Feuil1')
# filter the main df to separate the two tables
metrics = ['precision', 'sensitivity', 'f1-score']
structures = ['Lesion', 'Parenchyma']

# FEUILLE 1: 3:8 & 13:18

df_eval = {"models": [value for value in df.iloc[3:8, 0] for k in range(15)] + [value for value in df.iloc[13:18, 0] for k in range(15)],  # num_fold * num_metrics
           "score (%)": list(df.iloc[3:8, 1:16].to_numpy().flatten()) + list(df.iloc[13:18, 1:16].to_numpy().flatten()),
           "metrics": [m for k in range(50) for m in metrics],  # num_fold * num_model * num_class 50
           "": [s for s in structures for k in range(75)]   # num_fold * num_metrics * num_model 75
           }

# df_lesi = {"models": [value for value in df.iloc[3:8, 0] for k in range(15)],  # num_fold * num_metrics
#            "score (%)": df.iloc[3:8, 1:16].to_numpy().flatten(),
#            "Lesions: metrics": [m for k in range(25) for m in metrics]  # num_fold * num_model
#            }
#
# df_paren = {"models": [value for value in df.iloc[13:18, 0] for k in range(15)],
#             "score (%)": df.iloc[13:18, 1:16].to_numpy().flatten(),
#             "Parenchyma: metrics": [m for k in range(25) for m in metrics]
#             }
#
# df_lesi = pd.DataFrame(df_lesi)
# df_paren = pd.DataFrame(df_paren)
df_eval = pd.DataFrame(df_eval)

###############################################################################################
#                                       Draw graph
###############################################################################################

plt.style.use(['science', 'no-latex', 'grid'])
plt.rcParams.update({'font.size': 18})

ax1 = sns.catplot(x="models", y="score (%)", hue="metrics", col="", capsize=.2, palette="flare", height=6,
                       aspect=1.2, kind="bar", legend=True, data=df_eval)
ax1.set_xticklabels(rotation=30, ha='right')
plt.savefig(f"/home/afleurentin/PycharmProjects/candlevision/paper_figures/figures/barplot_results.png")

# ax1 = sns.catplot(x="models", y="score (%)", hue="Lesions: metrics", capsize=.2, palette="flare", height=6,
#                        aspect=1.2, kind="bar", legend=True, data=df_lesi)
#
# plt.savefig(f"/home/afleurentin/PycharmProjects/candlevision/paper_figures/figures/barplot_lesion.png")
#
# ax2 = sns.catplot(x="models", y="score (%)", hue="Parenchyma: metrics", capsize=.2, palette="YlGnBu_d", height=6,
#                        aspect=1.2, kind="bar", legend=True, data=df_paren)
#
# plt.savefig(f"/home/afleurentin/PycharmProjects/candlevision/paper_figures/figures/barplot_paren.png")

# # Draw a pointplot to show pulse as a function of three categorical factors
# for name_metric in metrics:
#     # LESIONS
#     lesi_graph = sns.catplot(x="models", y="score", col='Lesions: metrics', capsize=.2, palette="YlGnBu_d", height=6,
#                              aspect=1.2, kind="point", legend=True,
#                              data=df_lesi.loc[df_lesi['Lesions: metrics'] == name_metric])
#     plt.savefig(f"/home/afleurentin/PycharmProjects/candlevision/paper_figures/figures/catplot_lesion_{name_metric}.png")
#
#     # PARENCHYMA
#     paren_graph = sns.catplot(x="models", y="score", col='Parenchyma: metrics', capsize=.2, palette="YlGnBu_d",
#                               height=6, aspect=1.2, kind="point", legend=True,
#                               data=df_paren.loc[df_paren['Parenchyma: metrics'] == name_metric])
#     plt.savefig(f"/home/afleurentin/PycharmProjects/candlevision/paper_figures/figures/catplot_paren_{name_metric}.png")
#
# # WITH ALL METRICS
# full_lesi_graph = sns.catplot(x="models", y="score", col="Lesions: metrics", capsize=.2, palette="YlGnBu_d", height=6,
#                               aspect=1.2, kind="point", data=df_lesi)
#
# plt.savefig(f"/home/afleurentin/PycharmProjects/candlevision/paper_figures/figures/catplot_lesion_full.png")
#
# full_paren_graph = sns.catplot(x="models", y="score", col="Parenchyma: metrics", capsize=.2, palette="YlGnBu_d",
#                                height=6, aspect=1.2, kind="point", data=df_paren)
#
# plt.savefig(f"/home/afleurentin/PycharmProjects/candlevision/paper_figures/figures/barplot_paren_full.png")

from typing import List, Tuple
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from pathlib import Path
from matplotlib.lines import Line2D
from tqdm import tqdm
from candlevision.utils.utils import eus_layout_crop
from moviepy.editor import VideoFileClip
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import seaborn_image as isns
from mpl_toolkits.axes_grid1 import ImageGrid
import matplotlib.patches as patches
# matplotlib.use('TKAgg')
plt.style.use('science')

def extract_frames(path_video: str, path_save_dir: str, fps: float = 1., timestamp_subclip: list = None):
    """ extract the frames from a video and save them in a directory """
    video = VideoFileClip(path_video)

    if timestamp_subclip:
        video = video.subclip(timestamp_subclip[0], timestamp_subclip[1])

    first_frame = video.get_frame(0)
    _, slice_height, slice_width = eus_layout_crop(first_frame)

    video_name = path_video.split('/')[-1][:-4]

    print(f"[INFO] process video {video_name}")

    for i, t in enumerate(tqdm(np.arange(1, video.duration, 1 / fps))):
        name_frame = path_save_dir + '/' + f"vid_{video_name}_fps_{fps}_{i + 1}.png"
        frame = video.get_frame(t)
        frame = frame[slice_height, slice_width, :]
        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        cv2.imwrite(name_frame, frame)


def display_seaquence(path_image_dir: str = None,
                      img_height: int = 160,
                      img_width: int = 300,
                      img_space_between: int = 0):
    
    sequence_path = Path(path_image_dir)
    
    fig, ax = plt.figure(), plt.axes(frameon=False)
    ax.set_xlim(0, 1000)
    
    for k, image_name in enumerate(sequence_path.glob('*.png')):

        image = mpimg.imread(image_name)
        print(image.shape)
        image = cv2.resize(image, (img_width, img_height), interpolation=cv2.INTER_CUBIC)
        print(image.shape)

        #imagebox = OffsetImage(image, zoom=0.2)
        imagebox = OffsetImage(image)

        #ab = AnnotationBbox(imagebox, (100 + (k*220), 0.5), frameon=False)
        x_offset = k * (img_width + img_space_between)
        ab = AnnotationBbox(imagebox, (x_offset, 0.5), frameon=False)

        ax.add_artist(ab)

        plt.draw()

    ax.axes.get_yaxis().set_visible(False)
    xmin, xmax = ax.get_xaxis().get_view_interval()
    ymin, ymax = ax.get_yaxis().get_view_interval()
    ax.add_artist(Line2D((xmin, xmax), (ymin, ymin), color='black', linewidth=2))
    plt.show()


def add_patches(ax, xmin, ymax, width, height, fill = False, alpha=1.0, color="black"):
    rect = patches.Rectangle((xmin, ymax), width, height, fill=fill, alpha=alpha, color=color, linewidth=3)
    ax.add_patch(rect)
    
def add_patches_filled(ax, xmin, ymax, width, height, alpha=1.0, color="black", linewidth=1):

    # first draw inside
    rect = patches.Rectangle((xmin, ymax), width, height, fill=True, alpha=alpha, color=color)
    ax.add_patch(rect)
    
    # draw border
    rect = patches.Rectangle((xmin, ymax), width, height, fill=False, color=color, linewidth=linewidth)
    ax.add_patch(rect)


def display_seaquence_2(path_image_dir: str = None,
                      img_height: int = 160,
                      img_width: int = 300,
                      img_space_between: int = 0,
                      fontsize: int = 4,
                      grid_size: Tuple = (32, 32),
                      nrows_ncols: Tuple = (2, 8),
                      y_labels: List = [],
                      x_labels: List = [],
                      bboxs: List = []):
    

    fig = plt.figure(figsize=grid_size, dpi=300)
    grid = ImageGrid(fig, 111,  # similar to subplot(111)
                    nrows_ncols=(2, 8),  # creates 2x2 grid of axes
                    axes_pad=0.1,  # pad between axes in inch.
                    share_all=True
                    )

    sequence_path = Path(path_image_dir)
    img_list = []
    
    # collect images
    for image_name in sequence_path.glob('*.png'):
        img_list.append(mpimg.imread(image_name))
        
    y_label_index = 0
    x_label_index = 0
    for i, (ax, im) in enumerate(zip(grid, img_list)):

        # add y labels
        if i%nrows_ncols[1] == 0 and i//nrows_ncols[1] < len(y_labels):
            ax.set_ylabel(y_labels[y_label_index], size='large', fontsize=fontsize)
            y_label_index += 1
            
        # add x labels
        if i < nrows_ncols[0] and i <= len(x_labels):
            ax.set_title(x_labels[x_label_index], size='large', fontsize=fontsize)
            x_label_index += 1
        
        # draw bboxs        
        if i < len(bboxs):
            for bbox in bboxs[i]:
                add_patches_filled(ax, **bbox, alpha=0.05, linewidth=0.5)
            
        ax.imshow(im, origin="upper")
        ax.set_xticklabels([])
        ax.set_yticklabels([])

    plt.show()    


if __name__ == '__main__':
    # extract_frames(path_video="/home/ameyer/Downloads/000009_VID001_an.mp4",
    #                path_save_dir="/home/ameyer/Downloads/test_extract",
    #                fps=0.1,
    #                timestamp_subclip=[5, 60])
    
    bboxs = [
        [
            {
                "xmin": 100, "ymax": 300, "width": 300, "height": 200, "color": "green"
            },
            {
                "xmin": 200, "ymax": 400, "width": 800, "height": 300, "color": "red"
            },
        ], # image 1
        [
            {
                "xmin": 100, "ymax": 300, "width": 300, "height": 200, "color": "red"
            }            
        ], # image 2,
        [],
        [
            {
                "xmin": 100, "ymax": 300, "width": 300, "height": 200, "color": "green"
            },
            {
                "xmin": 200, "ymax": 400, "width": 800, "height": 300, "color": "red"
            },
        ]
    ]

    display_seaquence_2("/home/ameyer/Downloads/test_extract",
                        y_labels = ["Predicted", "Ground True"],
                        x_labels = ["TTTTT", "AAAA"],
                        bboxs=bboxs)
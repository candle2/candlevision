import numpy as np
import torch


def test_convert_coco_bboxes_to_tensor():
    from candlevision.utils.utils import convert_coco_bboxes_to_tensor
    bb_wh = np.array([1, 2, 3, 4])
    bb_xy = torch.tensor([1, 2, 4, 6])
    assert all(convert_coco_bboxes_to_tensor(bb_wh) == bb_xy)
